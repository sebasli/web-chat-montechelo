//call framework express
var express = require('express');
var app = express();
//var path = require("path");
var server = require('http').Server(app);
//variable para manejar eventos de socketio, se determina el path por el cual va generar el socket
const io = require('socket.io')(server, { path: '/widget/sbs/socket.io' });
//configura la variable dotenv para llamar valores del .env
const dotenv = require('dotenv');
dotenv.config();
//libreria para formatear Strings
const format = require('string-format');
//inicializa el metodo format de la libreria string-format
format.extend(String.prototype, {});
//requiere la dependencia Cors de Cross Origin Resource Sharing
const cors = require('cors');
//middleware que escucha los eventos post, get al servidor
const morgan = require('morgan');
//llamado de la configuración wiston para loggin en produccion
const logger = require('../config/winston');
//define la configuracion del router
const router = express.Router();
//define ruta message.js
const utilMessages = require("./routes/utilities/message");

/*
  Se define que va usar el servidor expres: CROS, MORGAN con WINSTON, STATIC FILES, RUTA de TASKS (API)
*/

//-------------------SULLLA--------------------
const bodyParser = require('body-parser');
const fs = require('fs-extra');
var clientSulla;
var userWhatsapp = [];
app.use(bodyParser.urlencoded({ extended: true }));
const sulla = require('@open-wa/wa-automate');
//-------------------SULLLA--------------------
const tasksFunctions = require('./routes/tasksFunctions');
const tasks = require('./routes/tasks');
app.use(cors());
//Definición de rutas staticas
app.use('/widget/sbs/', express.static('src/public'));
app.use('/widget/sbs/', express.static('node_modules'));
app.use('/widget/sbs/', express.static('build'));
app.use('/bothuman/sbs', express.static('dist'));
app.use(morgan('combined', { stream: logger.stream }));
app.use(express.json());
app.use('/widget/sbs/tasks', require('./routes/tasks'));

//arrays para guardado dinamico de estado agentes, chat pendientes y conexiones
var connections = [];
var chatsPendientes = [];
var chatsFinalizados = 0;
var allMsg = 0;
var agentOnline = [];
var supervisorOnline = [];
//Timer que controla el tiempo de agentes en los diferentes estados
var { Timer } = require('easytimer.js');
var axios = require('axios');
//Dependencia para Programador de tareas
var schedule = require('node-schedule');
var timeStart = undefined;
var timeStop = undefined;
var warning = undefined;
var ending = undefined;

var tmo = [];

const projectId = process.env.DIALOGFLOW_PROJECT_ID;

const pathUrlTask = process.env.PATH_URL_SERVER;

const tokenAmd = process.env.TOKEN_AMD;

const languageCode = 'en-Es';

(
    async function() {
        await tmoActual();
    }
)();
/*
  Tarea para definir horario de atención del día, se ejecuta a las 00:01 am de la mañana
*/
var j = schedule.scheduleJob("1 0 * * *", async() => {
    chatsPendientes = [];
    userWhatsapp = [];
    tmo = [];
    chatsFinalizados = 0;
    allMsg = 0;
    const data = await isHabilDay();
    timeStart = data.hourstart;
    timeStop = data.hourend;
    logger.info(
        "Horario de atención del día de hoy inicia;" +
        timeStart +
        " finalzia: " +
        timeStop
    );
});


setInterval(function() {
    io.sockets.emit('cola_chats', { chatP: chatsPendientes.length, chatF: chatsFinalizados, agents: agentOnline, allMsg: allMsg });
}, 1000)


io.sockets.on('connection', function(socket) {
    //Instancia de Timer para tiempo Online y Break
    var timerOnline = new Timer();
    var timerBreak = new Timer();
    var timeChat = new Timer();
    var timeChatFin = new Timer();

    connections.push(socket.id);
    //Escucha desconexion de usuario
    socket.on('disconnect', function() {
        try {
            let pos = searchIndexAgentSocket(socket.id);
            if (agentOnline[pos]) {
                agentOnline[pos].connect = false;
            }
        } catch (e) {
            logger.error('Error, no existe propiedad connect en array de objetos agentOnline: ' + e);
        }
        timerOnline.stop();
        timerBreak.stop();
        timeChat.stop();
        timeChatFin.stop();
        connections.splice(connections.indexOf(socket.id), 1);
    });
    /*
       Para manejo de tiempo supervisor
       */
    socket.on('Supervisor', function(room, payload) {
        if (payload.idSocketAgent) {
            socket.join(room);
            var result = supervisorOnline.find(soc => soc.idAgent === payload.idSocketAgent);
            if (result === undefined) {
                supervisorOnline.push({ idAgent: payload.idSocketAgent, numCon: payload.numeroCon, sockId: socket.id, state: payload.stateAgent, connect: true, nameAgent: payload.nameAgent, capacity: payload.capacity });
                let pos = searchIndexSuperv(payload.idSocketAgent);
                if (payload.stateAgent === 'ONLINE' || payload.stateAgent === 'OCUPADO') {
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    timerOnline.start();
                } else if (payload.stateAgent === 'BREAK') {
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    timerBreak.start();
                }
            } else if (result.state === 'ONLINE') {
                try {
                    let pos = searchIndexSuperv(payload.idSocketAgent);
                    supervisorOnline[pos].sockId = socket.id;
                    supervisorOnline[pos].connect = true;
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(supervisorOnline[pos].timerOnline);
                    precisionOnline(timerOnline, valueinSeconds)
                    socket.emit('timer_break', supervisorOnline[pos].timerBreak);
                } catch (e) {
                    logger.error('Error, no existe propiedad timerOnline en array de objetos agentOnline: ' + e);
                }
            } else if (result.state === 'OCUPADO') {
                try {
                    let pos = searchIndexSuperv(payload.idSocketAgent);
                    supervisorOnline[pos].sockId = socket.id;
                    supervisorOnline[pos].connect = true;
                    addEventimerOnlineSup(timerOnline, socket, pos)
                    addEvenTimeBreakSup(timerBreak, socket, pos)
                    var valueinSeconds = convertToInt(supervisorOnline[pos].timerOnline);
                    precisionOnline(timerOnline, valueinSeconds)
                    socket.emit('timer_break', supervisorOnline[pos].timerBreak);
                } catch (e) {
                    logger.error('Error, no existe propiedad timerOnline en array de objetos agentOnline: ' + e);
                }
            } else if (result.state === 'BREAK') {
                try {
                    let pos = searchIndexSuperv(payload.idSocketAgent);
                    supervisorOnline[pos].sockId = socket.id;
                    supervisorOnline[pos].connect = true;
                    addEventimerOnlineSup(timerOnline, socket, pos)
                    addEvenTimeBreakSup(timerBreak, socket, pos)
                    var valueinSeconds = convertToInt(supervisorOnline[pos].timerBreak);
                    precisionBreak(timerBreak, valueinSeconds);
                    socket.emit('timer_online', supervisorOnline[pos].timerOnline);
                } catch (e) {
                    logger.error('Error, no existe propiedad timerBreak en array de objetos agentOnline: ' + e);
                }
            }
            //Cada vez que el usuario cambia a estado ONLINE 
        } else if (payload.checkCola) {
            socket.join(room);
            let pos = searchIndexSuperv(room);
            try {
                if (payload.newStat) {
                    if (supervisorOnline[pos].timerOnline) {
                        var valueinSeconds = convertToInt(supervisorOnline[pos].timerOnline);
                        precisionOnline(timerOnline, valueinSeconds)
                    } else {
                        addEventimerOnlineSup(timerOnline, socket, pos)
                        timerOnline.start();
                    }
                    timerBreak.stop();
                    supervisorOnline[pos].state = payload.newStat;
                }
            } catch (e) {
                logger.error('Error, no existe propiedad State en array de objetos agentOnline: ' + e);
            }
            //Cada vez que el usuario cambia de estado
        } else if (payload.newState) {
            let pos = searchIndexSuperv(room);
            if (payload.newState != 'OFFLINE') {
                try {
                    supervisorOnline[pos].connect = true;
                    supervisorOnline[pos].state = payload.newState;
                    if (payload.newState != 'ONLINE') {
                        if (payload.newState == 'OCUPADO') {
                            if (supervisorOnline[pos].timerOnline) {
                                var valueinSeconds = convertToInt(supervisorOnline[pos].timerOnline);
                                timerBreak.stop();
                                precisionOnline(timerOnline, valueinSeconds)
                            } else {
                                addEventimerOnlineSup(timerOnline, socket, pos)
                                timerBreak.stop();
                                timerOnline.start();
                            }
                        } else if (supervisorOnline[pos].timerBreak) {
                            var valueinSeconds = convertToInt(supervisorOnline[pos].timerBreak);
                            timerOnline.stop();
                            precisionBreak(timerBreak, valueinSeconds);
                        } else {
                            addEvenTimeBreakSup(timerBreak, socket, pos)
                            timerOnline.stop();
                            timerBreak.start();
                        }
                    }
                } catch (e) {
                    logger.error('Error, no existe propiedad del array de objetos agentOnline: ' + e);
                }
            } else {
                supervisorOnline.splice(pos, 1);
                timerOnline.stop();
                timerBreak.stop();
            }
            //Cada vez que ingresa un nuevo usuario      
        }
    });
    //Socket para Asignamiento de conversaciones a los Agentes
    socket.on('Agent', async(room, payload) => {
        //cada vez que el usuario se conecta o que reinicia la página
        if (payload.idSocketAgent) {
            console.log("idSocketAgent")
            socket.join(room);
            var result = agentOnline.find(soc => soc.idAgent === payload.idSocketAgent);
            if (result === undefined) {
                agentOnline.push({ idAgent: payload.idSocketAgent, numCon: payload.numeroCon, sockId: socket.id, state: payload.stateAgent, connect: true, nameAgent: payload.nameAgent, capacity: payload.capacity, idAg: payload.id });
                let pos = searchIndexAgent(payload.idSocketAgent);
                if (payload.stateAgent === 'ONLINE' || payload.stateAgent === 'OCUPADO') {
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    timerOnline.start();
                } else if (payload.stateAgent === 'BREAK') {
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    timerBreak.start();
                }
            } else if (result.state === 'ONLINE') {
                try {
                    let pos = searchIndexAgent(payload.idSocketAgent);
                    agentOnline[pos].sockId = socket.id;
                    agentOnline[pos].connect = true;
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(agentOnline[pos].timerOnline);
                    precisionOnline(timerOnline, valueinSeconds)
                    socket.emit('timer_break', agentOnline[pos].timerBreak);
                    checkChatsCola(room, pos);
                } catch (e) {
                    logger.error('Error, no existe propiedad timerOnline en array de objetos agentOnline: ' + e);
                }
            } else if (result.state === 'OCUPADO') {
                try {
                    let pos = searchIndexAgent(payload.idSocketAgent);
                    agentOnline[pos].sockId = socket.id;
                    agentOnline[pos].connect = true;
                    addEventimerOnline(timerOnline, socket, pos)
                    addEvenTimeBreak(timerBreak, socket, pos)
                    var valueinSeconds = convertToInt(agentOnline[pos].timerOnline);
                    precisionOnline(timerOnline, valueinSeconds)
                    socket.emit('timer_break', agentOnline[pos].timerBreak);
                } catch (e) {
                    logger.error('Error, no existe propiedad timerOnline en array de objetos agentOnline: ' + e);
                }
            } else if (result.state === 'BREAK') {
                try {
                    let pos = searchIndexAgent(payload.idSocketAgent);
                    agentOnline[pos].sockId = socket.id;
                    agentOnline[pos].connect = true;
                    addEventimerOnline(timerOnline, socket, pos)
                    addEvenTimeBreak(timerBreak, socket, pos)
                    var valueinSeconds = convertToInt(agentOnline[pos].timerBreak);
                    precisionBreak(timerBreak, valueinSeconds);
                    socket.emit('timer_online', agentOnline[pos].timerOnline);
                } catch (e) {
                    logger.error('Error, no existe propiedad timerBreak en array de objetos agentOnline: ' + e);
                }
            }
            //Cada vez que el usuario cambia a estado ONLINE 
        } else if (payload.checkCola) {
            socket.join(room);
            let pos = searchIndexAgent(room);
            try {
                if (payload.newStat) {
                    if (agentOnline[pos].timerOnline) {
                        var valueinSeconds = convertToInt(agentOnline[pos].timerOnline);
                        precisionOnline(timerOnline, valueinSeconds)
                    } else {
                        addEventimerOnline(timerOnline, socket, pos)
                        timerOnline.start();
                    }
                    timerBreak.stop();
                    agentOnline[pos].state = payload.newStat;
                }
                checkChatsCola(room, pos);
            } catch (e) {
                logger.error('Error, no existe propiedad State en array de objetos agentOnline: ' + e);
            }
            //Cada vez que el usuario cambia de estado
        } else if (payload.newState) {
            let pos = searchIndexAgent(room);
            if (payload.newState != 'OFFLINE') {
                try {
                    agentOnline[pos].connect = true;
                    agentOnline[pos].state = payload.newState;
                    if (payload.newState != 'ONLINE') {
                        if (payload.newState == 'OCUPADO') {
                            if (agentOnline[pos].timerOnline) {
                                var valueinSeconds = convertToInt(agentOnline[pos].timerOnline);
                                timerBreak.stop();
                                precisionOnline(timerOnline, valueinSeconds)
                            } else {
                                addEventimerOnline(timerOnline, socket, pos)
                                timerBreak.stop();
                                timerOnline.start();
                            }
                        } else if (agentOnline[pos].timerBreak) {
                            var valueinSeconds = convertToInt(agentOnline[pos].timerBreak);
                            timerOnline.stop();
                            precisionBreak(timerBreak, valueinSeconds);
                        } else {
                            addEvenTimeBreak(timerBreak, socket, pos)
                            timerOnline.stop();
                            timerBreak.start();
                        }
                    }
                } catch (e) {
                    logger.error('Error, no existe propiedad del array de objetos agentOnline: ' + e);
                }
            } else {
                agentOnline.splice(pos, 1);
                timerOnline.stop();
                timerBreak.stop();
            }
            //Cada vez que ingresa un nuevo usuario      
        } else if (payload.id) {
            if (timeStart) {
                asignAgent(socket, payload)
            } else {
                const data = await isHabilDay();
                timeStart = data.hourstart;
                timeStop = data.hourend;
                logger.info(
                    "Horario de atención del día de hoy inicia;" +
                    timeStart +
                    " finalzia: " +
                    timeStop
                );
                asignAgent(socket, payload);
            }
        }
    });
    //Canal privado establecido entre Agente y Usuario
    socket.on('Agent-User', async(room, msg, stc, idcon, channel, who) => {
        //suscripción al canal
        socket.join(room);
        /* Reinicar bot cuando esta esperando agente */
        if (msg && msg.toLowerCase().includes('hablar con isa bot')) {
            let pos = searchIndexChatPen(room)
            if (pos !== -1) {
                socket.emit('new-message', { msg: 'Hola nuevamente, escribe menú para continuar', action: 'resetChat' });
            }
            chatsPendientes.splice(pos, 1)
        }
        /*TMO*/

        if (who === 'tmoReset') {
            timeChat.reset()
            timeChatFin.stop()
        } else if (who === 'tmo') {
            var resultTmo = tmo.find(soc => soc.id === room);
            if (resultTmo === undefined) {
                tmo.push({ id: room });
            }
            try {
                timeChat.start({ countdown: true, startValues: { seconds: warning } });
                let pos = searchIndexTmo(room);
                addEventimeChat(timeChat, timeChatFin, socket, pos, room, channel, idcon)
            } catch (e) {
                logger.error('Error: ' + e)
            }
        } else if (who == 'tmoFin') {
            timeChat.stop()
            timeChatFin.stop()
            timeChat = new Timer()
            timeChatFin = new Timer()
        }

        if (who === 'user' && msg == null) {
            var resultTmo = tmo.find(soc => soc.id === room);
            if (resultTmo !== undefined) {
                let pos = searchIndexTmo(room);
                var valueinSeconds = convertToInt(tmo[pos].time);
                precisionTmo(timeChat, valueinSeconds)
                addEventimeChat(timeChat, timeChatFin, socket, pos, room, channel, idcon)
            }
        }
        /*TMO*/
        if (stc == null || stc == 2) {
            if (msg != null) {
                allMsg += 1;
            }
            if (channel === 'webchat') {
                socket.broadcast.to(room).emit('private-message', { socketIdUser: room, messageUser: msg, state: stc, idCon: idcon, who: who });
            } else if (channel === 'whatsapp' && stc === 2) {
                let pos = searchIndexWhatsapp(room);
                userWhatsapp[pos].idCon = idcon;
                /*TMO*/
                var resultTmo = tmo.find(soc => soc.id === room);
                if (resultTmo === undefined) {
                    tmo.push({ id: userWhatsapp[pos].idSocket });
                }
                try {
                    userWhatsapp[pos].timeChat.start({ countdown: true, startValues: { seconds: warning } });
                    let posTmo = searchIndexTmo(userWhatsapp[pos].idSocket);
                    addEventimeChat(userWhatsapp[pos].timeChat, userWhatsapp[pos].timeChatFin, null, posTmo, userWhatsapp[pos].idSocket, 'whatsapp', userWhatsapp[pos].idCon)
                } catch (e) {
                    logger.error('Error: ' + e)
                }
                /*TMO*/
                if (msg) {
                    try {
                        await tasksFunctions.saveMessageAgent(
                            userWhatsapp[pos].jwt,
                            userWhatsapp[pos].id,
                            userWhatsapp[pos].idCon,
                            msg,
                            "agent"
                        )
                        console.log("Usuarios WA: ", userWhatsapp)
                        console.log("Este es el room: ", room);
                        utilMessages.sendMessage(
                            userWhatsapp[pos].idSocket, msg
                        );
                        /* enviar(room, msg); */
                        /* start(clientSulla, room, msg); */
                    } catch (error) {
                        console.error(error);
                    }
                }
            } else if (channel === 'whatsapp' && stc === null) {
                if (msg) {
                    let pos = searchIndexWhatsapp(room);
                    socket.broadcast.to(room).emit('private-message', { socketIdUser: room, messageUser: msg, state: stc, idCon: idcon, who: who });
                    userWhatsapp[pos].timeChat.reset()
                    userWhatsapp[pos].timeChatFin.stop()
                    console.log("Usuarios WA: ", userWhatsapp)
                    console.log("Este es el room: ", room);
                    utilMessages.sendMessage(
                        userWhatsapp[pos].idSocket, msg
                    );
                    /* enviar(room, msg); */
                    /* start(clientSulla, room, msg) */
                    ;
                }
            }
        } else if (stc == 1) {
            chatsFinalizados += 1;
            if (channel === 'webchat') {
                socket.broadcast.to(room).emit('private-message', { socketIdUser: room, messageUser: 'Tu conversación con el agente a finalizado', state: stc, idCon: idcon });
            } else if (channel === 'whatsapp') {
                let pos = searchIndexWhatsapp(room);
                userWhatsapp[pos].bot = false;
                userWhatsapp[pos].timeChat.stop()
                userWhatsapp[pos].timeChatFin.stop()
                userWhatsapp[pos].timeChat = new Timer();
                userWhatsapp[pos].timeChatFin = new Timer();
                console.log("Usuarios WA: ", userWhatsapp)
                console.log("Este es el room: ", room);
                utilMessages.sendMessage(
                    userWhatsapp[pos].idSocket, 'Tu conversación con el agente a finalizado'
                );
                /* enviar(room, 'Tu conversación con el agente a finalizado'); */
                /* start(clientSulla, room, 'Tu conversación con el agente a finalizado'); */
            }
            let pos = searchIndexAgent(msg);
            try {
                if (agentOnline[pos].numCon > 0) {
                    agentOnline[pos].numCon -= 1;
                }
            } catch {
                logger.error('Error, no existe propiedad numCon en array de objetos agentOnline');
            }
        }
    });
    //Nuevo horario
    socket.on('new_office_hour', async(data) => {
            switch (data) {
                case 1:
                    const resp = await isHabilDay();
                    timeStart = resp.hourstart;
                    timeStop = resp.hourend;
                    logger.info(
                        "Nuevo Horario de atención del día de hoy inicia;" +
                        timeStart +
                        " finalzia: " +
                        timeStop
                    );
                    break;
            }
        })
        //Nueva Capacidad Agente
    socket.on('new_capacity', function(data) {
            try {
                let pos = searchIndexAgent(data.agent)
                if (pos != -1) {
                    agentOnline[pos].capacity = data.newCap
                    io.sockets.emit('new_info', { agent: data.agent, capacity: data.newCap })
                }
            } catch (e) {
                logger.error('Error, no existe propiedad numCon en array de objetos agentOnline' + e);
            }
        })
        //Nuevo TMO
    socket.on('new_tmo', function(data) {
        try {
            warning = data.warning * 60;
            ending = data.ending * 60;
        } catch (e) {
            logger.error('Error, no existe propiedad numCon en array de objetos agentOnline' + e);
        }
    })


    //Mensajes entre Dialogflow y Usuario
    socket.on('new-message', async function(data) {
        var sessionId = '{0.id}'.format(data);
        var message = data.message;

        const obj = await detectTextIntent(projectId, sessionId, message, languageCode);
        try {
            var actionQuery = obj[0].queryResult.action;

            /*Para sumar al count de widget polymer
            if(actionQuery == 'default'){
              socket.emit('estado', 'nuevo');
            }*/

            if (actionQuery == 'equipos') {
                socket.emit("new-message", {
                    msg: obj[0].queryResult.fulfillmentText,
                    action: actionQuery
                });
            } else if (actionQuery == 'human') {
                socket.emit('estado', 'agente');
            }
            var mensajes = obj[0].queryResult.fulfillmentMessages;
            mensajes.forEach(function(e) {
                if (e.platform === 'ACTIONS_ON_GOOGLE') {
                    try {
                        let checker = comprobarSuggestion(actionQuery)

                        if (checker) {
                            let messag = handlerSuggestions(actionQuery)
                            let preguntas = messag.preguntas
                            let estaAccion = messag.actionQuery
                            socket.emit('new-message', { sug: preguntas, action: estaAccion });
                        } else if (e.basicCard) {
                            const imagen = e.basicCard.image.imageUri
                            socket.emit('new-message', { image: imagen, action: actionQuery })
                            console.log(imagen)
                        } else {
                            var textToSpeech = e.simpleResponses.simpleResponses[0].textToSpeech;
                            socket.emit('new-message', { msg: textToSpeech, action: actionQuery });
                        }

                    } catch {
                        if (actionQuery == 'input.welcome') {
                            var servicios = [
                                { "title": "SI" },
                                { "title": "NO" }
                            ];

                            socket.emit('new-message', { sug: servicios, action: actionQuery });
                        } else {
                            var suggestions = e.suggestions.suggestions;
                            socket.emit('new-message', { sug: suggestions, action: actionQuery });
                        }
                    }
                } else if (e.platform === 'KIK') {
                    if (actionQuery == 'Cliente_si_datos_nombre' && e.text.text[0] == 'Bienvenido, selecciona una de las siguientes opciones :') {
                        let servicios = [
                            { "title": "Información General" },
                            { "title": "Servicio Técnico" },
                            { "title": "Facturación" },
                            { "title": "Mi Plan" }
                        ];
                        console.log(e.text.text[0])
                        socket.emit('new-message', { sug: servicios, action: actionQuery });
                    }
                    var textToSpeech = e.text.text[0];
                    socket.emit('new-message', { msg: textToSpeech, action: actionQuery });
                }
            });
        } catch {
            logger.error('Error, no hay resultado del API de Dialogflow');
        }
    });
});
/*
  Determina el puerto por el cual se va desplegar la aplicación
*/
server.listen(5000, function() {
    logger.info('Server On en https://dashbots.outsourcingcos.com/widget/sbs');
});

/*
  Funcion para poder imprimir las suggestions en el las preguntas frecuentes
*/

function comprobarSuggestion(actionQuery) {
    let a = 'Cliente_si_cuenta_cobranza'
    let b = 'Cliente_si_pf_Atencion_Cliente'
    let c = 'Cliente_si_pf_Planes_Datos'
    let d = 'Cliente_si_pf_Servicio_Tecnico'
    let e = "Cliente_si_soporte_modem"
    if (actionQuery == a || actionQuery == b || actionQuery == c || actionQuery == d || actionQuery == e) {
        return true
    } else {
        return false
    }
}

function handlerSuggestions(actionQuery) {
    if (actionQuery == 'Cliente_si_cuenta_cobranza') {
        let preguntas = [
            { "title": "¿Cómo funciona el pago del cargo de conexión mediante cupón de pago?" },
            { "title": "¿En qué consiste el pago del cargo de activación mediante cupón de pago?" },
            { "title": "¿Cómo funciona el pago de la factura/cupón de pago de HughesNet en el banco?" },
            { "title": "¿Cómo cambiar la forma de pago para débito en cuenta corriente (débito automático)?" },
            { "title": "¿Cómo obtengo una copia de la factura? REALIZAR CORREO" },
            { "title": "¿Cómo puedo cambiar mi dirección de facturación?" },
            { "title": "¿Cómo puedo pagar mi factura?" },
            { "title": "¿Cuál es la fecha de vencimiento de la factura?" },
            { "title": "¿Cómo es el cobro, yo primero uso y después pago?" },
            { "title": "Si cambio mi plan a mitad del mes, ¿Cómo es el cobro en mi factura?" },
            { "title": "¿En qué bancos puedo pagar la factura/boleto de pago de HughesNet?" },
            { "title": "Menú" },
            { "title": "Info general" }
        ]
        return { preguntas, actionQuery }
    } else if (actionQuery == 'Cliente_si_pf_Atencion_Cliente') {
        let preguntas = [
            { "title": "¿En qué sitio web puede ver la velocidad de mi internet?" },
            { "title": "No quiero llamar a Servicio al Cliente, prefiero que el contacto se haga por correo electrónico ¿Cómo debo proceder?" },
            { "title": "Quiero saber cuál es mi número de cuenta de suscriptor, ¿Dónde lo puedo encontrar?" },
            { "title": "¿Cómo puedo cambiar mi dirección de instalación? " },
            { "title": "¿Puedo tener más de una cuenta con mi número de identificación (CC) en direcciones diferentes?" },
            { "title": "Menú" },
            { "title": "Info General" }
        ]
        return { preguntas, actionQuery }
    } else if (actionQuery == 'Cliente_si_pf_Planes_Datos') {
        let preguntas = [
            { "title": "¿Cuáles son mis opciones de planes de datos?" },
            { "title": "¿Cómo funciona el plan de datos? " },
            { "title": "¿Puedo aumentar mi plan de datos?" },
            { "title": "¿Cuál es la regla de uso de la Capacidad?" },
            { "title": "No entiendo el asunto de la reducción de la velocidad, ¿cómo funciona? " },
            { "title": "¿Por qué comprar una recarga adicional?" },
            { "title": "¿Qué consume el plan de datos?" },
            { "title": "¿Cómo consultar cuánto he consumido del plan de datos?" },
            { "title": "¿Compré una recarga adicional, en qué tiempo estará disponible para uso?" },
            { "title": "¿Cómo puedo restablecer la velocidad de mi plan?" },
            { "title": "¿Existe algún sitio donde encontrar la información de mi plan y suscripción?" },
            { "title": "¿Puedo cambiar mi plan en cualquier momento?" },
            { "title": "Necesito suspender mi plan por un tiempo, ¿Cómo procedo?" },
            { "title": "¿Por qué hay plan de datos en el acceso a internet de banda ancha satelital? " },
            { "title": "Cuál es la regla de uso de la Franja Horaria de Datos Extra? " },
            { "title": "Menú" },
            { "title": "Info General" }
        ]
        return { preguntas, actionQuery }
    } else if (actionQuery == 'Cliente_si_pf_Servicio_Tecnico') {
        let preguntas = [
            { "title": "¿Necesito ponerme en contacto con el técnico, ¿Dónde encuentro esa información?" },
            { "title": "¿Necesito una línea telefónica o un módem?" },
            { "title": "¿Internet funciona incluso durante lluvia intensa o tiempo nublado?" },
            { "title": "¿La conexión de banda ancha siempre está disponible?" },
            { "title": "Estoy sin señal, ¿Cómo lo resuelvo?" },
            { "title": "En cuanto al lugar, ¿existe alguna especificación técnica para realizar la instalación?" },
            { "title": "¿Internet de HughesNet funciona con VPN?" },
            { "title": "¿Puedo jugar en línea tranquilamente con HughesNet?" },
            { "title": "¿Es posible realizar monitoreo con cámaras a través de una red HughesNet?" },
            { "title": "¿El servicio de acceso a Internet de HughesNet funciona incluso durante lluvia intensa o tiempo nublado?" },
            { "title": "¿Usted está tratando de hacer la conexión a través de Wi-Fi o el cable de red conectado al módem HughesNet o enrutador?" },
            { "title": "¿Ha realizado la limpieza de los datos, historiales y caché de su navegador de internet (Google Chrome, Internet Explorer, Firefox, Safari, etc.)?" },
            { "title": "¿Ha intentado hacer las pruebas utilizando otro equipo (de escritorio o portátil)?" },
            { "title": "¿Cómo funciona la instalación?" },
            { "title": "Menú" },
            { "title": "Info General" }
        ]
        return { preguntas, actionQuery }
    } else if (actionQuery == 'Cliente_si_soporte_modem') {
        let preguntas = [
            { "title": "Enlaces de indicadores" },
            { "title": "Recomendaciones" },
            { "title": "Barra de parámetros" },
            { "title": "Enlaces e información del panel central" },
            { "title": "Centro de control del sistema" },
            { "title": "Panel lateral" },
            { "title": "Información de contacto" },
            { "title": "Prueba de conectividad" },
            { "title": "Auto prueba interna" },
            { "title": "¿Cómo conecto mis dispositivos al módem WIfi-Hughesnet?" },
            { "title": "Configuración de Wi-Fi protegido (Wps- Wifi protected setup)" },
            { "title": "Detección y solución de problemas" },
            { "title": "Menú" },
            { "title": "Info General" }
        ]
        return { preguntas, actionQuery }
    }


}

/*
 Función para detectar intentos de Dialogflow y retornar queryResult
*/
function detectTextIntent(projectId, sessionId, queries, languageCode) {
    try {
        // [START dialogflow_detect_intent_text]
        // Imports the Dialogflow library
        const dialogflow = require('dialogflow');
        // Instantiates a session client
        const sessionClient = new dialogflow.SessionsClient();
        // The path to identify the agent that owns the created intent.
        const sessionPath = sessionClient.sessionPath(projectId, sessionId);
        console.log("Este es el seesionPath: ", sessionPath);
        //variable for save object response from Dialogflow
        let response;
        // The text query request.
        const request = {
            session: sessionPath,
            queryInput: {
                text: {
                    text: queries,
                    languageCode: languageCode
                }
            }
        };

        console.log('request', request)

        response = sessionClient.detectIntent(request);
        //return object with queryResult

        return response;
    } catch (error) {
        console.log(error);
    }

}
/*
 Función para buscar indice en array de agentes segun valor del Agent ID
*/
function searchIndexAgent(value) {
    pos = agentOnline.findIndex(ele => ele.idAgent === value);
    return pos;
}
/*
 Función para buscar indice en array de agentes segun valor del Socket ID
*/
function searchIndexAgentSocket(value) {
    pos = agentOnline.findIndex(ele => ele.sockId === value);
    return pos;
}
/*
Función para buscar indice en array de chats pendientes segun el socketid
+*/
function searchIndexChatPen(value) {
    pos = chatsPendientes.findIndex(ele => ele.socketId === value);
    return pos;
}
/*
 Función para buscar indice en array de supervisor segun valor del Agent ID
*/
function searchIndexSuperv(value) {
    pos = supervisorOnline.findIndex(ele => ele.idAgent === value);
    return pos;
}
/*
 Función para buscar indice en array de agentes segun valor del Agent ID
*/
function searchIndexTmo(value) {
    pos = tmo.findIndex(ele => ele.id === value);
    return pos;
}
/*
 Función para buscar indice en array de supervisor segun valor del Socket ID
*/
function searchIndexAgentSuperv(value) {
    pos = supervisorOnline.findIndex(ele => ele.sockId === value);
    return pos;
}
/*
 Función para buscar indice en array de agentes, con menos conversaciones
*/
function findMinCon(arr) {
    let min = arr[0].numCon,
        ind = 0,
        v;
    for (let i = 1, len = arr.length; i < len; i++) {
        v = arr[i].numCon;
        (v < min) ? (min = v, ind = i) : (min = min, ind);
    }
    return ind;
}
/*
  Funcion para convertir formato de hora-minutos-segundos hh:mm:ss a segundos
*/
function convertToInt(num) {
    try {
        let arr = num.split(":");
        let hr = parseInt(arr[0]);
        let min = parseInt(arr[1]);
        let seg = parseInt(arr[2]);
        let result = hr * 3600 + min * 60 + seg;
        return result;
    } catch {
        logger.error('Error, no existe array para hacerle split');
    }
}
/*
  Funcion para convertir segundos a formato de hora-minutos hh:mm am/pm
*/
function convertToHour(num) {
    try {
        var hours = Math.floor(num / 3600);
        var minutes = Math.floor(num % 3600 / 60);
        if (hours < 10)
            hours = "0" + hours;
        if (minutes < 10)
            minutes = "0" + minutes;
        if (hours > 12) {
            hours = hours - 12
            apm = "pm"
        } else if (hours == 12) {
            apm = "pm"
        } else {
            apm = "am"
        }
        return hours + ":" + minutes + " " + apm
    } catch {
        logger.error('Error, no existe array para hacerle split');
    }
}
/*
  Funcion para obtener formato de año-mes-dia yyyy:mm:dd
*/
function nowDate() {
    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = (date.getMonth() + 1);
    if (gg < 10)
        gg = "0" + gg;
    if (mm < 10)
        mm = "0" + mm;
    var cur_day = aaaa + "-" + mm + "-" + gg;
    return cur_day;
}
/*
  Funcion para obtener formato de hora-minutos-segundos hh:mm:ss
*/
function nowHour() {
    var date = new Date();
    var hours = date.getHours()
    var minutes = date.getMinutes()
    var seconds = date.getSeconds();
    if (hours < 10)
        hours = "0" + hours;
    if (minutes < 10)
        minutes = "0" + minutes;
    if (seconds < 10)
        seconds = "0" + seconds;
    return hours + ":" + minutes + ":" + seconds;
}
/*
  Funcion para llamada de horario de atención del día
*/
async function isHabilDay() {
    try {
        const data = await tasksFunctions.isDayHabil(nowDate());
        return data[0];
    } catch (error) {
        logger.error(
            "Error, no existe propiedad numCon en array de objetos agentOnline: " +
            error
        );
        return null;
    }
}
/*
  Funcion para llamada de horario de atención del día
*/
async function tmoActual() {
    try {
        const data = await tasksFunctions.tmo();
        warning = data[0].warning * 60;
        ending = data[0].ending * 60;
    } catch (error) {
        logger.error(error);
    }
}
/*
  Funcion para asignamiento de agentes, según horario de atención
*/
function asignAgent(socket, payload) {
    var hour = convertToInt(nowHour());
    if (hour >= timeStart && hour <= timeStop) {
        var result = agentOnline.filter(soc => soc.state === 'ONLINE' && soc.connect === true && soc.numCon < soc.capacity);
        if (result === undefined || result.length === 0) {
            socket.emit('new-message', { msg: 'En este momento no tenemos agentes disponibles, por favor espera un momento. Si deseas hablar nuevamente con el bot escribe *hablar con isa bot*', action: null });
            chatsPendientes.push(payload);
            socket.emit('messages', null);
        } else {
            try {
                let x = findMinCon(result);
                socket.broadcast.to(result[x].idAgent).emit('event', payload, 'webchat');
                let pos = searchIndexAgent(result[x].idAgent);
                agentOnline[pos].numCon += 1;
            } catch (e) {
                logger.error('Error asignando conversacion al agente: ' + e);
            }
        }
    } else {
        socket.emit('new-message', { msg: 'En este momento estamos fuera del horario de atención, podras comunicarte con un agente de ' + convertToHour(timeStart) + ' hasta ' + convertToHour(timeStop) + '.\nSi deseas, puedes seguir hablando con el bot', action: 'resetChat' });
    }
}
/*
  Función para asignar Chat en Cola al agente que finaliza conversación
*/
function checkChatsCola(room, pos) {
    let capacity = agentOnline[pos].capacity
    let conversation = agentOnline[pos].numCon
    let capTot = capacity - conversation
    if (chatsPendientes.length > 0 && conversation < capacity) {
        chatsPendientes.slice(0, capTot).forEach((e) => {
            io.in(room).emit('event', e, e.channel);
            try {
                agentOnline[pos].numCon += 1;
            } catch (e) {
                logger.error('Error, no existe propiedad numCon en array de objetos agentOnline: ' + e);
            }
        });
        chatsPendientes.splice(0, capTot);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEventimerOnline(timerOnline, socket, pos) {
    try {
        timerOnline.addEventListener('secondsUpdated', function(e) {
            agentOnline[pos].timerOnline = timerOnline.getTimeValues().toString();
            socket.emit('timer_online', timerOnline.getTimeValues().toString());
        });
    } catch (e) {
        logger.error('Error añaniendo evento timer: ' + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEvenTimeBreak(timerBreak, socket, pos) {
    try {
        timerBreak.addEventListener('secondsUpdated', function(e) {
            agentOnline[pos].timerBreak = timerBreak.getTimeValues().toString();
            socket.emit('timer_break', timerBreak.getTimeValues().toString());
        });
    } catch (e) {
        logger.error('Error añaniendo evento timer: ' + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEventimerOnlineSup(timerOnline, socket, pos) {
    try {
        timerOnline.addEventListener('secondsUpdated', function(e) {
            supervisorOnline[pos].timerOnline = timerOnline.getTimeValues().toString();
            socket.emit('timer_online', timerOnline.getTimeValues().toString());
        });
    } catch (e) {
        logger.error('Error añaniendo evento timer: ' + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEvenTimeBreakSup(timerBreak, socket, pos) {
    try {
        timerBreak.addEventListener('secondsUpdated', function(e) {
            supervisorOnline[pos].timerBreak = timerBreak.getTimeValues().toString();
            socket.emit('timer_break', timerBreak.getTimeValues().toString());
        });
    } catch (e) {
        logger.error('Error añaniendo evento timer: ' + e);
    }
}
/*
  Función para iniciar el contador de tiempo Online en los segundos precisos que estaba
*/
function precisionOnline(timerOnline, valueinSeconds) {
    try {
        timerOnline.start({
            precision: "seconds",
            startValues: { seconds: valueinSeconds }
        });
    } catch (e) {
        logger.error('Error iniciando timer: ' + e);
    }
}
/*
  Función para iniciar el contador de tiempo en Break los segundos precisos que estaba
*/
function precisionBreak(timerBreak, valueinSeconds) {
    try {
        timerBreak.start({
            precision: "seconds",
            startValues: { seconds: valueinSeconds }
        });
    } catch (e) {
        logger.error('Error iniciando timer: ' + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEventimeChat(timeC, timeCFin, socket, pos, room, channel, idCon) {
    try {
        tmo[pos].time = timeC.getTimeValues().toString()
        timeC.addEventListener('secondsUpdated', function(e) {
            tmo[pos].time = timeC.getTimeValues().toString()
        });
        timeC.addEventListener('reset', function(e) {
            tmo[pos].time = timeC.getTimeValues().toString()
        });
        timeC.addEventListener('targetAchieved', function(e) {
            io.in(room).emit('private-message', { socketIdUser: room, messageUser: 'Tu conversación se finalizará en ' + ending / 60 + ' minuto(s) si no respondes algo', state: 4, idCon: idCon, who: 'agent' });
            timeC.stop();
            if (channel === 'webchat') {
                timeCFin.start({ countdown: true, startValues: { seconds: ending } });
                let pos = searchIndexTmo(room);
                addEventimeChatFin(timeCFin, socket, pos, room, channel, idCon)
            } else if (channel === 'whatsapp') {
                console.log("Usuarios WA: ", userWhatsapp)
                console.log("Este es el room: ", room);
                utilMessages.sendMessage(
                    userWhatsapp[pos].idSocket, 'Tu conversación se finalizará en ' + ending / 60 + ' minuto(s) si no respondes algo'
                );
                /* enviar(room, 'Tu conversación se finalizará en ' + ending / 60 + ' minuto(s) si no respondes algo'); */
                /* start(clientSulla, room, 'Tu conversación se finalizará en ' + ending / 60 + ' minuto(s) si no respondes algo'); */
                timeCFin.start({ countdown: true, startValues: { seconds: ending } });
                let pos = searchIndexTmo(room);
                addEventimeChatFin(timeCFin, socket, pos, room, channel, idCon)
            }
        });
    } catch (e) {
        logger.error('Error añaniendo evento timer: ' + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEventimeChatFin(timeCF, socket, pos, room, channel, idCon) {
    try {
        tmo[pos].timeFin = timeCF.getTimeValues().toString()
        timeCF.addEventListener('secondsUpdated', function(e) {
            tmo[pos].timeFin = timeCF.getTimeValues().toString()
        });
        timeCF.addEventListener('reset', function(e) {
            tmo[pos].timeFin = timeCF.getTimeValues().toString()
        });
        timeCF.addEventListener('targetAchieved', function(e) {
            if (channel === 'webchat') {
                io.in(room).emit('private-message', { socketIdUser: room, messageUser: null, state: 5, idCon: idCon, who: 'agent', channel: channel });
            } else if (channel === 'whatsapp') {
                io.in(room).emit('private-message', { socketIdUser: room, messageUser: null, state: 5, idCon: idCon, who: 'agent', channel: channel });
                let posWha = searchIndexWhatsapp(room);
                userWhatsapp[posWha].bot = false;
                userWhatsapp[posWha].timeChat.stop();
                userWhatsapp[posWha].timeChatFin.stop();
                userWhatsapp[posWha].timeChat = new Timer();
                userWhatsapp[posWha].timeChatFin = new Timer();
            }
        });
    } catch (e) {
        logger.error('Error añaniendo evento timer: ' + e);
    }
}
/*
  Función para iniciar el contador de tiempo en chat los segundos precisos que estaba
*/
function precisionTmo(timeC, valueinSeconds) {
    try {
        timeC.start({
            countdown: true,
            startValues: { seconds: valueinSeconds }
        });
    } catch (e) {
        logger.error('Error iniciando timer: ' + e);
    }
}

/***********************************************************************************************************
 *********************************************** SULLA  *****************************************************
 ***********************************************************************************************************/

/*
    Ejecución de cliente de sulla para escuchar mensajes
*/

console.log("hola1")

app.post("/widget/sbs/tasks/messages", (req, res) => {
    console.log("hola2")
    console.log(req.body)
    let receivedMessages = req.body.results; // Received messages from Whatsapp
    var fromWha = req.body.results[0].from;
    var msgWha = req.body.results[0].message.text;
    console.log(msgWha);
    receivedMessages.forEach(async receivedMsg => {
        var messagesToSend = await utilMessages.processMessage(
            receivedMsg.from,
            receivedMsg.message.text
        );
        messagesToSend.forEach(msg => {
            utilMessages.sendMessage(
                receivedMsg.from,
                msg
            ); // Envia mensaje a Whatsapp
        });
    });
    enviar(fromWha, msgWha);
    res.send("Entry point working");
});


async function enviar(from, message) {

    logger.info('Enviando mensajes de WhatsApp')

    let idUserWa = from;
    let nameWa = from;
    let messWa = message;
    let result = userWhatsapp.find(soc => soc.idSocket === idUserWa);
    console.log('Sergio y Javier no creen: ', messWa);
    let a = await checkandSave(result, idUserWa, nameWa);

    let pos = searchIndexWhatsapp(idUserWa);

    if (result === undefined) {
        DialogFlow(pos, idUserWa, messWa);
    } else {
        if (result.bot === true) {
            console.log("Dialogflow");
            DialogFlow(pos, idUserWa, messWa);
        } else {
            console.log("BotHuman");
            BotHuman(pos, idUserWa, messWa);
        }
    }
};

async function DialogFlow(pos, idUserWa, messWa) {
    try {
        await tasksFunctions.saveMessage(
            userWhatsapp[pos].jwt,
            userWhatsapp[pos].id,
            messWa,
            "user"
        )
        console.log('mesWa', messWa)
        var sessionId = idUserWa.format(messWa);
        console.log("Este es el sessionId: ", sessionId);
        var messages = messWa;
        const obj = await detectTextIntent(projectId, sessionId, messages, languageCode);
        var actionQuery = obj[0].queryResult.action;
    } catch (error) {
        console.error(error);
    }
    /*
        Asignación de agente desde whatsapp
    */
    if (actionQuery == 'human') {
        if (timeStart) {
            userWhatsapp[pos].bot = true;
            asignAgentWhat(idUserWa, { id: userWhatsapp[pos].id, socketId: idUserWa, channel: 'whatsapp' });
        } else {
            const data = await isHabilDay();
            timeStart = data.hourstart;
            timeStop = data.hourend;
            logger.info(
                "Horario de atención del día de hoy inicia;" +
                timeStart +
                " finalzia: " +
                timeStop
            );
            userWhatsapp[pos].bot = true;
            asignAgentWhat(idUserWa, {
                id: userWhatsapp[pos].id,
                socketId: idUserWa,
                channel: "whatsapp"
            });
        }
    }
    try {
        var mensajes = obj[0].queryResult.fulfillmentMessages;
        mensajes.forEach(async(e) => {
            if (e.platform === 'PLATFORM_UNSPECIFIED') {
                try {
                    let textWa = e.text.text[0];
                    if (textWa) {
                        try {
                            await tasksFunctions.saveMessage(
                                userWhatsapp[pos].jwt,
                                userWhatsapp[pos].id,
                                textWa,
                                "bot",
                            );
                        } catch (error) {
                            console.error(error);
                        }
                    }
                    utilMessages.sendMessage(
                        idUserWa, textWa
                    );
                } catch (error) {
                    logger.error(error);
                    logger.error('Error, intentando guardar mensaje');
                }
            }
        });
    } catch (error) {
        logger.error(error);
        logger.error('Error, no hay resultado del API de Dialogflow');
    }
}

async function BotHuman(pos, idUserWa, messWa) {
    let sockidfor = userWhatsapp[pos].idSocket;
    userWhatsapp[pos].timeChat.reset()
    userWhatsapp[pos].timeChatFin.stop()
    if (messWa && messWa.toLowerCase().includes('hablar con isa bot')) {
        let pos = searchIndexChatPen(sockidfor)
        if (pos !== -1) {
            userWhatsapp[pos].bot = false;
            utilMessages.sendMessage(
                idUserWa, 'Hola nuevamente, escribe menú para continuar'
            );
        }
        chatsPendientes.splice(pos, 1)
    }
    try {
        await tasksFunctions.saveMessageAgent(
            userWhatsapp[pos].jwt,
            userWhatsapp[pos].id,
            userWhatsapp[pos].idCon,
            messWa,
            "user"
        )
        io.sockets.in(userWhatsapp[pos].idSocket).emit("private-message", {
            socketIdUser: userWhatsapp[pos].idSocket,
            messageUser: messWa,
            idCon: userWhatsapp[pos].idCon
        });
    } catch (error) {
        logger.error(error);
    }
}


/*
Guardado de Usuario en Base de Datos
*/
async function checkandSave(result, idUserWa, nameWa) {
    if (result === undefined) {
        try {
            const data = await tasksFunctions.callUserWhatsapp(tokenAmd, idUserWa);
            if (data) {
                let idUs = data.id;
                const result = await tasksFunctions.reloadUserWa(tokenAmd, nameWa, idUserWa);
                console.log("Esta es result: ", result)
                userWhatsapp.push({
                    id: idUs,
                    name: nameWa,
                    idSocket: idUserWa,
                    idCon: 0,
                    jwt: result.token,
                    bot: false,
                    timeChat: new Timer(),
                    timeChatFin: new Timer()
                })
            } else {
                const resultOfSave = await tasksFunctions.saveUser(nameWa, null, null, idUserWa, null);
                userWhatsapp.push({
                    id: resultOfSave.data[0].id,
                    name: nameWa,
                    idSocket: idUserWa,
                    idCon: 0,
                    jwt: resultOfSave.token,
                    bot: false,
                    timeChat: new Timer(),
                    timeChatFin: new Timer()
                })
            }
        } catch (error) {
            console.error(error);
        }
    }
}
/*
  envio de mensajes con cliente Sulla
*/
function start(client, idUserWa, messWa) {
    client.sendText(idUserWa, messWa);
}
/*
Busqueda de indice en array de usuarios Whatsapp
*/
function searchIndexWhatsapp(value) {
    pos = userWhatsapp.findIndex(ele => ele.idSocket === value);
    return pos;
}
/*
  Asignamiento agente para Whatsapp
*/
function asignAgentWhat(authorMess, payload) {
    var hour = convertToInt(nowHour());
    if (hour >= timeStart && hour <= timeStop) {
        var result = agentOnline.filter(soc => soc.state === 'ONLINE' && soc.connect === true && soc.numCon < soc.capacity);
        if (result === undefined || result.length === 0) {
            var author = authorMess;
            var text = "En este momento no tenemos agentes disponibles, por favor espera un momento. Si deseas hablar nuevamente con el bot escribe *hablar con isa bot*"
            chatsPendientes.push(payload)
            utilMessages.sendMessage(
                author, text
            );
            /*
            enviar(author, text);
            setTimeout(function() {
                start(clientSulla, author, text);
            }, 2500) 
            */
        } else {
            try {
                let x = findMinCon(result);
                io.sockets.in(result[x].idAgent).emit('event', payload, 'whatsapp');
                let pos = searchIndexAgent(result[x].idAgent);
                agentOnline[pos].numCon += 1;
            } catch (e) {
                logger.error('Error asignando conversacion al agente: ' + e);
            }
        }
    } else {
        var author = authorMess;
        let pos = searchIndexWhatsapp(authorMess)
        userWhatsapp[pos].bot = false;
        var text = 'En este momento estamos fuera del horario de atención, podras comunicarte con un agente de ' + convertToHour(timeStart) + ' hasta ' + convertToHour(timeStop) + '.\nSi deseas, puedes seguir hablando con el bot';
        utilMessages.sendMessage(
            author, text
        );
        /*
        enviar(author, text);
        setTimeout(function() {
            start(clientSulla, author, text);
        }, 2500) 
        */
    }
}

/*****************************************************************/