const axios = require("axios");

const bot = require("../../bot");

async function processMessage(from, message) {
    try {
        let sessionId = `447491163530-${from}`;
        let receivedMessage = message;
        let intent = await bot(sessionId, receivedMessage);
        let responseOfDialogflow = intent[0].queryResult.fulfillmentMessages;
        var actionQuery = intent[0].queryResult.action;
        let messagesToSend = [];
        console.log("Este es el actionQuery: ", actionQuery)
        console.log("Este es el intent[0].queryResult: ", intent[0].queryResult)

        if (actionQuery === "human") {
            console.log("Funcionaaaaaaaa");
        } else {
            responseOfDialogflow.forEach(element => {
                if (element.platform === "PLATFORM_UNSPECIFIED") {
                    messagesToSend.push(element.text.text[0])
                }
            });
        }

        console.log(responseOfDialogflow);
        return messagesToSend;
    } catch (error) {
        console.error(error);
        return [];
    }
}

function sendMessage(to, message) {
    axios
        .post(
            "https://192ypn.api.infobip.com/omni/1/advanced",
            JSON.stringify({
                scenarioKey: "D88E215790A18F35C9BC90DA2915E287",
                destinations: [{
                    to: {
                        phoneNumber: to
                    }
                }],
                whatsApp: {
                    text: message
                },
                sms: {
                    text: message
                }
            }), {
                headers: {
                    "Authorization": "Basic R3VzdGF2b19UZXN0QWNjOkNvczIwMjAu",
                    "Content-Type": "application/json"
                }
            }
        )
        .then(data => {
            return true;
        })
        .catch(err => {
            return false;
        });
}

module.exports = { processMessage, sendMessage };