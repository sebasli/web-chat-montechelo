import io from 'socket.io-client';
import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import '@granite-elements/granite-bootstrap/granite-bootstrap.js';
import {} from '@polymer/polymer/lib/utils/resolve-url.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-dialog/paper-dialog.js';
import './css/style.css?name=polychat';
import './css/chat.css?name=chatstyle';
import axios from 'axios';
//variables para generación de hora actual y de id unico
const uuid = require('uuid');
/*
    ********************************************************************************************************************
    establecimiento de la variable socket para hacer uso de la función emit del modulo socket i.o
    ********************************************************************************************************************
*/
const pathUrl = 'https://dashbots.outsourcingcos.com';
const nomlocalStor = 'data_user_bot_Hughes';
const nomIdConStor = 'idCon_Hughes';
const nomCountStor = 'count_Hughes';
const socket = io.connect(pathUrl, { 'path': '/widget/sbs/socket.io', 'forceNew': true });
socket.off('private-message');
socket.off('new-message');

var count = 0;
/*
    ********************************************************************************************************************
    Clase que extiende del PolymerElement, define el html del custom element web-chat, sus propiedades y functiones
    ********************************************************************************************************************
*/




class WebChat extends PolymerElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open', delegatesFocus: false });
    }

    static get template() {
        return html `
    <style include = "polychat"></style>
    
    <section class="avenue-messenger">
      <div class="menu">
      <div class="items"><span>
        <a href="#" title="Minimize">&mdash;</a><br>
        <a href="#" title="End Chat">&#10005;</a>
        
        </span></div>
        <div on-click="showHide" class="button"><img width="15px" height="15px" src="{{imgclosewindow}}" /></div>
      </div>
    <div class="chat">        
      <div class="chat-title">
        <img style="padding-bottom:15px;width: 100px;" src="{{imgHughes}}" />
      </div>
      <div class="messages">

        <div class="messages-content" id="boxMess"></div>
      </div>
      <div class="message-box">
      <textarea on-click="downWin" id="private_message" type="text" class="message-input" placeholder="Escribe tu mensaje..." on-keydown="sendMessage"></textarea>
      <button type="submit" class="message-submit-menu" on-click="sendMessageMenu">MENÚ</button>
        <button type="submit" class="message-submit" on-click="sendMessage"><img width="30px" src="{{imgsrcbtn}}"></button>
      </div>
      <div class="fotstyle">
      <p class="pstyle">Powered By <a style="text-decoration: none !important; color:#FFFFFF; " href="https://www.montechelo.com.co/" target="_blank">Montechelo</a></p>
      </div>
    </div>
      </div>
    `;
    }

    static get properties() {
        return {
            list: {
                type: Array,
                value: []
            },
            imgsrc: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/boticon.png') }
            },
            imgsrcbtn: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/Icon_send.svg') }
            },
            imgHughes: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/logo-hughes.png') }
            },
            imgiconbot: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/bot_chat.png') }
            },
            imgactive: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/Activo_Top.svg') }
            },
            imgchatbot: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/bot_chat1.png') }
            },
            imgchatuser: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/user_chat.svg') }
            },
            imgclosewindow: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/close.png') }
            }
        };
    }

    ready() {
        super.ready();
        this.runOncePerDay();



        window.mess = this.$.messages;
        window.boxMess = this.$.boxMess;
        window.divv = this.$.winMessages;

        window.prvmess = this.$.private_message;
        //Si usuario ya se encuentra registrado
        if (JSON.parse(localStorage.getItem(nomlocalStor))) {
            window.idUser = JSON.parse(localStorage.getItem(nomlocalStor)).id;
            window.idToken = JSON.parse(localStorage.getItem(nomlocalStor)).token;
            window.idTokenJWT = JSON.parse(localStorage.getItem(nomlocalStor)).jwt;

            axios
                .post(pathUrl + '/widget/sbs/tasks/callAllConversation', {
                    idUser: window.idUser
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'authorization': 'Bearer ' + window.idTokenJWT
                    }
                }).then((res) => {
                    res.data.forEach((e) => {
                        let node = document.createElement('div');
                        if (e.who == 'user') {
                            renderMessageUser(node, e.message, e.hora);
                        } else if (e.who == 'bot') {
                            renderMessageBot(node, e.message, e.hora);
                        } else if (e.who == 'agent') {
                            renderMessageAgent(node, e.message, e.hora);
                        }
                    })
                    window.boxMess.scrollTop = window.boxMess.scrollHeight;
                });

            if (localStorage.getItem(nomIdConStor)) {
                window.idConversation = localStorage.getItem(nomIdConStor);
                axios
                    .post(pathUrl + '/widget/sbs/tasks/statusConvertation', {
                        idCon: localStorage.getItem(nomIdConStor)
                    }, {
                        headers: {
                            'Content-Type': 'application/json',
                            'authorization': 'Bearer ' + window.idTokenJWT
                        }
                    }).then((res) => {
                        if (res.data[0].estado != 'Abierta') {
                            localStorage.setItem(nomCountStor, 0);
                            localStorage.removeItem(nomIdConStor);
                        }
                    });
            }

        }

        if (parseInt(localStorage.getItem(nomCountStor)) == 3) {
            socket.emit('Agent-User', JSON.parse(localStorage.getItem(nomlocalStor)).token, null, null, null, 'webchat', 'user');
        }
    }
    hasOneDayPassed() {
        // get today's date. eg: "7/37/2007"
        var date = new Date().toLocaleDateString();
        // if there's a date in localstorage and it's equal to the above: 
        // inferring a day has yet to pass since both dates are equal.
        if (localStorage.getItem('date_app_Hughes') == date)
            return false;

        // this portion of logic occurs when a day has passed
        localStorage.setItem('date_app_Hughes', date);
        return true;
    }


    // some function which should run once a day
    runOncePerDay() {
        if (!this.hasOneDayPassed()) return false;
        localStorage.removeItem(nomlocalStor);
        localStorage.removeItem(nomCountStor);
        localStorage.removeItem(nomIdConStor);
    }
    _isEqual(list, string) {
        return list == string;
    }
    downWin() {
        window.boxMess.scrollTop = window.boxMess.scrollHeight;
    }
    reset() {
        localStorage.setItem(nomCountStor, 0);
    }
    sendMessageMenu() {
        if (localStorage.getItem(nomCountStor) < 3) {
            if (window.nodoSuggestions != undefined) {
                try {
                    window.boxMess.removeChild(window.nodoSuggestions);
                    window.nodoSuggestions = undefined;
                } catch (e) {
                    console.error(e);
                }
            }
            var node = document.createElement('div');
            renderMessageUser(node, 'Menú', nowHour());
            socket.emit('new-message', { 'message': 'menú', 'id': JSON.parse(localStorage.getItem(nomlocalStor)).token });
        }
    }
    sendMessage(evt) {
        var payload = this.$.private_message.value;
        if ((evt.keyCode == 13 || evt.type == "click") && payload.trim() != '') {
            evt.preventDefault();
            if (window.nodoSuggestions != undefined) {
                try {
                    window.boxMess.removeChild(window.nodoSuggestions);
                    window.nodoSuggestions = undefined;
                } catch (e) {
                    console.error(e);
                }
            }
            var node = document.createElement('div');
            renderMessageUser(node, payload, nowHour());
            if (localStorage.getItem(nomCountStor) == '0' || localStorage.getItem(nomCountStor) == '1') {
                socket.emit('new-message', { 'message': payload, 'id': JSON.parse(localStorage.getItem(nomlocalStor)).token });
                axios
                    .post(pathUrl + '/widget/sbs/tasks/saveMessage', {
                        idUser: window.idUser,
                        message: payload,
                        from: 'user'
                    }, {
                        headers: {
                            'Content-Type': 'application/json',
                            'authorization': 'Bearer ' + window.idTokenJWT
                        }
                    }).then(() => {
                        this.downWin();
                    });
            } else if (localStorage.getItem(nomCountStor) == '2') {
                socket.emit('Agent',
                    'Chats', {
                        socketId: JSON.parse(localStorage.getItem(nomlocalStor)).token,
                        id: window.idUser,
                        channel: 'webchat'
                    });
                axios
                    .post(pathUrl + '/widget/sbs/tasks/saveMessage', {
                        idUser: window.idUser,
                        message: payload,
                        from: 'user'
                    }, {
                        headers: {
                            'Content-Type': 'application/json',
                            'authorization': 'Bearer ' + window.idTokenJWT
                        }
                    }).then(() => {
                        this.downWin();
                    });
                var node2 = document.createElement('div');
                renderMessageBot(node2, "En un momento un agente se comunicara contigo", nowHour());
                count = parseInt(localStorage.getItem(nomCountStor)) + 1;
                localStorage.removeItem(nomCountStor);
                localStorage.setItem(nomCountStor, count);
                socket.emit('Agent-User', JSON.parse(localStorage.getItem(nomlocalStor)).token, null);
            } else {
                socket.emit('Agent-User', JSON.parse(localStorage.getItem(nomlocalStor)).token, payload, null, window.idConversation, 'webchat', 'tmoReset');
                axios
                    .post(pathUrl + '/widget/sbs/tasks/saveMessageAgent', {
                        idUser: window.idUser,
                        idSocket: window.idConversation,
                        message: payload,
                        from: 'user'
                    }, {
                        headers: {
                            'Content-Type': 'application/json',
                            'authorization': 'Bearer ' + window.idTokenJWT
                        }
                    }).then(() => {
                        this.downWin();
                    });
            }
            this.$.private_message.value = '';
            window.boxMess.scrollTop = window.boxMess.scrollHeight;
        }
    }
    showHide() {
        var displayed = window.cchat2.style.display;
        window.segundo.style.visibility = 'hidden';
        window.segundo.style.display = 'none';
        window.primero.style.visibility = 'visible';
        window.primero.style.display = 'block';
        if (displayed == 'none') {
            window.cchat2.style.display = '';
        } else {
            window.cchat2.style.display = 'none';
        }
    }
}
/*
    ********************************************************************************************************************
    Clase que extiende del PolymerElement, define el html del custom element web-form, sus propiedades y functiones
    ********************************************************************************************************************
*/
class WebForm extends PolymerElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open', delegatesFocus: false });
    }

    static get template() {
        return html `
    <style include = "polychat">
    </style>
    <section class="avenue-messenger">
      <div class="menu">
      <div class="items"><span>
        <a href="#" title="Minimize">&mdash;</a><br>
        <a href="#" title="End Chat">&#10005;</a>        
        </span></div>
        <div on-click="showHide" class="button"><img width="15px" height="15px" src="{{imgclosewindow}}" /></div>
      </div>
    <div class="chat">
      <div class="chat-title">
      <img style="padding-bottom:15px;width: 100px;" width: 100px; src="{{imgHughes}}" />
      </div>
      <div class="messages">
      <!--inicio form-->
      <paper-dialog id="modalTermsCon" class="size-modalTerms">
        <div style="text-align:justify; text-justify: inter-word;">
        <p style="text-align: center;"><strong>Política de Tratamiento deDatos Personales</strong></p><br>
        Última actualización: 15 de mayo de 2019
        Hughes de Colombia S.A.S. (“Hughes”) está comprometido a
        proteger la confidencialidad y la privacidad de sus suscriptores,
        empleados, contratistas, proveedores y otros socios de negocios, al
        igual que el procesamiento de sus datos personales, de acuerdo
        con las leyes y regulaciones aplicables (incluyendo la Ley 1581 de
        2012 and al Decreto 1377 de 2013).
    
        Este documento (la “Política”) contiene la política de Hughes en
        relación con el tratamiento de datos personales, incluyendo aquella
        que se maneje a través de la página web de Hughes en
        www.hughesnet.com.co y en cualquier otro sitio digital mantenido u
        operado por Hughes (en conjunto la “Página Web”). Los términos y
        condiciones aplicables al uso de la Página Web están disponibles
        en http://www.hughesnet.com.co/servicio-al-cliente/legales
        Hughes se reserva el derecho, a su exclusiva discreción, de
        modificar los términos de esta Política en cualquier momento. En
        caso de cualquier cambio material a la Política, Hughes informará
        que dicha Política está siendo modificada y lo invitará a revisarla en
        esta página web o a través de otros medios que Hughes pueda
        tener disponibles. Su uso continuado de este Servicio, en caso de
        ser usted un suscriptor, o en términos generales su relación
        continuada con Hughes después de dicha notificación, constituye
        su aceptación a los cambios. Usted deberá visitar esta Página Web
        para revisar la actual Política con regularidad. La fecha de la última
        actualización está indicada al final de esta Política para su
        comodidad.
        Obtención y Uso de la Información
        Cuando usted visita nuestra Página Web, solicita información a
        través de nuestra Página Web, solicita ser o se convierte en un
        suscriptor del servicio HughesNet, es un empleado de Hughes o senvolucra en una relación comercial con Hughes, en cualquier
        momento durante su relación
        podrá 
        solicitarle información personal, tal como su nombre, dirección,
        estrato, número telefónico, dirección de correo electrónico y su
        información financiera, tal como la información de su tarjeta de
        crédito. 
        la información Sobre
        que usted pueda
        suministrar, Hughes podrá obtener información adicional
        usted de otras fuentes. Hughes obtendrá, guardará, utilizará,
        circulará, suprimirá, analizará, compartirá, transmitirá, transferirá y
        en general procesará la información que usted le suministre a
        Hughes, al igual que la información que Hughes obtenga acerca de
        usted (en conjunto su “Datos Personales”).
        Atención al Cliente
        #206
        Su Datos Personales podrán ser utilizados con los siguientes
        propósitos, según aplicable: 1) para efectos de facturación y cobros
        y para manejar nuestra relación con usted; 2) para suministrarle los
        productos y servicios que usted solicite; 3) para anticipar y resolver
        problemas con nuestros productos y servicios; 4) para procesar y
        responder a preguntas; 5) para ponernos en contacto con usted
        acerca de productos y servicios que puedan ser de su interés; 6)
        para verificaciones y confirmaciones de crédito; 7) para generar
        perfiles de los suscriptores de Hughes, con base en sus
        preferencias de uso; 8) para enviar información acerca de nuevos
        productos, noticias y promociones; 9) para ponernos en contacto
        con usted en relación con su cuenta, las transacciones que usted
        ha hecho o nuestros servicios; 10) para hacer análisis estadísticos
        de Datos Personales obtenidos y de otra información relevante; 11)
        para hacer encuestas de calidad de servicio y satisfacción de los
        clientes; 12) para contratar, entrenar y evaluar individuos y personas
        que soliciten trabajo; 13) para desarrollar actividades relacionadas a
        los programas de bienestar social y salud ocupacional de los
        empleados; 14) para expedir certificados laborales y dar referencias
        laborales solicitadas; 15) para manejar la nómina y hacer pagos de
        seguridad social; 16) para conservar la información personal de los
        empleados una vez se termine el contrato de trabajo, de acuerdo
        con las normas de seguridad social, para prevención de crímenes y
        para posibles efectos judiciales; 17) para crear bases de datos; 18)
        para detectar, investigar y prevenir actividades que pudiesen violar
        las políticas de Hughes o las leyes y regulaciones aplicables; 19)
        para manejar toda la información necesaria para cumplir con las
        obligaciones tributarias y con los registros y obligaciones
        comerciales, corporativos y contables de Hughes; 20) para
        adelantar campañas de actualización de Datos Personales con el
        fin de garantizar su integridad; 21) para hacer análisis de control y
        prevención de fraude y de lavado de dinero, incluyendo sin
        limitación consultas de informes a listas restrictivas y oficinas de
        crédito; 22) para enviar notificaciones en relación con las
        modificaciones a esta Política; 23) para solicitar nuevas
        autorizaciones para procesar su Datos Personales; 24) para
        implementar canales de comunicación con usted; 25) para hacer
        cumplir nuestros contratos de suscriptor, las políticas del suscriptor,
        los términos de uso de la Página Web y/o otros contratos o acuerdos; 26) para mejorar el contenido y la navegabilidad de la
        a HughesNet
        Página Web y 27) para otros 
        fines notificados
        a usted en el 
        momento en que usted autorice el uso de la información. Hughes
        no alquila, vende o comparte su Datos Personales con terceros en
        forma diferente a según se indica aquí.
        Divulgaciones Adicionales de la Información
        Hughes también podrá utilizar o divulgar y transmitir a terceros (en
        Colombia o en el exterior) información acerca de usted, incluyendo
        su Datos Personales, en cualquiera de los siguientes casos:
        Para exigir cumplimiento de nuestros Contratos de Suscriptor,
        nuestras Políticas de Suscriptor, los términos de uso de nuestra
        Página Web y/o otros contratos o acuerdos.
        Para hacer outsourcing (“tercerización”) de cualquiera de las
        tareas a las que se hace referencia en esta Política (por
        ejemplo facturación, respuestas a solicitudes).
        En respuesta a una citación, orden de un tribunal u otros
        procesos legales.
        Para establecer o ejercer nuestros derechos legales o
        defendernos contra reclamaciones legales.
        A agencias de reportes de crédito con el fin de reportar
        propósitos o para obtener servicios de revisión crediticia.
        A una compañía controlada por o bajo control común con
        Hughes, para cualquier propósito permitido por esta Política y
        que usted haya autorizado.
        Cuando Hughes considere que tal utilización o divulgación es 1)
        necesaria con el fin de investigar, prevenir o tomar acciones en
        relación con actividades que se sospecha son ilegales, fraude o
        situaciones que involucren amenazas potenciales a la
        seguridad física de cualquier persona o 2) según lo requiera la
        ley.
        En el futuro podremos vender algunos o todos nuestros activos
        o reorganizar nuestra estructura corporativa. En tales
        transacciones, la información relacionada con clientes,
        empleados, contratistas, proveedores y otros individuos cuya
        información personal es procesada por Hughes, incluyendo su
        Datos Personales, en general es uno de los activos comerciales
        transferidos. En caso de una venta o reorganización de nuestros
        activos, incluyendo nuestra base de datos, podrá ser posible
        transferir los Datos Personales.
        Hughes podrá utilizar y divulgar información no personal,
        acumulada o resumida en relación con nuestros suscriptores,
        usuarios, clientes, empleados, contratistas, proveedores y otros
        socios comerciales o individuos cuya información personal es
        procesada por Hughes, sin notificación adicional. Este tipo de
        información no lo identificará a usted individualmente.
        Derechos sobre su Datos Personales
        Bienvenido a HughesNet Colombia
        En los casos en que Hughes procese sus Datos Personales, usted
        tendrá derecho legal de 1) conocer, actualizar, corregir y tener
        acceso a sus Datos Personales; 2) solicitar que sus Datos
        Personales sean eliminados
        de las bases
        de datos de Hughes
        o
        Planes
        Sobre
        Atención
        revocar su consentimiento, siempre y cuando
        no
        exista
        ninguna
        HughesNet Al Cliente
        obligación legal o contractual que requiera que sus Datos
        Personales continúen en las bases de datos de Hughes; 3) solicitar
        evidencia de su consentimiento; 4) solicitar ser informado acerca de
        la forma en que se han utilizado sus Datos Personales y 5) radicar
        quejas en relación con el procesamiento de sus Datos Personales
        ante la Superintendencia de Industria y Comercio, después de
        haber agotado el proceso de quejas con Hughes.
        Atención al Cliente
        #206
        Persona o Área a Cargo de la Protección de la Información
        Servicio al Cliente es responsable por los asuntos relacionados con
        la protección de los datos en Hughes y manejará todas las
        solicitudes, quejas y consultas en relación con sus Datos
        Personales. La siguiente es la información de contacto:
        - Número telefónico: 01 8009 13 14 97
        - Formulario de contacto electrónico:
        https://www.hughesnet.com.co/peticiones-quejas-y-reclamos
        Procedimiento para Presentar Solicitudes, Radicar Quejas o Hacer
        Consultas
        Usted podrá presentar solicitudes o quejas, según se indica abajo,
        a través del formulario de contacto electrónico disponible en
        https://www.hughesnet.com.co/peticiones-quejas-y-reclamos. Le
        rogamos también utilizar esta dirección de correo electrónico para
        reportar cualquier otro asunto o comentario en relación con nuestra
        Política o el procesamiento de su Datos Personales.
        Solicitudes
        Usted, o una persona autorizada por usted, podrá presentar una
        solicitud en relación con sus Datos Personales conexa con
        aspectos tales como los siguientes:
        Qué Datos Personales suyos están incluidos en las bases de
        datos de Hughes.
        En qué forma utiliza Hughes sus Datos Personales.
        Para qué propósitos utiliza Hughes sus Datos Personales.
        Después de haber verificado su identidad, o la identidad de la
        persona autorizada que presenta la solicitud a nombre suyo, usted
        o dicha persona tendrá acceso a sus Datos Personales en poder de
        Hughes, de acuerdo con los términos de su solicitud, las leyes y
        regulaciones aplicables.

                Las respuestas a cualquier solicitud se enviarán, dentro de los 10
        días hábiles siguientes a la fecha
        en que se
        recibió la solicitud
        incluyendo toda la información necesaria del solicitante, a la
        dirección de correo electrónico indicada en esta Política. En caso
        de que no sea posible resolver la solicitud dentro de los 10 días
        Sobre
        Atención será
        hábiles siguientes, usted Planes
        o la persona que
        presentó la solicitud
        HughesNet
        informado acerca de las razones por las cuales todavía Al
        se Cliente
        está
        procesando la solicitud, indicando la fecha en que se responderá a
        la misma.
        La respuesta definitiva a todas las solicitudes no tomará más de 15
        días hábiles después de la fecha en que Hughes recibió la solicitud
        en la dirección de correo electrónico aquí indicada incluyendo toda
        la información necesaria del solicitante.
        Su solicitud no tendrá costo alguno, siempre y cuando se envíe una
        sola cada mes calendario, o en casos en los cuales haya
        modificaciones importantes a esta Política. En caso de que usted
        desee presentar más de una solicitud durante un mes calendario,
        Hughes únicamente cobrará el envío, la reproducción y, de ser
        aplicable, la certificación de los documentos. Los costos de copias
        no serán mayores a los costos de recuperación del material
        correspondiente.
        Quejas
        Usted, o la persona autorizada por usted, podrá presentar una
        queja en relación con sus Datos Personales procesados por
        Hughes y que deban ser corregidos, actualizados o suprimidos en
        los siguientes casos:
        Un supuesto incumplimiento de cualquiera de las obligaciones
        de Hughes bajo las leyes aplicables.
        Datos Personales procesados por Hughes que deban ser
        corregidos, actualizados o suprimidos.
        Usted deberá incluir la siguiente información en el momento de
        presentar una queja:
        Nombre e identificación del solicitante.
        Número de la cuenta de suscriptor HughesNet, de ser aplicable.
        Dirección de correspondencia.
        Número telefónico.
        Hechos que apoyan la queja.
        Propósito de la queja (actualización, corrección o supresión).
        Cualquier documento que apoye la queja.
        En caso de que la queja esté incompleta o se requiera de
        documentación adicional, Hughes se pondrá en contacto con la
        persona que presenta la queja con el fin de que se corrija cualquier

        30/2019
        Política de Tratamiento de Datos Personales | Hughesnet.com.co
        falla en la documentación, dentro de los 5 días hábiles siguientes a
        la fecha de recibo de la queja.
        En caso de a que
        la persona no
        presente la información adicional o la documentación requerida
        dentro de los dos meses siguientes a la fecha de la queja inicial, se
        considerará que la persona ha retirado la queja.
        Planes
        Sobre
        Atención
        El tiempo máximo para resolver cualquier
        queja será de Al 15 Cliente
        días
        HughesNet
        hábiles después de su recibo. En caso de que no sea posible
        resolver la queja dentro de dicho tiempo, la parte interesada será
        informada acerca de las razones de la demora y la fecha en la cual
        se resolverá su queja, tiempo que no excederá 8 días hábiles
        después de la terminación del primer período de tiempo arriba
        indicado.
        Únicamente después de que usted, o una persona autorizada por
        usted, haya presentado una queja completa a Hughes y el proceso
        de la queja se haya agotado sin una solución/respuesta, podrá
        usted presentar una queja ante la Superintendencia de Industria y
        Comercio.
        Actualización de Su Información
        En caso de que usted sea un suscriptor y necesite actualizar su
        información, le rogamos visitar
        https://selfcare.hughesnet.com.co/Sa_Login y entrar en la sección
        “Información de la Cuenta”.
        Preferencias de Contacto
        Usted podrá cambiar sus preferencias en relación con el recibo de
        información de Hughes, enviándonos una solicitud a la siguiente
        URL: https://www.hughesnet.com.co/peticiones-quejas-y-reclamos.
        Haremos esfuerzos razonables para procesar cualquier cambio.
        Menores de Edad
        Usted deberá tener un mínimo de 18 años de edad para solicitar o
        suscribirse al servicio HughesNet. Hughes no obtiene
        intencionalmente Datos Personales de menores de edad.
        Seguridad
        Hughes utiliza medidas razonables para proteger su Datos
        Personales contra acceso por personas no autorizadas.
        Vigencia
        Esta política es efectiva a partir de mayo 15 de 2019.
        Hughes de Colombia S.A.S., NIT: 900.971.687-1, Carrera 11a # 94- 45
        oficina 703 Edificio Oxo Center, Bogotá D.C.
        © 2019 Hughes de Colombia S.A.S. Todos los derechos reservados.
        HughesNet es una marca comercial registrada de Hughes Network
        Systems, LLC, una Compañía EchoStar
        </div>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalTermsCon" value="Cerrar">         
        </div>
      </paper-dialog>
      <paper-dialog id="modalCorreo" class="size-modalData">
        <p class="paragraphModal">Debes ingresar un <strong>Teléfono</strong> valido</p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalEmail" value="Cerrar">         
        </div>
      </paper-dialog>
      <paper-dialog id="modalData" class="size-modalData">
        <p class="paragraphModal">Por favor, dime tu <strong>nombre</strong> y <strong>télefono</strong></p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModal" value="Cerrar">         
        </div>
      </paper-dialog>
      <paper-dialog id="modalTerms" class="size-modalData">
        <p class="paragraphModal">Para continuar con nuestro servicio debes aceptar <strong>terminos y condiciones</strong> </p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalTerms" value="Cerrar">         
        </div>
      </paper-dialog>
        <div class="messages-content">
        <img src="{{imgactive}}" class="imgstyle2"/>
          <img src="{{imgiconbot}}" class="imgstyle"/>
          <p class="greetings">¡Hola!</p>
          <p class="paragraph">Mi nombre es <strong>ISA</strong>, Por favor <strong>completa la siguiente información</strong> para que charlemos.</p>
          <br/>
          <div class="divForm" style="">
            <form class="formFlex">

            <label for="fname" class="labelForm">Nombre Completo</label>
            <input type="text" id="name" name="name" class="inputclass" placeholder="Escribe tu nombre completo">

            

            <label for="femail" class="labelForm">Teléfono ó celular</label>
            <input type="text" id="email" name="email" class="inputclass" placeholder="Escribe tu teléfono">

            <label for="femail" class="labelForm">Correo electronico</label>
            <input type="email" id="email1" name="email1" class="inputclass" placeholder="Escribe tu correo electronico">
              
         
            </form>
              <input id="defaultCheck1" class="check" type="checkbox" name="terminos" value="terminos"><span  on-click="openTerms" style="cursor: pointer; color: #0091C4;"> Aceptar <span style="text-decoration:underline;">terminos y condiciones</span></span><br>
              <input class="buttonContinue" type="button" on-click="submit" value="Continuar">            
          </div>
        </div>
      </div>
      <!--fin form -->
      <div class="fotstyle">
        <p class="pstyle">Powered By <a style="text-decoration: none !important; color:#FFFFFF; " href="https://www.montechelo.com.co/" target="_blank">Montechelo</a></p>
      </div>
    </div>
      </div>
    `;
    }

    static get properties() {
        return {
            imgHughes: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/logo-hughes.png') }
            },
            imgiconbot: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/bot_chat.png') }
            },
            imgactive: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/Activo_Top.svg') }
            },
            imgclosewindow: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/close.png') }
            }
        };
    }

    ready() {
        super.ready();
    }

    showHide() {
        window.segundo.style.visibility = 'hidden';
        window.segundo.style.display = 'none';
        window.primero.style.visibility = 'visible';
        window.primero.style.display = 'block';
        var displayed = window.cchat.style.display;
        if (displayed == 'none') {
            window.cchat.style.display = '';
        } else {
            window.cchat.style.display = 'none';
        }
    }
    openTerms() {
        this.$.modalTermsCon.open();
    }

    hideModalTermsCon() {
        this.$.modalTermsCon.toggle();
    }

    hideModal() {
        this.$.modalData.toggle();
    }

    hideModalTerms() {
        this.$.modalTerms.toggle();
    }
    hideModalEmail() {
        this.$.modalCorreo.toggle();
    }


    submit(e) {
        e.preventDefault();

        var nameUser = this.$.name.value;
        var emailUser = this.$.email.value;
        var email1User = this.$.email1.value;
        var terminosUser = this.$.defaultCheck1.checked;

        if (nameUser && emailUser && email1User && terminosUser === true) {
            if (isNaN(emailUser) === true || emailUser.length < 5) {
                this.$.modalCorreo.open();
            } else {
                axios
                    .post(pathUrl + '/widget/sbs/tasks/saveUser', {
                        name: nameUser,
                        email: emailUser,
                        email1: email1User,
                        conditions: "Aceptado"
                    }).then((response) => {
                        localStorage.setItem(nomlocalStor, JSON.stringify({ id: response.data.data[0].id, name: nameUser, email: emailUser, token: uuid() + getCleanedString(nameUser), jwt: response.data.token, email1: email1User }));
                        localStorage.setItem(nomCountStor, 0);
                        window.idUser = response.data.data[0].id;
                        window.idTokenJWT = response.data.token;
                        window.cchat.style.display = 'none';
                        socket.emit('new-message', {
                            'message': 'Hola',
                            'id': JSON.parse(localStorage.getItem(nomlocalStor)).token
                        });
                        window.cchat2.style.display = '';
                    }).catch((e) => {
                        console.error(e);
                        console.log('vali madres')
                    })
            }
        } else if (!nameUser) {
            this.$.modalData.open();
        } else if (!emailUser) {
            this.$.modalData.open();
        } else if (terminosUser === false) {
            this.$.modalTerms.open();
        }
    }
}


/*
    ********************************************************************************************************************
    Clase que extiende del PolymerElement, define el html del custom element web-icon, sus propiedades y functiones
    ********************************************************************************************************************
*/
class WebIcon extends PolymerElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open', delegatesFocus: false });
    }

    static get template() {
        return html `
   <style>
     
    	.buttonChat {
        position:fixed;
        bottom:10px;
        right:10px;
        border: none;
        color: white;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
        z-index: 999;      
      }  
    </style>
    <style include="granite-bootstrap"></style>    
    <style include="chatstyle"></style>

    <web-form id="chatWin" style="border-radius: 50px 20px; display:none"></web-form>
    <web-chat id="chatWin2" style="border-radius: 50px 20px; display:none"></web-chat>  

    <div class="buttonChat bubble"> 
      <p id="primero" style="visibility:visible; display:block;" on-click="visualiza_segundo">
        <img src="{{imgsrchat}}" style="width:55px; margin-top:10px;">
      </p>
      <p id="segundo" style="visibility:hidden; display:none;" on-click="visualiza_primero">
        <img src="{{imgsrclose}}" style="width:25px; margin-top:18px;">
      </p>    
    </div>  
    `;
    }

    ready() {
        super.ready();
        window.cchat = this.$.chatWin;
        window.cchat2 = this.$.chatWin2;
        window.primero = this.$.primero;
        window.segundo = this.$.segundo;
    }
    static get properties() {
        return {
            imgsrchat: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/chat.png') }
            },
            imgsrclose: {
                type: String,
                value: function() { return this.resolveUrl(pathUrl + '/widget/sbs/img/close.png') }
            }
        };
    }

    visualiza_primero() {
        this.$.primero.style.visibility = 'visible';
        this.$.primero.style.display = 'block';
        this.$.segundo.style.visibility = 'hidden';
        this.$.segundo.style.display = 'none';
        window.boxMess.scrollTop = window.boxMess.scrollHeight;
        if (localStorage.getItem(nomlocalStor)) {
            var displayed = this.$.chatWin2.style.display;
            if (displayed == 'none') {
                this.$.chatWin2.style.display = '';
            } else {
                this.$.chatWin2.style.display = 'none';
            }
            window.boxMess.scrollTop = window.boxMess.scrollHeight;
        } else {
            var displayed = this.$.chatWin.style.display;
            if (displayed == 'none') {
                this.$.chatWin.style.display = '';
            } else {
                this.$.chatWin.style.display = 'none';
            }
            window.boxMess.scrollTop = window.boxMess.scrollHeight;
        }
    };
    visualiza_segundo() {
        this.$.segundo.style.visibility = 'visible';
        this.$.segundo.style.display = 'block';
        this.$.primero.style.visibility = 'hidden';
        this.$.primero.style.display = 'none';
        if (localStorage.getItem(nomlocalStor)) {
            var displayed = this.$.chatWin2.style.display;
            if (displayed == 'none') {
                this.$.chatWin2.style.display = '';
            } else {
                this.$.chatWin2.style.display = 'none';
            }
        } else {
            var displayed = this.$.chatWin.style.display;
            if (displayed == 'none') {
                this.$.chatWin.style.display = '';
            } else {
                this.$.chatWin.style.display = 'none';
            }
        }
    };
}

/*
    ********************************************************************************************************************
    Definición de los elementos personalizados (custom-element) en el navegador
    ********************************************************************************************************************
*/
window.customElements.define('web-icon', WebIcon);
window.customElements.define('web-chat', WebChat);
window.customElements.define('web-form', WebForm);
/*
    ********************************************************************************************************************
    suscripción del cliente al canal new-message del socket, permite la comunicación del cliente con el usuario
    ********************************************************************************************************************
*/
socket.on('new-message', async function(data) {

    var node = document.createElement('div');

    if (data.action != 'suggestions') {
        window.setTimeSugg = 2500;
    } else {
        window.setTimeSugg = 0;
    }

    if (data.action == 'human') {
        window.setTimeMsg = 0;
    } else if (data.action == 'resetChat') {
        localStorage.setItem(nomCountStor, 0);
        window.setTimeMsg = 2200;
    } else {
        window.setTimeMsg = 2200;
    }

    if (data.image) {
        renderLoadingMsg(node)
        setTimeout(() => {
            renderImage(node, data.image)
        })

    }

    if (data.msg) {
        renderLoadingMsg(node);
        setTimeout(function() {
            renderMessageBot(node, data.msg, nowHour());
            axios
                .post(pathUrl + '/widget/sbs/tasks/saveMessage', {
                    idUser: window.idUser,
                    message: data.msg,
                    from: 'bot'
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'authorization': 'Bearer ' + window.idTokenJWT
                    }
                })
        }, window.setTimeMsg);
    } else if (data.sug) {
        setTimeout(function() {
            if (window.nodoSuggestions != undefined) {
                try {
                    window.boxMess.removeChild(window.nodoSuggestions);
                    window.nodoSuggestions = undefined;
                } catch (e) {
                    console.error(e);
                }
            }
            window.nodoSuggestions = document.createElement('div');
            window.nodoSuggestions.setAttribute("class", "server-response-suggestions");
            window.nodoSuggestions.setAttribute("id", "suggestion");
            window.boxMess.appendChild(window.nodoSuggestions);
            data.sug.forEach(function(e) {
                var node1 = document.createElement('button')
                node1.setAttribute("class", "sug");
                node1.setAttribute("value", "" + e.title + "");
                node1.innerHTML = e.title;
                window.nodoSuggestions.appendChild(node1);
                window.boxMess.scrollTop = window.boxMess.scrollHeight;
                node1.addEventListener('click', function() {

                    node.setAttribute("class", "message message-personal");
                    var messugestion = node1.getAttribute("value");
                    axios
                        .post(pathUrl + '/widget/sbs/tasks/saveMessage', {
                            idUser: window.idUser,
                            message: messugestion,
                            from: 'user'
                        }, {
                            headers: {
                                'Content-Type': 'application/json',
                                'authorization': 'Bearer ' + window.idTokenJWT
                            }
                        });
                    renderMessageUser(node, messugestion, nowHour())
                    window.boxMess.removeChild(window.nodoSuggestions);
                    window.nodoSuggestions = undefined;
                    socket.emit('new-message', { 'message': node1.getAttribute("value"), 'id': JSON.parse(localStorage.getItem(nomlocalStor)).token });
                    window.prvmess.value = '';
                    window.boxMess.scrollTop = window.boxMess.scrollHeight;
                });
            });
        }, window.setTimeSugg);
    }
});

socket.on('estado', function(msg) {
    if (msg == 'nuevo') {
        count = parseInt(localStorage.getItem(nomCountStor)) + 1;
        localStorage.removeItem(nomCountStor);
        localStorage.setItem(nomCountStor, count);
    } else {
        socket.emit('Agent',
            'Chats', {
                socketId: JSON.parse(localStorage.getItem(nomlocalStor)).token,
                id: window.idUser,
                channel: 'webchat'
            });
        localStorage.removeItem(nomCountStor);
        localStorage.setItem(nomCountStor, 3);
        socket.emit('Agent-User', JSON.parse(localStorage.getItem(nomlocalStor)).token, null);
    }
})

socket.on('private-message', function(data) {
    var node = document.createElement('div');
    if (data.messageUser != null) {
        if (data.state == null) {
            socket.emit('Agent-User', JSON.parse(localStorage.getItem(nomlocalStor)).token, null, null, null, 'webchat', 'tmoReset');
            renderMessageAgent(node, data.messageUser, nowHour())
        } else if (data.state == 1 || data.state == 5) {
            axios
                .post(pathUrl + '/widget/sbs/tasks/saveMessageAgent', {
                    idUser: window.idUser,
                    idSocket: JSON.parse(localStorage.getItem(nomIdConStor)),
                    message: data.messageUser,
                    from: 'agent'
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'authorization': 'Bearer ' + window.idTokenJWT
                    }
                })
            renderMessageAgent(node, data.messageUser, nowHour())
            localStorage.removeItem(nomCountStor);
            localStorage.removeItem(nomIdConStor);
            localStorage.setItem(nomCountStor, 0);
            socket.emit('Agent-User', JSON.parse(localStorage.getItem(nomlocalStor)).token, null, null, window.idConversation, 'webchat', 'tmoFin');
        } else if (data.state == 2) {
            axios
                .post(pathUrl + '/widget/sbs/tasks/saveMessageAgent', {
                    idUser: window.idUser,
                    idSocket: data.idCon,
                    message: data.messageUser,
                    from: 'agent'
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'authorization': 'Bearer ' + window.idTokenJWT
                    }
                })
            renderMessageAgent(node, data.messageUser, nowHour())
            localStorage.setItem(nomIdConStor, data.idCon);
            window.idConversation = data.idCon;
            socket.emit('Agent-User', JSON.parse(localStorage.getItem(nomlocalStor)).token, null, null, window.idConversation, 'webchat', 'tmo');
        } else if (data.state == 4) {
            axios
                .post(pathUrl + '/widget/sbs/tasks/saveMessageAgent', {
                    idUser: window.idUser,
                    idSocket: data.idCon,
                    message: data.messageUser,
                    from: 'agent'
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'authorization': 'Bearer ' + window.idTokenJWT
                    }
                }.then(res => console.log(res)))
            renderMessageAgent(node, data.messageUser, nowHour())
        }
    }
});
/*
  Function para renderizado de mensajes
*/
function renderMessageUser(node, msg, hour) {
    node.setAttribute("class", "message message-personal");
    node.innerHTML = "<figure class='avatarUser'><img src='https://dashbots.outsourcingcos.com/widget/sbs/img/user_chat.svg'></figure>" + msg + "<p class='putTimestampUser'>" + hour + "</p>";
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}
/*
  Mensaje Loading
*/
function renderLoadingMsg(node) {
    node.setAttribute("class", "message new loading");
    node.innerHTML = "<figure class='avatar'><img src='https://dashbots.outsourcingcos.com/widget/sbs/img/bot_chat.png'></figure><span></span><span></span><span></span><p class='putTimestampBot'>" + nowHour() + "</p>";
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}
/*
  Funcion para rendedizar mensaje del bot
*/
function renderMessageBot(node, msg, hour) {
    var strNew = msg.replace(/\*(\S(.*?\S)?)\*/gm, '<strong>$1</strong>');
    var strNews = strNew.replace(/(?:\r\n|\r|\n)/g, '<br>');
    let strLink = strNews.replace(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm, function(url) {
        return '<a target="autoblank" href="' + url + '">' + url + '</a>';
    })
    node.setAttribute("class", "message new");
    node.innerHTML = "<figure class='avatar'><img src='https://dashbots.outsourcingcos.com/widget/sbs/img/bot_chat1.png'></figure>" + strLink + "<p class='putTimestampBot'>" + hour + "</p>";
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}
/*
/*
  Funcion para rendedizar mensaje del Agente
*/
function renderMessageAgent(node, msg, hour) {
    let strNew = msg.replace(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm, function(url) {
        return '<a target="autoblank" href="' + url + '">' + url + '</a>';
    });
    var node = document.createElement('div');
    node.setAttribute("class", "message new");
    node.innerHTML = "<figure class='avatar'><img src='https://dashbots.outsourcingcos.com/widget/sbs/img/bot_chat1.png'></figure>" + strNew + "<p class='putTimestampBot'>" + hour + "</p>";
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}
/*
  Funcion para obtener formato de hora-minutos-segundos hh:mm:ss
*/
function nowHour() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var apm = " ";
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (hours > 12) {
        hours = hours - 12;
        apm = "pm";
    } else if (hours == 12) {
        apm = "pm";
    } else {
        apm = "am";
    }
    if (seconds < 10) seconds = "0" + seconds;
    return hours + ":" + minutes + ":" + seconds + " " + apm;
}

function renderImage(node, img) {
    node.setAttribute("class", "message new loading");
    const imagen = '<img class="img_mostrar" src="' + img + '">'
    node.innerHTML = "<figure class='avatar'><img src='https://dashbots.outsourcingcos.com/widget/sbs/img/bot_chat1.png'></figure>" + imagen + "<p class='putTimestampBot'>" + nowHour() + "</p>";
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}

function getCleanedString(cadena) {
    // Definimos los caracteres que queremos eliminar
    var specialChars = "!@#$^&%*()+=-[]/{}|:<>?,.";
    // Los eliminamos todos
    for (var i = 0; i < specialChars.length; i++) {
        cadena = cadena.replace(new RegExp("\\" + specialChars[i], "gi"), "");
    }
    // Lo queremos devolver limpio en minusculas
    cadena = cadena.toLowerCase();
    // Quitamos espacios y los sustituimos por -
    cadena = cadena.replace(/ /g, "-");
    // Quitamos acentos y "ñ". Fijate en que va sin comillas el primer parametro
    cadena = cadena.replace(/á/gi, "a");
    cadena = cadena.replace(/é/gi, "e");
    cadena = cadena.replace(/í/gi, "i");
    cadena = cadena.replace(/ó/gi, "o");
    cadena = cadena.replace(/ú/gi, "u");
    cadena = cadena.replace(/ñ/gi, "n");
    return cadena;
}