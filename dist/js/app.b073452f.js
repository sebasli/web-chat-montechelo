(function(t) {
    function e(e) {
        for (var s, o, r = e[0], l = e[1], c = e[2], h = 0, u = []; h < r.length; h++) o = r[h], Object.prototype.hasOwnProperty.call(i, o) && i[o] && u.push(i[o][0]), i[o] = 0;
        for (s in l) Object.prototype.hasOwnProperty.call(l, s) && (t[s] = l[s]);
        d && d(e);
        while (u.length) u.shift()();
        return n.push.apply(n, c || []), a()
    }

    function a() {
        for (var t, e = 0; e < n.length; e++) {
            for (var a = n[e], s = !0, r = 1; r < a.length; r++) {
                var l = a[r];
                0 !== i[l] && (s = !1)
            }
            s && (n.splice(e--, 1), t = o(o.s = a[0]))
        }
        return t
    }
    var s = {},
        i = { app: 0 },
        n = [];

    function o(e) { if (s[e]) return s[e].exports; var a = s[e] = { i: e, l: !1, exports: {} }; return t[e].call(a.exports, a, a.exports, o), a.l = !0, a.exports }
    o.m = t, o.c = s, o.d = function(t, e, a) { o.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: a }) }, o.r = function(t) { "undefined" !== typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 }) }, o.t = function(t, e) {
        if (1 & e && (t = o(t)), 8 & e) return t;
        if (4 & e && "object" === typeof t && t && t.__esModule) return t;
        var a = Object.create(null);
        if (o.r(a), Object.defineProperty(a, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t)
            for (var s in t) o.d(a, s, function(e) { return t[e] }.bind(null, s));
        return a
    }, o.n = function(t) { var e = t && t.__esModule ? function() { return t["default"] } : function() { return t }; return o.d(e, "a", e), e }, o.o = function(t, e) { return Object.prototype.hasOwnProperty.call(t, e) }, o.p = "/bothuman/montechelo/";
    var r = window["webpackJsonp"] = window["webpackJsonp"] || [],
        l = r.push.bind(r);
    r.push = e, r = r.slice();
    for (var c = 0; c < r.length; c++) e(r[c]);
    var d = l;
    n.push([0, "chunk-vendors"]), a()
})({
    0: function(t, e, a) { t.exports = a("56d7") },
    "066c": function(t, e, a) {},
    "06fc": function(t, e, a) {},
    "096c": function(t, e, a) {},
    "0a31": function(t, e, a) {},
    "0ede": function(t, e, a) {
        "use strict";
        var s = a("639f"),
            i = a.n(s);
        i.a
    },
    1: function(t, e) {},
    1143: function(t, e, a) { t.exports = a.p + "img/elipseOffline.85477f9c.svg" },
    1188: function(t, e, a) {},
    "13fd": function(t, e, a) { t.exports = a.p + "img/navbarmontechelo.e2eca97e.svg" },
    "15f7": function(t, e, a) {},
    "175c": function(t, e, a) { t.exports = a.p + "img/montechelo-logo.0a22ffd6.svg" },
    "245b": function(t, e, a) { t.exports = a.p + "img/Online.d31fa79c.svg" },
    "2f6c": function(t, e, a) {
        "use strict";
        var s = a("cb5f"),
            i = a.n(s);
        i.a
    },
    3776: function(t, e, a) { t.exports = a.p + "img/whatsapp.8671d88f.svg" },
    "39df": function(t, e, a) { t.exports = a.p + "img/chat1.svg" },
    "4d58": function(t, e, a) {},
    "4db4": function(t, e, a) {},
    5009: function(t, e, a) { t.exports = a.p + "img/question.3cc25274.svg" },
    "566c": function(t, e, a) {
        "use strict";
        var s = a("bc54"),
            i = a.n(s);
        i.a
    },
    "56a9": function(t, e, a) { t.exports = a.p + "img/chat_module.c637c01e.svg" },
    "56d7": function(t, e, a) {
        "use strict";
        a.r(e);
        a("cadf"), a("551c"), a("f751"), a("097d");
        var s = a("2b0e"),
            i = function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("div", { attrs: { id: "app" } }, [s("sidebar-menu", { attrs: { menu: t.menu, collapsed: t.collapsed, width: t.width, widthCollapsed: t.widthCollapsed }, on: { "toggle-collapse": t.onToggleCollapse, "item-click": t.onItemClick } }), s("b-modal", { attrs: { id: "bv-modal-alto", title: "Alto!", "hide-footer": "", scrollable: "" } }, [s("p", { staticClass: "my-4" }, [t._v("\n      Tienes que finalizar todas las conversaciones antes de cerrar sesión\n    ")])]), s("b-navbar", { staticClass: "text-light p-2", staticStyle: { background: "#002A3A", transition: "all 700ms" }, style: { left: t.mgleftNav }, attrs: { fixed: "top", toggleable: "lg" } }, [s("b-navbar-brand", [s("img", { staticStyle: { width: "173px", height: "42px", "margin-left": "50%" }, attrs: { src: a("39df") } })]), s("b-navbar-brand", { staticClass: "d-flex sm" }), s("b-navbar-toggle", { style: { display: t.navDis }, attrs: { target: "nav-collapse" } }), s("b-collapse", { attrs: { id: "nav-collapse", "is-nav": "" } }, [s("b-navbar-nav", { staticClass: "ml-auto" }, [s("div", { staticClass: "timeContainer align-self-center flex-column", style: { display: t.showTimeOnline() } }, [s("span", { staticClass: "align-self-center text-light", class: { "d-none": t.isActive } }, [t._v("Tiempo Online ")]), s("span", { key: t.seconds_online, staticClass: "align-self-center text-light", class: { "d-none": t.isActive } }, [t._v("\n            " + t._s(t.seconds_online))])]), s("div", { staticStyle: { width: "15px" } }), s("div", { staticClass: "timeContainerBreak align-self-center flex-column", style: { display: t.showTimeBreak() } }, [s("span", { staticClass: "align-self-center text-light", class: { "d-none": t.isActive } }, [t._v("Tiempo en Break ")]), s("span", { key: t.seconds_break, staticClass: "align-self-center text-light", class: { "d-none": t.isActive } }, [t._v(t._s(t.seconds_break))])]), s("div", { staticStyle: { width: "140px" } }), s("div", { staticClass: "align-self-center d-flex flex-column text-right" }, [s("span", { staticClass: "text-light", staticStyle: { "font-size": "20px", "font-weight": "bold" } }, [t._v(t._s(t.nameAgent) + "\n          ")]), s("span", { staticClass: "text-light" }, [t._v(" " + t._s(t.idAgent))])]), s("div", { staticClass: "align-self-center" }, [s("b-nav-item-dropdown", { staticClass: "text-light", attrs: { variant: "link", right: "" } }, [s("template", { slot: "button-content" }, [s("img", { staticStyle: { width: "64px" }, attrs: { src: a("6364") } }), s("b-img", { staticStyle: { position: "fixed", right: "34px", top: "64px" }, attrs: { src: a("bb6e")("./" + t.checkStateAgent()) } })], 1), s("div", { staticClass: "d-flex flex-column justify-content-around" }, [s("span", { staticClass: "border-bottom", staticStyle: { color: "#002A3A", "font-weight": "bold" } }, [t._v("Estado")]), s("b-dropdown-item", { staticClass: "border-bottom", on: { click: function(e) { t.state = "ONLINE", t.estado() } } }, [s("span", [t._v("Online ")]), s("b-img", { attrs: { src: a("bb6e")("./" + t.checkState()) } })], 1), s("b-dropdown-item", { staticClass: "border-bottom", on: { click: function(e) { t.state = "BREAK", t.estado() } } }, [s("span", [t._v("Break ")]), s("b-img", { attrs: { src: a("bb6e")("./" + t.checkStateBreak()) } })], 1), s("b-dropdown-item", { staticClass: "border-bottom", on: { click: function(e) { t.state = "OCUPADO", t.estado() } } }, [s("span", [t._v("Ocupado ")]), s("b-img", { attrs: { src: a("bb6e")("./" + t.checkStateOcp()) } })], 1), s("b-dropdown-item", { on: { click: function(e) { return t.confirmOff() } } }, [s("span", { staticStyle: { color: "#002A3A", "font-weight": "bold" } }, [t._v("Cerrar Sesión ")]), s("img", { attrs: { src: a("d365") } })])], 1)], 2)], 1)])], 1)], 1), s("router-view")], 1)
            },
            n = [],
            o = a("8055"),
            r = a.n(o),
            l = a("bc3a"),
            c = a.n(l),
            d = a("4776");
        a("b15b"), a("15f5");
        a("87d5");
        var h = {
                name: "app",
                components: { SidebarMenu: d["SidebarMenu"] },
                data: function() { return { publicPath: "/bothuman/montechelo/", pathServer: "https://dashbots.outsourcingcos.com/widget/montechelo/tasks", localPathData: "data_agent_bot_montechelo", localPathJToken: "jwt_bothuman_montechelo", localPathState: "state_montechelos", isActive: !0, isTime: "none", navDis: "none", state: "SELECCIONA ALGUNO ESTADO", socket: r.a.connect("https://dashbots.outsourcingcos.com", { path: "/widget/montechelo/socket.io", forceNew: !0 }), seconds_online: "00:00:00", seconds_break: "00:00:00", nameAgent: "", idAgent: "", mgleftNav: "50px", boxTwo: "", menu: [{ header: !0, title: "Menu Principal", hiddenOnCollapse: !0 }, { href: "/", title: "Inbound", icon: "far fa-comment-dots", hidden: !1 }, { href: "/control", title: "Tablero Control", icon: "fa fa-chart-area", hidden: !1 }, { title: "Usuarios", icon: "fas fa-user-friends", hidden: !1, child: [{ href: "/user", title: "Crear", icon: "fas fa-user-plus" }, { href: "/user/update", title: "Actualizar", icon: "fas fa-user-edit" }] }, { title: "Configuraciones", icon: "fas fa-cogs", hidden: !1, child: [{ href: "/settings", title: "Horarios", icon: "far fa-calendar-alt" }, { href: "/settings/capacity", title: "Capacidad", icon: "far fa-comments" }, { href: "/settings/tipificaciones", title: "Tipificaciones", icon: "fas fa-pencil-alt" }, { href: "/settings/atajos", title: "Atajos Chat", icon: "far fa-file-alt" }, { href: "/settings/tmo", title: "TMO", icon: "far fa-clock" }, { href: "/settings/qr", title: "QR Whatsapp", icon: "fas fa-qrcode" }] }], collapsed: !0, width: "200px", widthCollapsed: "50px" } },
                methods: {
                    onToggleCollapse: function(t) { "200px" == this.mgleftNav ? this.mgleftNav = "50px" : this.mgleftNav = "200px" },
                    onItemClick: function(t, e) {},
                    checkState: function() { return "ONLINE" === this.state ? "Online.svg" : "Offbutton.svg" },
                    checkStateBreak: function() { return "BREAK" === this.state ? "Break.svg" : "Offbutton.svg" },
                    checkStateOcp: function() { return "OCUPADO" === this.state ? "Ocupado.svg" : "Offbutton.svg" },
                    checkStateAgent: function() { return "ONLINE" === this.state ? "elipseOnline.svg" : "BREAK" === this.state ? "elipseBreak.svg" : "OCUPADO" === this.state ? "elipseOcupado.svg" : "elipseOffline.svg" },
                    showTimeBreak: function() { return null === this.seconds_break || "" === this.seconds_break || "00:00:00" === this.seconds_break ? "none" : "flex" },
                    showTimeOnline: function() { return null === this.seconds_online || "" === this.seconds_online || "00:00:00" === this.seconds_online ? "none" : "flex" },
                    confirmOff: function() {
                        var t = this;
                        c.a.post(this.pathServer + "/callConversations", { idAgent: window.idAg, tokenUser: localStorage.getItem(this.localPathJToken) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(e) { console.log(e.data), e.data.length > 0 ? t.$bvModal.show("bv-modal-alto") : (t.boxTwo = "", t.$bvModal.msgBoxConfirm("¿Estas seguro de desconectarte?", { title: "Please Confirm", size: "sm", buttonSize: "sm", okVariant: "secondary", okTitle: "SI", cancelVariant: "info", cancelTitle: "NO", footerBgVariant: "white", footerClass: "p-2", hideHeader: !0, centered: !0 }).then((function(e) { t.boxTwo = e, 1 == e && ("00:00:00" != t.seconds_online && "" != t.seconds_online && t.seconds_online && c.a.post(t.pathServer + "/saveStateAgent", { state: "ONLINE", idAgent: JSON.parse(localStorage.getItem(t.localPathData)).idAgent, tokenUser: localStorage.getItem(t.localPathJToken), totalTime: t.seconds_online }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(t.localPathData)).jsonWebToken } }), "00:00:00" != t.seconds_break && "" != t.seconds_break && t.seconds_break && c.a.post(t.pathServer + "/saveStateAgent", { state: "BREAK", idAgent: JSON.parse(localStorage.getItem(t.localPathData)).idAgent, tokenUser: localStorage.getItem(t.localPathJToken), totalTime: t.seconds_break }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(t.localPathData)).jsonWebToken } }), t.state = "OFFLINE", t.estado(), t.seconds_online = "00:00:00", t.seconds_break = "00:00:00", t.isActive = !0, t.navDis = "none", localStorage.removeItem(t.localPathJToken), localStorage.removeItem(t.localPathData), localStorage.removeItem(t.localPathState), t.$router.push({ name: "login" })) })).catch((function(t) { console.log(t) }))) }))
                    },
                    estado: function() {
                        var t = this,
                            e = localStorage.getItem(this.localPathJToken);
                        c.a.post(this.pathServer + "/checkAgent", { idAgent: window.idAg, tokenUser: localStorage.getItem(this.localPathJToken) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(a) { "sucess" === a.data ? "ONLINE" == t.state ? t.socket.emit("Agent", e, { checkCola: e, newStat: t.state }) : t.socket.emit("Agent", e, { newState: t.state }) : "Fail" === a.data && ("ONLINE" == t.state ? t.socket.emit("Supervisor", e, { checkCola: e, newStat: t.state }) : t.socket.emit("Supervisor", e, { newState: t.state })) })), localStorage.setItem(this.localPathState, this.state)
                    },
                    hasOneDayPassed: function() { var t = (new Date).toLocaleDateString(); return localStorage.getItem("date_app_bh_montechelo") != t && (localStorage.setItem("date_app_bh_montechelo", t), !0) },
                    runOncePerDay: function() {
                        if (!this.hasOneDayPassed()) return !1;
                        localStorage.removeItem(this.localPathData), localStorage.removeItem(this.localPathState), localStorage.removeItem(this.localPathJToken)
                    }
                },
                beforeMount: function() {
                    var t = this;
                    this.runOncePerDay(), localStorage.getItem(this.localPathData) && (window.idAg = JSON.parse(localStorage.getItem(this.localPathData)).idAgent, this.nameAgent = JSON.parse(localStorage.getItem(this.localPathData)).nameAgent, this.idAgent = JSON.parse(localStorage.getItem(this.localPathData)).idAgent, c.a.post(this.pathServer + "/checkSup", { idAgent: window.idAg, tokenUser: localStorage.getItem(this.localPathJToken) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(e) { "sucess" === e.data ? t.menu[1].hidden = !0 : (t.menu[2].hidden = !0, t.menu[3].hidden = !0, t.menu[4].hidden = !0) }))), this.socket.off("timer_online"), this.socket.off("timer_break"), this.socket.off("new_info"), localStorage.getItem(this.localPathState) && (this.state = localStorage.getItem(this.localPathState))
                },
                mounted: function() {
                    var t = this;
                    this.socket.on("timer_online", (function(e) { t.seconds_online = e })), this.socket.on("timer_break", (function(e) { t.seconds_break = e })), this.socket.on("new_info", (function(e) {
                        var a = localStorage.getItem(t.localPathJToken);
                        if (e.agent == a) {
                            var s = JSON.parse(localStorage.getItem(t.localPathData));
                            s.capacity = e.capacity, localStorage.setItem(t.localPathData, JSON.stringify(s)), t.socket.emit("Agent", a, { checkCola: a })
                        }
                    })), localStorage.getItem(this.localPathJToken) && (this.isActive = !1, this.navDis = "")
                }
            },
            u = h,
            m = (a("c327"), a("2877")),
            p = Object(m["a"])(u, i, n, !1, null, "9c01ca4a", null),
            g = p.exports,
            f = a("8c4f"),
            b = function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", [a("b-container", { attrs: { fluid: "" } }, [a("div", { staticClass: "row p-5", staticStyle: { height: "550px", "margin-top": "80px", transition: "all 700ms", "margin-bottom": "40px" }, style: { "margin-left": t.marginCom() } }, [a("div", { staticClass: "col-12" }, [a("ChatCom")], 1)]), a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n        -\n        "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])])], 1)
            },
            v = [],
            w = function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("b-container", { staticClass: "p-0 mx-auto", attrs: { fluid: "" } }, [s("b-row", { staticClass: "p-0", staticStyle: { background: "#FAFAFA", "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)" } }, [s("b-col", { staticClass: "colConver", attrs: { cols: "3 p-0" } }, [s("div", { staticClass: "p-0", staticStyle: { height: "70vh" } }, [s("div", { staticClass: "d-flex flex-column columnConversations" }, [s("p", { staticClass: "text-light text-center my-4 d-none d-md-block", staticStyle: { "font-weight": "bold", "word-break": "break-word" } }, [t._v("Conversaciones")]), t._l(t.items, (function(e, i) { return s("div", { key: e.sockId, staticClass: "text-center p-1 ml-4", staticStyle: { "border-top": "1px solid #838383" }, attrs: { id: "divClick" }, on: { click: function(a) { t.toEnd(i), t.setActive(e, i) } } }, [s("div", { key: e.iduser, staticClass: "row", class: { divIsSelected: t.activeIndex === i } }, [s("div", { staticClass: "p-2", staticStyle: { width: "35px" } }, [s("b-img", { staticClass: "d-none d-md-block img-fluid", attrs: { src: a("bb6e")("./" + t.checkChannel(i)) } })], 1), s("div", { staticClass: "d-flex flex-column" }, [s("span", { staticClass: "text-light", staticStyle: { "font-weight": "bold", "font-size": "15px" } }, [t._v(t._s(e.name))]), s("span", { key: t.componentKey, staticClass: "text-light", staticStyle: { "font-size": "10px" } }, [t._v(t._s(t.cutMessage(e.lastMessage)))])]), s("div", { staticClass: "d-flex", staticStyle: { padding: "0px" } }, [s("span", { key: e.count, staticClass: "paragrahpNoti align-self-center", class: { "d-none": t.styleNotification(e.count) } }, [t._v(t._s(e.count))])])])]) }))], 2)])]), s("b-col", { attrs: { cols: "9 p-0 border" } }, [s("div", { staticClass: "p-0", staticStyle: { width: "100%" } }, [s("b-container", [s("b-row", { staticClass: "d-flex justify-content-center flex-nowrap", staticStyle: { height: "100px" } }, [s("b-col", { staticClass: "flex-column ml-3 mt-4", staticStyle: { "border-bottom": "1px solid var(--main-color)" }, style: { display: t.showOptions } }, [s("span", { staticStyle: { color: "var(--main-color)", "font-weight": "bold", "font-size": "10px" } }, [t._v("Hablas con:")]), s("span", { staticClass: "text-dark", staticStyle: { "font-size": "14px" } }, [t._v(t._s(t.nameUser))])]), s("span", { staticClass: "text-dark align-self-center text-center", staticStyle: { "font-weight": "bold" }, style: { display: t.showIntro } }, [t._v("Para comenzar selecciona una conversación")]), s("b-col", { staticClass: "justify-content-around mt-4", staticStyle: { "border-bottom": "1px solid var(--main-color)", "z-index": "3" }, style: { display: t.showOptions } }, [s("div", { staticClass: "historyContainer d-flex justify-content-around", attrs: { id: "divClick" }, on: { click: function(e) { t.showHistory(), t.$bvModal.show("bv-modal-example") } } }, [s("span", { staticClass: "text-dark align-self-center d-none d-md-block", staticStyle: { "font-weight": "bold", "font-size": "12px" } }, [t._v("Ver Historial")]), s("div", { staticClass: "align-self-center" }, [s("img", { staticStyle: { width: "30px", height: "30px" }, attrs: { src: a("6f6b") } })]), s("b-modal", { attrs: { id: "bv-modal-example", scrollable: "" } }, [s("template", { slot: "modal-title" }), s("b-container", [s("b-row", [s("b-col", { staticClass: "px-md-2 mt-1", staticStyle: { height: "50vh" } }, [s("div", { staticClass: "bot-web-chat" }, [s("div", { staticClass: "bot-web-chat_messages", attrs: { id: "winMessages" } }, [s("table", { staticClass: "bot-web-chat_messages-table" }, [s("tbody", [s("tr", [s("td", { attrs: { id: "messages" } }, t._l(t.history, (function(e, i) { return s("div", { key: i, staticClass: "p-2" }, ["agent" === e.who ? s("div", { staticClass: "user-request d-flex justify-content-around" }, [s("div", { staticClass: "chatBubbleAgentHistory" }, [s("span", { domProps: { innerHTML: t._s(t.renderMessageRegexBot(e.message)) } }, [s("br")]), s("span", { staticStyle: { float: "right", "font-size": "10px" } }, [t._v(t._s(e.hora))])]), s("img", { staticClass: "align-self-center", staticStyle: { width: "30px", height: "30px", "margin-left": "10px" }, attrs: { src: a("6364") } })]) : s("div", { staticClass: "server-response d-flex justify-content-around" }, [s("img", { staticClass: "align-self-center", staticStyle: { width: "30px", height: "30px", "margin-right": "10px" }, attrs: { src: a("811d") } }), s("div", { staticClass: "chatBubbleUserHistory text-light" }, [s("span", { domProps: { innerHTML: t._s(t.renderMessageRegexBot(e.message)) } }, [s("br")]), s("span", { staticStyle: { float: "right", "font-size": "10px" } }, [t._v(t._s(e.hora))])])])]) })), 0)])])])])])])], 1)], 1), s("div", { staticClass: "d-flex justify-content-around w-100", attrs: { slot: "modal-footer" }, slot: "modal-footer" })], 2)], 1)]), s("b-col", { staticClass: "justify-content-around mt-4 mr-3", staticStyle: { "border-bottom": "1px solid var(--main-color)", "z-index": "3" }, style: { display: t.showOptions } }, [s("div", { staticClass: "tipConversation d-flex justify-content-around", attrs: { id: "divClick" }, on: { click: function(e) { return t.$bvModal.show("bv-modal-tipificacion") } } }, [s("span", { staticClass: "text-dark align-self-center d-none d-md-block", staticStyle: { "font-weight": "bold", padding: "12px", "font-size": "12px" } }, [t._v("Tipificar conversación")]), s("div", { staticClass: "align-self-center" }, [s("img", { staticStyle: { width: "30px", height: "30px" }, attrs: { src: a("d5e9") } })]), s("b-modal", { attrs: { id: "bv-modal-tipificacion", "hide-footer": "", "hide-header": "", scrollable: "" } }, [s("template", { slot: "modal-title" }), s("div", [s("span", { staticClass: "text-dark mb-3", staticStyle: { "font-weight": "bold" } }, [t._v("Tipificar conversación")]), s("b-alert", { attrs: { variant: "danger", dismissible: "" }, model: { value: t.showDismissibleTip, callback: function(e) { t.showDismissibleTip = e }, expression: "showDismissibleTip" } }, [t._v("Por favor selecciona alguna!")]), s("b-form-select", { staticClass: "mt-3", attrs: { options: t.options }, model: { value: t.selected, callback: function(e) { t.selected = e }, expression: "selected" } }), s("div", { staticClass: "mt-3" }, [s("span", { staticClass: "text-dark", staticStyle: { "font-weight": "bold" } }, [t._v("\n                          Seleccionado:\n                          "), s("strong", [t._v(t._s(t.selected))])])])], 1), s("div", { staticClass: "d-flex justify-content-around" }, [s("b-button", { staticClass: "mt-2 align-self-center", on: { click: function(e) { return t.endConversation() } } }, [t._v("Finalizar")]), s("b-button", { staticClass: "mt-2 align-self-center", on: { click: function(e) { return t.$bvModal.hide("bv-modal-tipificacion") } } }, [t._v("Cerrar")])], 1)], 2)], 1)])], 1), s("b-row", [s("b-col", { staticStyle: { height: "55vh" } }, [s("div", { staticClass: "bot-web-chat" }, [s("div", { staticClass: "bot-web-chat_messages", attrs: { id: "winMessages" } }, [s("table", { staticClass: "bot-web-chat_messages-table" }, [s("tbody", [s("tr", [s("td", { attrs: { id: "messages" } }, t._l(t.messagesSockets(t.messages), (function(e) { return s("div", { key: e.idMess }, ["agent" === e.who ? s("div", { staticClass: "user-request text-dark d-flex justify-content-around" }, [s("div", { staticClass: "chatBubbleAgent" }, [s("span", { domProps: { innerHTML: t._s(t.renderMessageRegex(e.message)) } }, [s("br")]), s("span", { staticStyle: { float: "right", "font-size": "10px" } }, [t._v("\n                                    " + t._s(e.hora) + "\n                                  ")])]), s("img", { staticClass: "align-self-center", staticStyle: { width: "30px", height: "30px", "margin-left": "10px" }, attrs: { src: a("6364") } })]) : s("div", { staticClass: "server-response text-light d-flex justify-content-around" }, [s("img", { staticClass: "align-self-center", staticStyle: { width: "30px", height: "30px", "margin-right": "10px" }, attrs: { src: a("811d") } }), s("div", { staticClass: "chatBubbleUser" }, [s("span", { domProps: { innerHTML: t._s(t.renderMessageRegex(e.message)) } }, [s("br")]), s("span", { staticStyle: { float: "right", "font-size": "10px" } }, [t._v("\n                                    " + t._s(e.hora) + "\n                                  ")])])])]) })), 0)])])])])])])], 1), s("b-row", [s("b-col", { staticClass: "p-0 d-flex around border-top", staticStyle: { "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)" }, attrs: { cols: "10" } }, [s("textarea", { directives: [{ name: "model", rawName: "v-model", value: t.message, expression: "message" }], ref: "ta", staticClass: "border-0 p-2", staticStyle: { height: "40px", width: "100%" }, attrs: { placeholder: "Escribe un mensaje...", autocomplete: "off", disabled: t.activeChat }, domProps: { value: t.message }, on: { keydown: function(e) { return !e.type.indexOf("key") && t._k(e.keyCode, "enter", 13, e.key, "Enter") ? null : (e.preventDefault(), t.submit(e)) }, keyup: t.shorts, click: t.toEndChat, input: function(e) { e.target.composing || (t.message = e.target.value) } } })]), s("b-col", { staticClass: "p-0 d-flex border-top justify-content-center pt-2", staticStyle: { "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)" }, attrs: { id: "divClick", disabled: t.activeChat } }, [s("label", { attrs: { for: "file" } }, [s("img", { staticClass: "align-self-center", staticStyle: { width: "25px", height: "25px" }, attrs: { src: a("9bd9") } })]), s("input", { staticClass: "d-none", attrs: { type: "file", id: "file" }, on: { change: t.handleFiles } })]), s("b-col", { staticClass: "p-0 d-flex border-top justify-content-center", staticStyle: { "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)" }, attrs: { id: "divClick", disabled: t.activeChat } }, [s("img", { staticClass: "align-self-center", staticStyle: { width: "25px", height: "25px" }, attrs: { src: a("d99d") }, on: { click: t.openEmojiWin } }), s("picker", { style: { display: t.emojiDisplay, position: "absolute", bottom: "34px", right: "20px" }, attrs: { set: "emojione", data: t.emojiIndex, title: "Escoge tu emoji…", emoji: "point_up", i18n: { search: "Buscar", categories: { search: "Resultados", recent: "Recientes", people: "Sonrisas & Gente", nature: "Animales & Naturaleza", foods: "Comida & bebida", activity: "Actividades", places: "Viajes & Lugares", objects: "Objetos", symbols: "Simbolos", flags: "Banderas" } } }, on: { select: t.addEmoji } })], 1), s("b-col", { staticClass: "p-0 d-flex border-top justify-content-center", staticStyle: { "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)" }, attrs: { id: "divClick", disabled: t.activeChat }, on: { click: t.submit } }, [s("img", { staticClass: "align-self-center", staticStyle: { width: "25px", height: "25px" }, attrs: { src: a("c23e") } })]), s("b-modal", { attrs: { id: "bv-modal-shorts", scrollable: "", size: "lg" } }, [s("template", { slot: "modal-title" }), s("b-container", [s("div", { staticClass: "row w-75 mx-auto rounded-lg justify-content-around mt-3 font-weight-bold flex-nowrap tableAgent" }, [s("div", { staticClass: "col-3 text-center align-self-center colTable" }, [t._v("Atajo")]), s("div", { staticClass: "col text-center align-self-center colTable" }, [t._v("Texto")])]), t._l(t.shortcuts, (function(e) { return s("div", { key: e.value, staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-around flex-nowrap tableAgents" }, [s("div", { staticClass: "col-3 text-center align-self-center colTable" }, [s("div", { staticClass: "text-center" }, [t._v(t._s(e.value))])]), s("div", { staticClass: "col text-center align-self-center" }, [s("div", { staticClass: "text-center" }, [t._v(t._s(e.text))])])]) }))], 2), s("div", { staticClass: "d-flex justify-content-around w-100", attrs: { slot: "modal-footer" }, slot: "modal-footer" })], 2), s("b-col", { staticClass: "p-0 d-flex border-top justify-content-center", staticStyle: { "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)" }, attrs: { id: "divClick", disabled: t.activeChat }, on: { click: function(e) { return t.$bvModal.show("bv-modal-shorts") } } }, [s("img", { staticClass: "align-self-center", staticStyle: { width: "25px", height: "25px" }, attrs: { src: a("5009") } })])], 1)], 1)], 1)])], 1), s("b-row", { staticClass: "p-0", staticStyle: { height: "15px", "background-color": "var(--main-color)" } })], 1)
            },
            x = [],
            k = (a("ac6a"), a("f3e2"), a("a481"), a("20d6"), a("7f7f"), a("28a5"), a("4f37"), a("d25f"), a("4917"), a("96cf"), a("3b8d")),
            y = a("4793"),
            C = a.n(y),
            _ = a("364d"),
            A = a("41fb"),
            S = (a("01ce"), new A["EmojiIndex"](_));
        a("15f7");
        var T = {
                components: { Picker: A["Picker"] },
                data: function() { return { publicPath: "/bothuman/montechelo/", pathServer: "https://dashbots.outsourcingcos.com/widget/montechelo/tasks", localPathData: "data_agent_bot_montechelo", localPathJToken: "jwt_bothuman_montechelo", localPathState: "state_montechelos", activeIndex: void 0, message: "", namUs: "", nameUser: "", items: [], messages: [], history: [], showDismissibleTip: !1, showOptions: "none", backgDriv: "", showIntro: "", hora: this.nowHour(), showContainer: "none", selected: null, emojiIndex: S, activeChat: !0, isActive: !0, componentKey: 0, emojiDisplay: "none", options: [{ value: null, text: "Por favor, selecciona la tipificación" }], shortcuts: [] } },
                methods: {
                    handleFiles: function() {
                        var t = Object(k["a"])(regeneratorRuntime.mark((function t(e) {
                            var a;
                            return regeneratorRuntime.wrap((function(t) {
                                while (1) switch (t.prev = t.next) {
                                    case 0:
                                        return a = e.target.files[0], t.next = 3, this.toBase64(a);
                                    case 3:
                                        this.message = t.sent, this.submit(e, !0, a.type);
                                    case 5:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this)
                        })));

                        function e(e) { return t.apply(this, arguments) }
                        return e
                    }(),
                    toBase64: function(t) {
                        return new Promise((function(e, a) {
                            var s = new FileReader;
                            s.readAsDataURL(t), s.onload = function() { return e(s.result) }, s.onerror = function(t) { return a(t) }
                        }))
                    },
                    getMimeBase64: function(t) { var e = null; if ("string" !== typeof t) return e; var a = t.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/); return a && a.length && (e = a[1]), e },
                    addEmoji: function(t) {
                        var e = t.native,
                            a = this.$refs.ta;
                        if (document.selection) a.focus(), this.message = e, this.openEmojiWin();
                        else if (a.selectionStart || "0" == a.selectionStart) {
                            a.focus();
                            var s = a.selectionStart,
                                i = a.selectionEnd;
                            this.message = a.value.substring(0, s) + e + a.value.substring(i, a.value.length), a.setSelectionRange(i + e.length, i + e.length), this.openEmojiWin()
                        } else this.message += e, this.openEmojiWin()
                    },
                    openEmojiWin: function() { 1 != this.activeChat && ("none" === this.emojiDisplay ? this.emojiDisplay = "" : this.emojiDisplay = "none") },
                    shorts: function() {
                        var t = this;
                        if ("/" === this.message[0]) {
                            var e = this.shortcuts.filter((function(e) { return "/" + e.value === t.message }));
                            e[0] && (this.message = e[0].text)
                        }
                    },
                    checkShorts: function() {},
                    setActive: function(t, e) { this.activeIndex = e, this.items.iduser = t.iduser },
                    checkChannel: function(t) { return "whatsapp" === this.items[t].channel ? "whatsapp.svg" : "webchat" === this.items[t].channel ? "chat.svg" : "Facebook Messenger" === this.items[t].channel ? "facebook-messenger.png" : void 0 },
                    nowHour: function() {
                        var t = new Date,
                            e = t.getHours(),
                            a = t.getMinutes(),
                            s = t.getSeconds(),
                            i = " ";
                        return e < 10 && (e = "0" + e), a < 10 && (a = "0" + a), i = e > 12 || 12 == e ? "pm" : "am", s < 10 && (s = "0" + s), e + ":" + a + ":" + s + " " + i
                    },
                    submit: function(t, e, a) {
                        var s = this;
                        t.preventDefault(), e ? (this.items[window.indexActive].lastMessage = this.message, this.items[window.indexActive].count = 0, this.messages.push({ socketid: this.namUs, message: this.message, idcon: window.idConver, hora: this.nowHour(), who: "agent" }), c.a.post(this.pathServer + "/saveMessageAgent", { idUser: window.useractive, idSocket: window.idConver, message: "sended-files/agent-user-".concat(window.idConver, "-").concat((new Date).getHours()).concat((new Date).getMinutes()).concat((new Date).getSeconds(), ".").concat(a.split("/")[1]), from: "agent" }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function() { s.toEndChat() })), this.$parent.socket.emit("Agent-User", window.activeConversation, this.message, null, window.idConver, window.activeChannel, "agent", "agent-user-".concat(window.idConver, "-").concat((new Date).getHours()).concat((new Date).getMinutes()).concat((new Date).getSeconds(), ".").concat(a.split("/")[1])), this.message = "") : "" != this.message && "" != this.message.trim() && (this.items[window.indexActive].lastMessage = this.message, this.items[window.indexActive].count = 0, this.messages.push({ socketid: this.namUs, message: this.message, idcon: window.idConver, hora: this.nowHour(), who: "agent" }), c.a.post(this.pathServer + "/saveMessageAgent", { idUser: window.useractive, idSocket: window.idConver, message: this.message, from: "agent" }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function() { s.toEndChat() })), this.$parent.socket.emit("Agent-User", window.activeConversation, this.message, null, window.idConver, window.activeChannel, "agent"), this.message = "")
                    },
                    toEnd: function() {
                        var t = Object(k["a"])(regeneratorRuntime.mark((function t(e) {
                            return regeneratorRuntime.wrap((function(t) {
                                while (1) switch (t.prev = t.next) {
                                    case 0:
                                        return t.next = 2, this.send(e);
                                    case 2:
                                        this.toEndChat();
                                    case 3:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this)
                        })));

                        function e(e) { return t.apply(this, arguments) }
                        return e
                    }(),
                    toEndChat: function() { window.objDiv.scrollTop = window.objDiv.scrollHeight },
                    styleDiv: function() { var t = { background: "#4B2973" }; return t },
                    send: function(t) { this.selected = null, this.showOptions = "flex", this.showIntro = "none", this.activeChat = !1, this.namUs = this.items[t].sockid, this.nameUser = this.items[t].name, window.activeConversation = this.items[t].sockid, window.useractive = this.items[t].iduser, window.idConver = this.items[t].conid, window.activeChannel = this.items[t].channel, this.items[t].count = 0, window.indexActive = t },
                    showHistory: function() {
                        var t = this;
                        c.a.post(this.pathServer + "/callAllconversation", { idUser: window.useractive, tokenUser: window.AgenToken }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(e) { t.history = e.data }))
                    },
                    endConversation: function() {
                        var t = this;
                        null === this.selected ? this.showDismissibleTip = !0 : c.a.post(this.pathServer + "/updateConversation", { estado: this.selected, idSocket: window.activeConversation, idConversation: window.idConver, tokenUser: window.AgenToken }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function() {
                            var e = localStorage.getItem(t.localPathJToken);
                            t.$parent.socket.emit("Agent-User", window.activeConversation, e, 1, window.idConver, window.activeChannel, "agent"), t.$parent.socket.emit("Agent", e, { checkCola: e }), t.items.splice(window.indexActive, 1), t.$bvModal.hide("bv-modal-tipificacion"), t.selected = null, t.nameUser = "", t.showOptions = "none", t.showIntro = "", t.activeChat = !0, window.activeConversation = null, window.idConver = null, window.activeChannel = null, window.useractive = null
                        }))
                    },
                    messagesSockets: function() { return this.messages.filter((function(t) { return t.socketid === window.activeConversation && t.idcon === window.idConver })) },
                    historySockets: function() { return this.history.filter((function(t) { return t.id_chat_user === window.useractive })) },
                    spawnNotification: function(t, e) {
                        var a = { body: t };
                        new Notification(e, a)
                    },
                    searchIndexUser: function(t) { var e = this.items.findIndex((function(e) { return e.sockid === t })); return e },
                    styleNotification: function(t) { return 0 === parseInt(t) },
                    cutMessage: function(t) {
                        if (/\.(jpe?g|png|gif|bmp)/i.test(t) || /\.(undefined|pdf)/i.test(t)) return "Archivo multimedia";
                        if (C()(t, { allowMime: !0 }) && t.length > 1e3) return "Archivo multimedia";
                        var e = "";
                        if (t) {
                            var a = window.innerWidth;
                            e = a > 960 ? t.substr(0, 15) : t.substr(0, 5)
                        }
                        return e + "..."
                    },
                    renderMessageRegex: function(t) { if (t) { var e = ""; if (/^http/.test(t) || (e = "https://dashbots.outsourcingcos.com/widget/montechelo/"), /\.(jpe?g|png|gif|bmp)/i.test(t)) return '<a href="'.concat(e).concat(t, '" target="_blank"><img src="').concat(e).concat(t, '" class="img-fluid w-50"/></a><br>'); if (/\.(undefined|pdf)/i.test(t)) return '\n              <a href="'.concat(e).concat(t, '" target="_blank" download>\n                Archivo\n                <img src="download.png" width="30" height="30">\n              </a>\n              '); if (t.split(" ").length > 1 || t.length > 500) { if (C()(t, { allowMime: !0 })) { var a = this.getMimeBase64(t); return "image" == a.split("/")[0] ? '<img src="'.concat(t, '" class="img-fluid w-50"/><br>') : '\n              <a href="'.concat(t, '" download>\n                Archivo\n                <img src="download.png" width="30" height="30">\n              </a>\n              ') } var s = t.replace(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm, (function(t) { return '<a target="autoblank" href="' + t + '">' + t + "</a>" })); return s + "<br>" } var i = t.replace(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm, (function(t) { return '<a target="autoblank" href="' + t + '">' + t + "</a>" })); return i + "<br>" } return t },
                    renderMessageRegexBot: function(t) { if (t) { var e = ""; if (/^http/.test(t) || (e = "https://dashbots.outsourcingcos.com/widget/montechelo/"), /\.(jpe?g|png|gif|bmp)/i.test(t)) return '<a href="'.concat(e).concat(t, '" target="_blank"><img src="').concat(e).concat(t, '" class="img-fluid w-50"/></a><br>'); if (/\.(undefined|pdf)/i.test(t)) return '\n              <a href="'.concat(e).concat(t, '" target="_blank" download>\n                Archivo\n                <img src="download.png" width="30" height="30">\n              </a>\n              '); if (t.split(" ").length > 1 || t.length > 500) { if (C()(t, { allowMime: !0 })) { var a = this.getMimeBase64(t); return "image" == a.split("/")[0] ? '<img src="'.concat(t, '" class="img-fluid w-50"/><br>') : '\n              <a href="'.concat(t, '" download>\n                Archivo\n                <img src="download.png" width="30" height="30">\n              </a>\n              ') } var s = t.replace(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm, (function(t) { return '<a target="autoblank" href="' + t + '">' + t + "</a>" })); return s + "<br>" } var i = t.replace(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm, (function(t) { return '<a target="autoblank" href="' + t + '">' + t + "</a>" })); return i + "<br>" } return t }
                },
                beforeMount: function() {
                    var t = this;
                    this.$parent.socket.off("event"), this.$parent.socket.off("private-message"), document.removeEventListener("visibilitychange", (function() {}), !0), c.a.post(this.pathServer + "/callTypifyAgent", { tokenUser: localStorage.getItem(this.localPathJToken) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(e) {
                        var a = t;
                        e.data.forEach((function(t) { a.options.push(t) }))
                    })), c.a.post(this.pathServer + "/callShortcutAgent", { tokenUser: localStorage.getItem(this.localPathJToken) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(e) { t.shortcuts = e.data })), localStorage.getItem(this.localPathData) && (window.idAg = JSON.parse(localStorage.getItem(this.localPathData)).idAgent, window.nameAg = JSON.parse(localStorage.getItem(this.localPathData)).nameAgent, window.capacity = JSON.parse(localStorage.getItem(this.localPathData)).capacity, window.AgenToken = localStorage.getItem(this.localPathJToken), c.a.post(this.pathServer + "/callConversations", { idAgent: window.idAg, tokenUser: window.AgenToken }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(e) {
                        e.data.forEach(function() {
                            var e = Object(k["a"])(regeneratorRuntime.mark((function e(a) {
                                return regeneratorRuntime.wrap((function(e) {
                                    while (1) switch (e.prev = e.next) {
                                        case 0:
                                            return t.$parent.socket.emit("Agent-User", a.sockid, null, null, null, a.channel, "agent"), e.next = 3, c.a.post(t.pathServer + "/callConversationAgent", { idSocket: a.sockid, idCons: a.conid, tokenUser: window.AgenToken }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(t.localPathData)).jsonWebToken } }).then((function(e) {
                                                var s = t.searchIndexUser(a.sockid);
                                                e.data.forEach((function(e) { t.messages.push(e) })), t.items[s].lastMessage = e.data[e.data.length - 1].message
                                            }));
                                        case 3:
                                        case "end":
                                            return e.stop()
                                    }
                                }), e)
                            })));
                            return function(t) { return e.apply(this, arguments) }
                        }()), t.items = e.data;
                        var a = localStorage.getItem(t.localPathJToken),
                            s = e.data.length;
                        t.$parent.socket.emit("Agent", a, { idSocketAgent: a, id: window.idAg, numeroCon: s, stateAgent: t.$parent.state, nameAgent: window.nameAg, capacity: window.capacity })
                    })))
                },
                mounted: function() {
                    var t = this;
                    "Notification" in window ? "granted" === Notification.permission || "denied" !== Notification.permission && Notification.requestPermission((function() {})) : alert("This browser does not support desktop notification"), window.objDiv = document.getElementById("winMessages"), document.addEventListener("visibilitychange", (function() { window.docuVisi = document.visibilityState }), !0), this.$parent.socket.on("event", (function(e, a) { c.a.post(t.pathServer + "/saveConvertation", { idSocket: e.socketId, idUser: e.id, estado: "Abierta", idAgent: window.idAg, tokenUser: window.AgenToken, channel: a }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(t.localPathData)).jsonWebToken } }).then((function() { c.a.post(t.pathServer + "/callUser", { idUser: e.id, tokenUser: window.AgenToken }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(t.localPathData)).jsonWebToken } }).then((function(s) { "hidden" == window.docuVisi && t.spawnNotification(s.data[0].name, "Nueva Convesacion abierta con:"), t.items.push({ iduser: s.data[0].id, conid: s.data[0].idcon, sockid: e.socketId, estado: "Abierta", name: s.data[0].name, email: s.data[0].email, idagent: window.idAg, count: 0, lastMessage: "Hola mi nombre es " + window.nameAg + "!", channel: a }), t.$parent.socket.emit("Agent-User", e.socketId, "Hola mi nombre es " + window.nameAg + "!", 2, s.data[0].idcon, a, "agent"), t.messages.push({ socketid: e.socketId, message: "Hola mi nombre es " + window.nameAg + "!", idcon: s.data[0].idcon, who: "agent" }) })) })) })), this.$parent.socket.on("private-message", function() {
                        var e = Object(k["a"])(regeneratorRuntime.mark((function e(a) {
                            var s, i;
                            return regeneratorRuntime.wrap((function(e) {
                                while (1) switch (e.prev = e.next) {
                                    case 0:
                                        if (null == a.messageUser) { e.next = 17; break }
                                        if ("hidden" == window.docuVisi && (s = t.searchIndexUser(a.socketIdUser), a.socketIdUser == window.activeConversation && (t.items[s].count = parseInt(t.items[s].count) + 1), t.spawnNotification(a.messageUser, "Nuevo Message: ")), a.state) { e.next = 7; break }
                                        return e.next = 5, t.messages.push({ socketid: a.socketIdUser, message: a.messageUser, idcon: parseInt(a.idCon), hora: t.nowHour(), count: 1 });
                                    case 5:
                                        e.next = 10;
                                        break;
                                    case 7:
                                        if (4 != a.state) { e.next = 10; break }
                                        return e.next = 10, t.messages.push({ socketid: a.socketIdUser, message: a.messageUser, idcon: parseInt(a.idCon), hora: t.nowHour(), who: "agent", count: 1 });
                                    case 10:
                                        i = t.searchIndexUser(a.socketIdUser), a.socketIdUser != window.activeConversation && (t.items[i].count = parseInt(t.items[i].count) + 1), t.items[i].lastMessage = a.messageUser, t.componentKey += 1, t.toEndChat(), e.next = 18;
                                        break;
                                    case 17:
                                        5 == a.state && null == a.messageUser && c.a.post(t.pathServer + "/updateConversation", { estado: "TMO", idSocket: a.socketIdUser, idConversation: a.idCon, tokenUser: window.AgenToken }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(t.localPathData)).jsonWebToken } }).then((function() {
                                            var e = localStorage.getItem(t.localPathJToken);
                                            t.$parent.socket.emit("Agent-User", a.socketIdUser, e, 1, a.idCon, a.channel, "agent"), t.$parent.socket.emit("Agent", e, { checkCola: e });
                                            var s = t.items.findIndex((function(t) { return t.sockid === a.socketIdUser }));
                                            t.items.splice(s, 1), s === window.indexActive && (t.selected = null, t.nameUser = "", t.showOptions = "none", t.showIntro = "", t.activeChat = !0, window.activeConversation = null, window.idConver = null, window.activeChannel = null, window.useractive = null)
                                        }));
                                    case 18:
                                    case "end":
                                        return e.stop()
                                }
                            }), e)
                        })));
                        return function(t) { return e.apply(this, arguments) }
                    }())
                }
            },
            D = T,
            I = (a("d000"), Object(m["a"])(D, w, x, !1, null, "56aa00f2", null)),
            j = I.exports,
            P = { name: "home", components: { ChatCom: j }, data: function() { return { socket: this.$parent.socket, state: this.$parent.state } }, methods: { marginCom: function() { return "50px" == this.$parent.mgleftNav ? "0px" : "150px" } } },
            E = P,
            N = Object(m["a"])(E, b, v, !1, null, null, null),
            O = N.exports,
            B = function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticStyle: { "margin-top": "80px", transition: "all 700ms" }, style: { "margin-left": t.marginCom() } }, [a("ControlCom", { attrs: { id: "main" } }), t._m(0)], 1)
            },
            z = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }],
            $ = function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("div", { staticClass: "main w-75 mx-auto" }, [s("div", { staticClass: "row justify-content-center" }, [s("b-modal", { attrs: { static: !0, size: "xl", id: "bv-modal-example", scrollable: "" } }, [s("div", { attrs: { id: "conversacio" } }), s("template", { slot: "modal-title" }), s("b-container", [s("b-row", { staticClass: "p-0", staticStyle: { background: "#FAFAFA", "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)" } }, [s("b-col", { staticClass: "colConver", attrs: { cols: "3 p-0" } }, [s("div", { staticClass: "p-0", staticStyle: { height: "70vh" } }, [s("div", { staticClass: "d-flex flex-column columnConversations" }, [s("h3", { staticClass: "text-light text-center mt-5 mb-5 ", staticStyle: { "font-weight": "bold", "word-break": "break-word" } }, [t._v("\n                  Conversaciones\n                ")]), t._l(t.items, (function(e, i) { return s("div", { key: e.sockId, staticClass: "text-center p-1 ml-4", staticStyle: { "border-top": "1px solid #838383" }, attrs: { id: "divClick" }, on: { click: function(a) { t.toEnd(i), t.setActive(e, i) } } }, [s("div", { key: e.iduser, staticClass: "row p-2", class: { divIsSelected: t.activeIndex === i } }, [s("b-img", { staticClass: "col-2 d-flex", staticStyle: { width: "44px", padding: "8px" }, attrs: { src: a("bb6e")("./" + t.checkChannel(i)) } }), s("div", { staticClass: "col-8 d-flex flex-column" }, [s("span", { staticClass: "text-light", staticStyle: { "font-weight": "bold" } }, [t._v(t._s(e.name) + "\n                      ")]), s("span", { key: t.componentKey, staticClass: "text-light" }, [t._v("\n                        " + t._s(t.cutMessage(e.lastMessage)) + "\n                      ")])]), s("div", { staticClass: "col-2 d-flex", staticStyle: { padding: "0px" } }, [s("span", { key: e.count, staticClass: "paragrahpNoti align-self-center", class: { "d-none": t.styleNotification(e.count) } }, [t._v(t._s(e.count))])])], 1)]) }))], 2)])]), s("b-col", { attrs: { cols: "9 p-0 border" } }, [s("div", { staticClass: "p-0", staticStyle: { width: "100%" } }, [s("b-container", [s("b-row", [s("b-col", { staticStyle: { height: "69vh" } }, [s("div", { staticClass: "bot-web-chat" }, [s("div", { staticClass: "bot-web-chat_messages", attrs: { id: "winMessagess" } }, [s("table", { staticClass: "bot-web-chat_messages-table" }, [s("tbody", [s("tr", [s("td", { attrs: { id: "messages" } }, t._l(t.messagesSockets(t.messages), (function(e) { return s("div", { key: e.idMess }, ["agent" === e.who ? s("div", { staticClass: "user-request text-dark d-flex justify-content-around" }, [s("div", { staticClass: "chatBubbleAgent" }, [s("span", { domProps: { innerHTML: t._s(t.renderMessageRegex(e.message)) } }, [s("br")]), s("span", { staticStyle: { float: "right", "font-size": "10px" } }, [t._v(t._s(e.hora))])]), s("img", { staticClass: "align-self-center", staticStyle: { width: "30px", height: "30px", "margin-left": "10px" }, attrs: { src: a("6364") } })]) : s("div", { staticClass: "server-response text-light d-flex justify-content-around" }, [s("img", { staticClass: "align-self-center", staticStyle: { width: "30px", height: "30px", "margin-right": "10px" }, attrs: { src: a("811d") } }), s("div", { staticClass: "chatBubbleUser" }, [s("span", { domProps: { innerHTML: t._s(t.renderMessageRegex(e.message)) } }, [s("br")]), s("span", { staticStyle: { float: "right", "font-size": "10px" } }, [t._v(t._s(e.hora))])])])]) })), 0)])])])])])])], 1)], 1)], 1)])], 1)], 1), s("div", { staticClass: "d-flex justify-content-around w-100", attrs: { slot: "modal-footer" }, slot: "modal-footer" })], 2), s("div", { staticClass: "col", staticStyle: { background: "#FAFAFA", "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)", padding: "34px", margin: "40px", "margin-bottom": "43px" } }, [t._m(0), s("div", { staticClass: "row d-flex justify-content-around w-100 mx-auto rounded-lg text-dark " }, [s("div", { staticClass: "col border rounded-lg p-2 m-3 summary d-flex flex-column text-center" }, [s("span", { staticClass: "align-self-center font-weight-bold" }, [t._v("\n            Conversaciones en Cola")]), s("br"), s("br"), s("span", { key: t.numChatsPend, staticStyle: { "font-size": "40px" } }, [t._v(t._s(t.numChatsPend) + "\n            "), s("b-img", { staticStyle: { width: "55px" }, attrs: { src: a("896a") } })], 1)]), s("div", { staticClass: "col border rounded-lg p-2 m-3 summary d-flex flex-column text-center" }, [s("span", { staticClass: "align-self-center font-weight-bold" }, [t._v("\n            Conversaciones finalizadas")]), s("br"), s("span", { key: t.numChatsFin, staticStyle: { "font-size": "40px" } }, [t._v(t._s(t.numChatsFin) + "\n            "), s("b-img", { staticStyle: { width: "55px" }, attrs: { src: a("56a9") } })], 1)]), s("div", { staticClass: "col border rounded-lg p-2 m-3 summary d-flex flex-column text-center" }, [s("span", { staticClass: "align-self-center font-weight-bold" }, [t._v("\n            Cantidad de Mensajes")]), s("br"), s("br"), s("span", { key: t.numMsgs, staticStyle: { "font-size": "40px" } }, [t._v(t._s(t.numMsgs) + "\n            "), s("b-img", { staticStyle: { width: "55px" }, attrs: { src: a("6310") } })], 1)]), s("div", { staticClass: "col border rounded-lg p-2 m-3 summary d-flex flex-column text-center" }, [s("span", { staticClass: "align-self-center font-weight-bold" }, [t._v("\n            Capacidad Total")]), s("br"), s("br"), s("span", { key: t.chatsActual, staticStyle: { "font-size": "40px" } }, [t._v(t._s(t.chatsActual) + "/" + t._s(t.capacityTotal) + "\n            "), s("b-img", { staticStyle: { width: "55px" }, attrs: { src: a("6310") } })], 1)])]), t._m(1), t._m(2), t._l(t.agents, (function(e, i) { return s("div", { key: e.idAgent, staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-around flex-nowrap tableAgents" }, [s("div", { key: e.nameAgent, staticClass: "col text-center align-self-center colTable" }, [s("span", [t._v(t._s(e.nameAgent))])]), s("div", { key: e.numCon, staticClass: "col text-center align-self-center colTable" }, [s("span", [t._v(t._s(e.numCon) + "/" + t._s(e.capacity))])]), s("div", { key: e.state, staticClass: "col text-center align-self-center colTable" }, [s("span", [t._v(t._s(e.state))])]), s("div", { key: t.filterTime(e), staticClass: "col text-center align-self-center colTable" }, [s("span", [t._v(t._s(t.filterTime(e)))])]), s("div", { staticClass: "col text-center align-self-center" }, [s("b-img", { staticStyle: { width: "40px", cursor: "pointer" }, attrs: { src: a("66fc") }, on: { click: function(e) { t.checkChat(i), t.$bvModal.show("bv-modal-example") } } })], 1)]) }))], 2)], 1)])
            },
            M = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "text-center" }, [a("strong", [a("h1", { staticClass: "inicio mb-3" }, [t._v("Tablero de Control")])])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-center mt-5" }, [a("span", { staticClass: "font-weight-bold", staticStyle: { color: "#002A3A", "font-size": "25px" } }, [t._v("\n          Agentes Conectados\n        ")])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row w-75 mx-auto rounded-lg justify-content-around mt-3 font-weight-bold flex-nowrap tableAgent" }, [a("div", { staticClass: "col text-center align-self-center colTable" }, [t._v("\n          Nombre del Agente\n        ")]), a("div", { staticClass: "col text-center align-self-center colTable" }, [t._v("\n          Numero Conversaciones\n        ")]), a("div", { staticClass: "col text-center align-self-center colTable" }, [t._v("\n          Estado\n        ")]), a("div", { staticClass: "col text-center align-self-center colTable" }, [t._v("\n          Tiempo\n        ")]), a("div", { staticClass: "col text-center align-self-center" }, [t._v("\n          Conversaciones en vivo\n        ")])])
            }],
            U = {
                name: "ControlCom",
                data: function() { return { localPathData: "data_agent_bot_montechelo", pathServer: "https://dashbots.outsourcingcos.com/widget/montechelo/tasks", localPathJToken: "jwt_bothuman_montechelo", localPathState: "state_montechelos", numChatsPend: 0, numChatsFin: 0, numMsgs: 0, capacityTotal: 0, chatsActual: 0, agents: [], items: [], messages: [], activeIndex: void 0, componentKey: 0 } },
                methods: {
                    checkChat: function(t) {
                        var e = this;
                        this.items = [], this.messages = [], window.activeConversation = null, this.activeIndex = null, c.a.post(this.pathServer + "/callConversations", { idAgent: this.agents[t].idAg, tokenUser: localStorage.getItem(this.localPathJToken) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(t) {
                            e.items = t.data, t.data.forEach(function() {
                                var t = Object(k["a"])(regeneratorRuntime.mark((function t(a) {
                                    return regeneratorRuntime.wrap((function(t) {
                                        while (1) switch (t.prev = t.next) {
                                            case 0:
                                                return e.$parent.socket.emit("Agent-User", a.sockid, null, null, null, a.channel), t.next = 3, c.a.post(e.pathServer + "/callConversationAgent", { idSocket: a.sockid, idCons: a.conid, tokenUser: localStorage.getItem(e.localPathJToken) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(e.localPathData)).jsonWebToken } }).then((function(t) {
                                                    var s = e.searchIndexUser(a.sockid);
                                                    t.data.forEach((function(t) { e.messages.push(t) })), e.items[s].lastMessage = t.data[t.data.length - 1].message
                                                }));
                                            case 3:
                                            case "end":
                                                return t.stop()
                                        }
                                    }), t)
                                })));
                                return function(e) { return t.apply(this, arguments) }
                            }())
                        }))
                    },
                    searchIndexUser: function(t) { var e = this.items.findIndex((function(e) { return e.sockid === t })); return e },
                    filterTime: function(t) { return "ONLINE" == t.state || "OCUPADO" == t.state ? t.timerOnline : "BREAK" == t.state ? t.timerBreak : void 0 },
                    setActive: function(t, e) { this.activeIndex = e, this.items.iduser = t.iduser },
                    checkChannel: function(t) { return "whatsapp" === this.items[t].channel ? "whatsapp.svg" : "webchat" === this.items[t].channel ? "chat.svg" : void 0 },
                    nowHour: function() {
                        var t = new Date,
                            e = t.getHours(),
                            a = t.getMinutes(),
                            s = t.getSeconds(),
                            i = " ";
                        return e < 10 && (e = "0" + e), a < 10 && (a = "0" + a), i = e > 12 || 12 == e ? "pm" : "am", s < 10 && (s = "0" + s), e + ":" + a + ":" + s + " " + i
                    },
                    toEnd: function() {
                        var t = Object(k["a"])(regeneratorRuntime.mark((function t(e) {
                            return regeneratorRuntime.wrap((function(t) {
                                while (1) switch (t.prev = t.next) {
                                    case 0:
                                        return t.next = 2, this.send(e);
                                    case 2:
                                        this.toEndChat();
                                    case 3:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this)
                        })));

                        function e(e) { return t.apply(this, arguments) }
                        return e
                    }(),
                    toEndChat: function() { window.objDiv.scrollTop = window.objDiv.scrollHeight },
                    send: function(t) { window.activeConversation = this.items[t].sockid, window.useractive = this.items[t].iduser, window.idConver = this.items[t].conid, window.activeChannel = this.items[t].channel, this.items[t].count = 0, window.indexActive = t },
                    styleDiv: function() { var t = { background: "#4B2973" }; return t },
                    messagesSockets: function() { return this.messages.filter((function(t) { return t.socketid === window.activeConversation && t.idcon === window.idConver })) },
                    styleNotification: function(t) { return 0 === parseInt(t) },
                    cutMessage: function(t) { if (t) { var e = t.substr(0, 25); return e + "..." } return null },
                    renderMessageRegex: function(t) { if (t) { var e = t.replace(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm, (function(t) { return '<a target="autoblank" href="' + t + '">' + t + "</a>" })); return e + "<br>" } return t },
                    renderMessageRegexBot: function(t) {
                        if (t) {
                            var e = t.replace(/\*(\S(.*?\S)?)\*/gm, "<strong>$1</strong>"),
                                a = e.replace(/(?:\r\n|\r|\n)/g, "<br>"),
                                s = a.replace(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm, (function(t) { return '<a target="autoblank" href="' + t + '">' + t + "</a>" }));
                            return s + "<br>"
                        }
                        return t
                    }
                },
                beforeMount: function() {
                    var t = localStorage.getItem(this.localPathJToken);
                    this.$parent.socket.emit("Supervisor", t, { idSocketAgent: t, stateAgent: localStorage.getItem(this.localPathState), nameAgent: JSON.parse(localStorage.getItem(this.localPathData)).nameAgent })
                },
                mounted: function() {
                    var t = this;
                    window.objDiv = document.getElementById("winMessagess"), this.$parent.socket.on("cola_chats", (function(e) { t.numChatsPend = e.chatP, t.numChatsFin = e.chatF, t.agents = e.agents, t.numMsgs = e.allMsg, t.capacityTotal = 0, t.chatsActual = 0, e.agents.forEach((function(e) { t.capacityTotal = t.capacityTotal + e.capacity, t.chatsActual = t.chatsActual + e.numCon })) })), this.$parent.socket.on("private-message", function() {
                        var e = Object(k["a"])(regeneratorRuntime.mark((function e(a) {
                            var s;
                            return regeneratorRuntime.wrap((function(e) {
                                while (1) switch (e.prev = e.next) {
                                    case 0:
                                        if (null == a.messageUser) { e.next = 6; break }
                                        return e.next = 3, t.messages.push({ socketid: a.socketIdUser, message: a.messageUser, idcon: parseInt(a.idCon), hora: t.nowHour(), who: a.who, count: 1 });
                                    case 3:
                                        s = t.searchIndexUser(a.socketIdUser), t.items[s].lastMessage = a.messageUser, t.toEndChat();
                                    case 6:
                                    case "end":
                                        return e.stop()
                                }
                            }), e)
                        })));
                        return function(t) { return e.apply(this, arguments) }
                    }())
                }
            },
            J = U,
            H = (a("d417"), Object(m["a"])(J, $, M, !1, null, "b6066b56", null)),
            F = H.exports,
            W = { name: "control", components: { ControlCom: F }, data: function() { return { socket: this.$parent.socket } }, methods: { marginCom: function() { return "50px" == this.$parent.mgleftNav ? "0px" : "150px" } } },
            R = W,
            q = (a("8fbe"), Object(m["a"])(R, B, z, !1, null, "4fb46cb0", null)),
            V = q.exports,
            L = function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticStyle: { "margin-top": "80px", transition: "all 700ms" }, style: { "margin-left": t.marginCom() } }, [a("RegisterCom"), t._m(0)], 1)
            },
            Z = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }],
            X = function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("div", { staticClass: "main w-50 mx-auto" }, [s("div", { staticClass: "row justify-content-center" }, [s("div", { staticClass: "col", staticStyle: { background: "#FAFAFA", "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)", padding: "34px", margin: "40px", "margin-left": "90px", "margin-bottom": "43px" } }, [t._m(0), s("div", { staticClass: "container mt-2", staticStyle: { "max-width": "450px" } }, [s("div", {}, [s("b-form", [s("b-alert", { attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert, callback: function(e) { t.showDismissibleAlert = e }, expression: "showDismissibleAlert" } }, [t._v("\n              Ya existe un usuario con esa cedula\n              "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), s("b-alert", { attrs: { variant: "success" }, model: { value: t.showDismissibleAlert6, callback: function(e) { t.showDismissibleAlert6 = e }, expression: "showDismissibleAlert6" } }, [t._v("\n              Se ha registrado satisfactoriamente el agente\n            ")]), s("b-form-group", { staticClass: "s-label", staticStyle: { "font-weight": "bold" }, attrs: { label: "Cedula del agente", "label-for": "user" } }, [s("b-alert", { attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert2, callback: function(e) { t.showDismissibleAlert2 = e }, expression: "showDismissibleAlert2" } }, [t._v("\n                Escribe una Cedula de Usuario Correcta\n              ")]), s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputName + ")" }, attrs: { id: "user", type: "text", required: "", autofocus: "", placeholder: "Escribe la cedula usuario..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.user, callback: function(e) { t.user = e }, expression: "user" } })], 1), s("b-form-group", { staticClass: "s-label mt-3 ", staticStyle: { "font-weight": "bold" }, attrs: { label: "Nombre del agente", "label-for": "name" } }, [s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputNameUser + ")" }, attrs: { id: "name", type: "text", required: "", placeholder: "Escribe un nombre..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.name, callback: function(e) { t.name = e }, expression: "name" } })], 1), s("b-form-group", { staticClass: "s-label mt-3 ", staticStyle: { "font-weight": "bold" }, attrs: { label: "Correo eléctronico", "label-for": "email" } }, [s("b-alert", { attrs: { variant: "danger", dismissible: "" }, model: { value: t.showDismissibleAlert4, callback: function(e) { t.showDismissibleAlert4 = e }, expression: "showDismissibleAlert4" } }, [t._v("\n                Escribe un Email Correcto\n              ")]), s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputEmail + ")" }, attrs: { id: "email", type: "email", required: "", placeholder: "Escribe el correo..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.email, callback: function(e) { t.email = e }, expression: "email" } })], 1), s("b-form-group", { staticClass: "s-label mt-3 ", staticStyle: { "font-weight": "bold" }, attrs: { label: "Capacidad Conversaciones", "label-for": "capacity" } }, [s("b-alert", { attrs: { variant: "danger", dismissible: "" }, model: { value: t.showDismissibleAlert7, callback: function(e) { t.showDismissibleAlert7 = e }, expression: "showDismissibleAlert7" } }, [t._v("\n                La capacidad solo debe ser números\n              ")]), s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputEmail + ")" }, attrs: { id: "capacity", type: "text", required: "", placeholder: "Capacidad agente..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.capacity, callback: function(e) { t.capacity = e }, expression: "capacity" } })], 1), s("b-form-group", { staticClass: "s-label mt-3 ", staticStyle: { "font-weight": "bold" }, attrs: { label: "Contraseña", "label-for": "password" } }, [s("b-alert", { attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert3, callback: function(e) { t.showDismissibleAlert3 = e }, expression: "showDismissibleAlert3" } }, [t._v("\n                La contraseña es muy corta\n              ")]), s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputPass + ")" }, attrs: { id: "password", type: "password", required: "", placeholder: "Escribe una contraseña..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.password, callback: function(e) { t.password = e }, expression: "password" } })], 1), s("b-form-group", { staticClass: "s-label mt-3 ", staticStyle: { "font-weight": "bold" }, attrs: { label: "Confirmar Contraseña", "label-for": "password2" } }, [s("b-alert", { attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert5, callback: function(e) { t.showDismissibleAlert5 = e }, expression: "showDismissibleAlert5" } }, [t._v("\n                Las contraseñas no coinciden\n              ")]), s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputPass2 + ")" }, attrs: { id: "password2", type: "password", required: "", placeholder: "Confirma la contraseña..." }, on: { click: t.reset, keydown: t.reset, keyup: function(e) { return !e.type.indexOf("key") && t._k(e.keyCode, "enter", 13, e.key, "Enter") ? null : t.handleSubmit(e) } }, model: { value: t.password2, callback: function(e) { t.password2 = e }, expression: "password2" } })], 1), s("center", [s("br"), s("b-button", { staticClass: "button-login mt-1", style: { background: t.buttonActive().back, "border-color": t.buttonActive().back, cursor: t.buttonActive().cursor, color: t.buttonActive().color }, attrs: { type: "button", disabled: !!t.myInput }, on: { click: t.handleSubmit } }, [t._v("\n                Ingresar\n              ")])], 1)], 1)], 1)])])]), t._m(1)])
            },
            G = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "text-center" }, [a("strong", [a("h1", { staticClass: "inicio mb-3" }, [t._v("Registro Agentes")]), a("span", [t._v("Ingresa los siguientes datos ")])])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }];
        a("b882");
        var Y = {
                data: function() { return { publicPath: "/bothuman/montechelo/", pathServer: "https://dashbots.outsourcingcos.com/widget/montechelo/tasks", localPathData: "data_agent_bot_montechelo", localPathJToken: "jwt_bothuman_montechelo", localPathState: "state_montechelos", user: "", password: "", name: "", password2: "", email: "", bot: "", capacity: 0, showDismissibleAlert: !1, showDismissibleAlert2: !1, showDismissibleAlert3: !1, showDismissibleAlert4: !1, showDismissibleAlert5: !1, showDismissibleAlert6: !1, showDismissibleAlert7: !1, imageInputName: a("7db1"), imageInputPass: "", imageInputNameUser: "", imageInputEmail: "", imageInputPass2: "", myInput: !0 } },
                beforeMount: function() {
                    var t = localStorage.getItem(this.localPathJToken);
                    this.$parent.socket.emit("Supervisor", t, { idSocketAgent: t, stateAgent: localStorage.getItem(this.localPathState), nameAgent: JSON.parse(localStorage.getItem(this.localPathData)).nameAgent })
                },
                methods: {
                    handleSubmit: function(t) {
                        var e = this;
                        t.preventDefault(), this.password.length >= 8 && !1 === isNaN(this.user) && !0 === this.validateEmail(this.email) && !1 === isNaN(this.capacity) && this.password === this.password2 ? c.a.post(this.pathServer + "/saveAgent", { tokenUser: localStorage.getItem(this.localPathJToken), cedula: this.user, name: this.name, email: this.email, password: this.password, capacity: this.capacity }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(t) { "sucess" === t.data ? (e.showDismissibleAlert6 = !0, e.user = "", e.name = "", e.email = "", e.password = "", e.password2 = "", e.capacity = "") : "Error" === t.data && (e.showDismissibleAlert = !0) })).catch((function(t) { console.log(t.response) })) : this.password.length < 8 ? (this.imageInputPass = a("ca21"), this.showDismissibleAlert3 = !0) : !0 === isNaN(this.user) ? (this.imageInputName = a("ca21"), this.showDismissibleAlert2 = !0) : !0 === isNaN(this.capacity) ? (this.imageInputName = a("ca21"), this.showDismissibleAlert7 = !0) : !1 === this.validateEmail(this.email) ? (this.imageInputEmail = a("ca21"), this.showDismissibleAlert4 = !0) : this.password !== this.password2 && (this.imageInputPass2 = a("ca21"), this.showDismissibleAlert5 = !0)
                    },
                    validateEmail: function(t) { var e = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; return e.test(t) },
                    reset: function() { this.showDismissibleAlert = !1, this.showDismissibleAlert2 = !1, this.imageInputName = a("7db1"), this.imageInputPass = "", this.imageInputEmail = "", this.imageInputPass2 = "", this.imageInputNameUser = " ", this.showDismissibleAlert3 = !1, this.showDismissibleAlert4 = !1, this.showDismissibleAlert5 = !1, this.showDismissibleAlert6 = !1, this.showDismissibleAlert7 = !1 },
                    buttonActive: function() { return this.password.length <= 0 || this.password2.length <= 0 || this.user.length <= 0 || this.email.length <= 0 || this.name.length <= 0 ? (this.myInput = !0, { back: "#D8D8D8 !important", cursor: "not-allowed !important", color: "#8F8F8F !important" }) : (this.myInput = !1, { back: "#008A46 !important", cursor: "pointer !important", color: "#fff !important" }) }
                }
            },
            K = Y,
            Q = (a("963a"), Object(m["a"])(K, X, G, !1, null, "449c5c04", null)),
            tt = Q.exports,
            et = { name: "user", components: { RegisterCom: tt }, data: function() { return { socket: this.$parent.socket } }, methods: { marginCom: function() { return "50px" == this.$parent.mgleftNav ? "0px" : "150px" } } },
            at = et,
            st = Object(m["a"])(at, L, Z, !1, null, "2ce07794", null),
            it = st.exports,
            nt = function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticStyle: { "margin-top": "80px", transition: "all 700ms" }, style: { "margin-left": t.marginCom() } }, [a("UpdateCom"), t._m(0)], 1)
            },
            ot = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }],
            rt = function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("div", { staticClass: "main w-50 mx-auto" }, [s("div", { staticClass: "row" }, [s("div", { staticClass: "col", staticStyle: { background: "#FAFAFA", "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)", padding: "34px", margin: "40px", "margin-left": "90px", "margin-bottom": "43px" } }, [t._m(0), s("div", { staticClass: "container mt-5", staticStyle: { "max-width": "450px" } }, [s("div", {}, [s("b-form", [s("b-alert", { staticClass: "text-center", attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert, callback: function(e) { t.showDismissibleAlert = e }, expression: "showDismissibleAlert" } }, [t._v("\n              No existe un usuario con esa cedula o no tienes permiso para\n              actualizar\n              "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), s("b-alert", { attrs: { variant: "success" }, model: { value: t.showDismissibleAlert6, callback: function(e) { t.showDismissibleAlert6 = e }, expression: "showDismissibleAlert6" } }, [t._v("\n              Se ha registrado satisfactoriamente el agente\n            ")]), s("div", { staticClass: "row" }, [s("b-form-group", { staticClass: "s-label col-8", staticStyle: { "font-weight": "bold" }, attrs: { label: "Cedula del agente", "label-for": "user" } }, [s("b-alert", { attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert2, callback: function(e) { t.showDismissibleAlert2 = e }, expression: "showDismissibleAlert2" } }, [t._v("\n                  Escribe una Cedula de Usuario Correcta\n                ")]), s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputName + ")" }, attrs: { id: "user", type: "text", required: "", autofocus: "", placeholder: "Escribe la cedula usuario..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.user, callback: function(e) { t.user = e }, expression: "user" } })], 1), s("b-button", { staticClass: "button-login align-self-center col-4 mt-2", style: { background: t.buttonActiveSearch().back, "border-color": t.buttonActiveSearch().back, cursor: t.buttonActiveSearch().cursor, color: t.buttonActiveSearch().color }, attrs: { type: "submit", disabled: !!t.myInputSearch }, on: { click: t.searchAgent } }, [t._v("Buscar\n              ")])], 1), s("b-form-group", { staticClass: "s-label mt-0 ", staticStyle: { "font-weight": "bold" }, attrs: { label: "Nombre del agente", "label-for": "name" } }, [s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputNameUser + ")" }, attrs: { id: "name", type: "text", required: "", placeholder: "Escribe un nombre..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.name, callback: function(e) { t.name = e }, expression: "name" } })], 1), s("b-form-group", { staticClass: "s-label mt-3 ", staticStyle: { "font-weight": "bold" }, attrs: { label: "Correo eléctronico", "label-for": "email" } }, [s("b-alert", { attrs: { variant: "danger", dismissible: "" }, model: { value: t.showDismissibleAlert4, callback: function(e) { t.showDismissibleAlert4 = e }, expression: "showDismissibleAlert4" } }, [t._v("\n                Escribe un Email Correcto\n              ")]), s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputEmail + ")" }, attrs: { id: "email", type: "email", required: "", placeholder: "Escribe el correo..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.email, callback: function(e) { t.email = e }, expression: "email" } })], 1), s("b-form-group", { staticClass: "s-label mt-3 ", staticStyle: { "font-weight": "bold" }, attrs: { label: "Contraseña", "label-for": "password" } }, [s("b-alert", { attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert3, callback: function(e) { t.showDismissibleAlert3 = e }, expression: "showDismissibleAlert3" } }, [t._v("\n                La contraseña es muy corta\n              ")]), s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputPass + ")" }, attrs: { id: "password", type: "password", required: "", placeholder: "Escribe una contraseña..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.password, callback: function(e) { t.password = e }, expression: "password" } })], 1), s("b-form-group", { staticClass: "s-label mt-3 ", staticStyle: { "font-weight": "bold" }, attrs: { label: "Confirmar Contraseña", "label-for": "password2" } }, [s("b-alert", { attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert5, callback: function(e) { t.showDismissibleAlert5 = e }, expression: "showDismissibleAlert5" } }, [t._v("\n                Las contraseñas no coinciden\n              ")]), s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputPass2 + ")" }, attrs: { id: "password2", type: "password", required: "", placeholder: "Confirma la contraseña..." }, on: { click: t.reset, keydown: t.reset, keyup: function(e) { return !e.type.indexOf("key") && t._k(e.keyCode, "enter", 13, e.key, "Enter") ? null : t.handleSubmit(e) } }, model: { value: t.password2, callback: function(e) { t.password2 = e }, expression: "password2" } })], 1), s("b-form-group", { staticClass: "s-label m-0  text-center", staticStyle: { "font-weight": "bold" }, attrs: { label: "Estado del Agente", "label-for": "status" } }, [s("b-form-checkbox", { attrs: { id: "checkbox-1", name: "checkbox-1", value: !0, "unchecked-value": !1, switch: "" }, model: { value: t.status, callback: function(e) { t.status = e }, expression: "status" } }, [t._v("\n                " + t._s(t.activeAgent()) + "\n              ")])], 1), s("center", [s("br"), s("b-button", { staticClass: "button-login m-0", style: { background: t.buttonActive().back, "border-color": t.buttonActive().back, cursor: t.buttonActive().cursor, color: t.buttonActive().color }, attrs: { type: "button", disabled: !!t.myInput }, on: { click: t.handleSubmit } }, [t._v("\n                Ingresar\n              ")])], 1)], 1)], 1)])])]), t._m(1)])
            },
            lt = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "text-center" }, [a("strong", [a("h1", { staticClass: "inicio mb-3" }, [t._v("Actualización Agentes")]), a("span", [t._v("Ingresa los siguientes datos ")])])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }];
        a("b882");
        var ct = {
                data: function() { return { publicPath: "/bothuman/montechelo/", pathServer: "https://dashbots.outsourcingcos.com/widget/montechelo/tasks", localPathData: "data_agent_bot_montechelo", localPathJToken: "jwt_bothuman_montechelo", localPathState: "state_montechelos", user: "", password: "", name: "", password2: "", email: "", bot: "", showDismissibleAlert: !1, showDismissibleAlert2: !1, showDismissibleAlert3: !1, showDismissibleAlert4: !1, showDismissibleAlert5: !1, showDismissibleAlert6: !1, imageInputName: a("7db1"), imageInputPass: "", imageInputNameUser: "", imageInputEmail: "", imageInputPass2: "", myInput: !0, myInputSearch: !0, status: !1 } },
                beforeMount: function() {
                    var t = localStorage.getItem(this.localPathJToken);
                    this.$parent.socket.emit("Supervisor", t, { idSocketAgent: t, stateAgent: localStorage.getItem(this.localPathState), nameAgent: JSON.parse(localStorage.getItem(this.localPathData)).nameAgent })
                },
                methods: {
                    activeAgent: function() {
                        switch (this.status) {
                            case !0:
                                return "Activo";
                            case !1:
                                return "Inactivo"
                        }
                    },
                    handleSubmit: function(t) {
                        var e = this;
                        t.preventDefault(), this.password.length >= 8 && !0 === this.validateEmail(this.email) && this.password === this.password2 ? c.a.post(this.pathServer + "/updateAgent", { tokenUser: localStorage.getItem(this.localPathJToken), cedula: this.user, name: this.name, email: this.email, password: this.password, active: this.status }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(t) { "sucess" === t.data ? (e.showDismissibleAlert6 = !0, e.user = "", e.name = "", e.email = "", e.password = "", e.password2 = "", e.status = !1) : "No existe" === t.data && (e.showDismissibleAlert = !0) })).catch((function(t) { console.log(t.response) })) : this.password.length < 8 ? (this.imageInputPass = a("ca21"), this.showDismissibleAlert3 = !0) : !1 === this.validateEmail(this.email) ? (this.imageInputEmail = a("ca21"), this.showDismissibleAlert4 = !0) : this.password !== this.password2 && (this.imageInputPass2 = a("ca21"), this.showDismissibleAlert5 = !0)
                    },
                    validateEmail: function(t) { var e = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; return e.test(t) },
                    searchAgent: function(t) {
                        var e = this;
                        t.preventDefault(), !1 === isNaN(this.user) && this.user > 5 ? c.a.post(this.pathServer + "/searchAgent", { tokenUser: localStorage.getItem(this.localPathJToken), cedula: this.user }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(t) { t.data.name ? (e.name = t.data.name, e.email = t.data.email, e.status = t.data.active) : (e.name = "", e.email = "", e.showDismissibleAlert = !0, e.status = !1) })) : (this.imageInputName = a("ca21"), this.showDismissibleAlert2 = !0)
                    },
                    reset: function() { this.showDismissibleAlert = !1, this.showDismissibleAlert2 = !1, this.imageInputName = a("7db1"), this.imageInputPass = "", this.imageInputEmail = "", this.imageInputPass2 = "", this.imageInputNameUser = " ", this.showDismissibleAlert3 = !1, this.showDismissibleAlert4 = !1, this.showDismissibleAlert5 = !1, this.showDismissibleAlert6 = !1 },
                    buttonActive: function() { return this.password.length <= 0 || this.password2.length <= 0 || this.email.length <= 0 || this.name.length <= 0 ? (this.myInput = !0, { back: "#D8D8D8 !important", cursor: "not-allowed !important", color: "#8F8F8F !important" }) : (this.myInput = !1, { back: "#008A46 !important", cursor: "pointer !important", color: "#fff !important" }) },
                    buttonActiveSearch: function() { return this.user.length <= 4 ? (this.myInputSearch = !0, { back: "#D8D8D8 !important", cursor: "not-allowed !important", color: "#8F8F8F !important" }) : (this.myInputSearch = !1, { back: "#008A46 !important", cursor: "pointer !important", color: "#fff !important" }) }
                }
            },
            dt = ct,
            ht = (a("d675"), Object(m["a"])(dt, rt, lt, !1, null, "3217f15c", null)),
            ut = ht.exports,
            mt = { name: "userUpdate", components: { UpdateCom: ut }, data: function() { return { socket: this.$parent.socket } }, methods: { marginCom: function() { return "50px" == this.$parent.mgleftNav ? "0px" : "150px" } } },
            pt = mt,
            gt = Object(m["a"])(pt, nt, ot, !1, null, "7c481216", null),
            ft = gt.exports,
            bt = function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticStyle: { "margin-top": "80px", transition: "all 700ms" }, style: { "margin-left": t.marginCom() } }, [a("OfficeHours"), t._m(0)], 1)
            },
            vt = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }],
            wt = function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("div", { staticClass: "main w-75 mx-auto" }, [s("div", { staticClass: "row justify-content-center" }, [s("div", { staticClass: "col", staticStyle: { background: "#FAFAFA", "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)", padding: "34px", margin: "40px", "margin-bottom": "43px" } }, [t._m(0), s("b-alert", { attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert, callback: function(e) { t.showDismissibleAlert = e }, expression: "showDismissibleAlert" } }, [t._v("\n        la hora de fin de atención no puede ser menor que la de inicio\n        "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), t._m(1), s("div", { staticClass: "row mt-1" }, [s("div", { staticClass: "col text-center " }, [t._v("\n          Días habiles"), s("br"), t._v("\n          " + t._s(t.habilDay) + "\n        ")]), s("div", { staticClass: "col text-center " }, [t._v("\n          Días No habiles"), s("br"), t._v("\n          " + t._s(t.noHabilday) + "\n        ")])]), t._m(2), s("div", { staticClass: "row mt-2" }, [s("div", { staticClass: "col text-center" }, [t._v("\n          Días habiles"), s("br"), t._v("\n          " + t._s(t.selecTimeStart) + " - " + t._s(t.selecTimeEnd) + "\n          "), s("div", { staticClass: "mt-2" }, [s("VueCtkDateTimePicker", { attrs: { formatted: "HH:mm", format: "HH:mm a", onlyTime: !0, right: !0, label: t.labelTimeStart, noClearButton: !0 }, model: { value: t.selecTimeStart, callback: function(e) { t.selecTimeStart = e }, expression: "selecTimeStart" } })], 1), s("div", { staticClass: "mt-2" }, [s("VueCtkDateTimePicker", { attrs: { formatted: "HH:mm", format: "HH:mm a", onlyTime: !0, right: !0, label: t.labelTimeEnd, noClearButton: !0 }, model: { value: t.selecTimeEnd, callback: function(e) { t.selecTimeEnd = e }, expression: "selecTimeEnd" } })], 1)]), s("div", { staticClass: "col text-center" }, [t._v("\n          Días No habiles"), s("br"), t._v("\n          " + t._s(t.selecTimeStartNo) + " - " + t._s(t.selecTimeEndNo) + "\n          "), s("div", { staticClass: "mt-2" }, [s("VueCtkDateTimePicker", { attrs: { formatted: "HH:mm", format: "HH:mm a", onlyTime: !0, right: !0, label: t.labelTimeStart, noClearButton: !0 }, model: { value: t.selecTimeStartNo, callback: function(e) { t.selecTimeStartNo = e }, expression: "selecTimeStartNo" } })], 1), s("div", { staticClass: "mt-2" }, [s("VueCtkDateTimePicker", { attrs: { formatted: "HH:mm", format: "HH:mm a", onlyTime: !0, right: !0, label: t.labelTimeEnd, noClearButton: !0 }, model: { value: t.selecTimeEndNo, callback: function(e) { t.selecTimeEndNo = e }, expression: "selecTimeEndNo" } })], 1)])]), s("div", { staticClass: "row" }, [s("div", { staticClass: "col justify-content-center text-center" }, [s("b-button", { staticClass: "button-login mt-3", style: { background: t.buttonActive().back, "border-color": t.buttonActive().back, cursor: t.buttonActive().cursor, color: t.buttonActive().color }, attrs: { type: "button", disabled: !!t.myInput }, on: { click: t.check } }, [t._v("Cambiar")])], 1)])], 1)]), t._m(3)])
            },
            xt = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "text-center" }, [a("strong", [a("h1", { staticClass: "inicio mb-3" }, [t._v("Horarios de atención")])])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row" }, [a("div", { staticClass: "col text-center mt-3 font-weight-bold" }, [t._v("\n          Actual\n        ")])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row mt-3" }, [a("div", { staticClass: "col text-center font-weight-bold" }, [t._v("\n          Modificar\n        ")])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }],
            kt = a("e30a"),
            yt = a.n(kt);
        a("b40d");
        a("60ab");
        var Ct = {
                components: { VueCtkDateTimePicker: yt.a },
                data: function() { return { publicPath: "/bothuman/montechelo/", pathServer: "https://dashbots.outsourcingcos.com/widget/montechelo/tasks", localPathData: "data_agent_bot_montechelo", localPathJToken: "jwt_bothuman_montechelo", localPathState: "state_montechelos", noHabilday: "", habilDay: "", selecTimeStart: "", selecTimeEnd: "", selecTimeStartNo: "", selecTimeEndNo: "", boxTwo: "", formaTime: "hh:mm a", labelTimeStart: "Inicio atención", labelTimeEnd: "Fin atención", user: "", password: "", name: "", password2: "", email: "", bot: "", showDismissibleAlert: !1, imageInputName: a("7db1"), imageInputPass: "", imageInputNameUser: "", imageInputEmail: "", imageInputPass2: "", myInput: !0 } },
                beforeMount: function() {
                    var t = this,
                        e = localStorage.getItem(this.localPathJToken);
                    this.$parent.socket.emit("Supervisor", e, { idSocketAgent: e, stateAgent: localStorage.getItem(this.localPathState), nameAgent: JSON.parse(localStorage.getItem(this.localPathData)).nameAgent }), c.a.post(this.pathServer + "/checkOfficeHours", { tokenUser: localStorage.getItem(this.localPathJToken) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(e) {
                        var a = t.convertToHour(e.data.dias_habiles_inicio),
                            s = t.convertToHour(e.data.dias_habiles_fin),
                            i = t.convertToHour(e.data.dias_no_habiles_inicio),
                            n = t.convertToHour(e.data.dias_no_habiles_fin);
                        t.selecTimeStart = a, t.selecTimeEnd = s, t.selecTimeStartNo = i, t.selecTimeEndNo = n, t.habilDay = a + "-" + s, t.noHabilday = i + "-" + n
                    }))
                },
                methods: {
                    check: function() {
                        var t = this;
                        this.showDismissibleAlert = !1;
                        var e = this.convertToInt(this.selecTimeStart),
                            a = this.convertToInt(this.selecTimeEnd),
                            s = this.convertToInt(this.selecTimeStartNo),
                            i = this.convertToInt(this.selecTimeEndNo);
                        a > e && i > s ? (this.boxTwo = "", this.$bvModal.msgBoxConfirm("¿Estas seguro de modificar el horario de atención?", { title: "Please Confirm", size: "sm", buttonSize: "sm", okVariant: "secondary", okTitle: "SI", cancelVariant: "info", cancelTitle: "NO", footerBgVariant: "white", footerClass: "p-2", hideHeader: !0, centered: !0 }).then((function(n) { t.boxTwo = n, 1 == n && c.a.post(t.pathServer + "/updateOfficeHours", { tokenUser: localStorage.getItem(t.localPathJToken), dhstart: e, dhend: a, dnhstart: s, dnhend: i }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(t.localPathData)).jsonWebToken } }).then((function(e) { 200 == e.status && (t.$parent.socket.emit("new_office_hour", 1), t.habilDay = t.selecTimeStart + "-" + t.selecTimeEnd, t.noHabilday = t.selecTimeStartNo + "-" + t.selecTimeEndNo) })) })).catch((function(t) { console.log(t) }))) : this.showDismissibleAlert = !0
                    },
                    convertToInt: function(t) {
                        try {
                            var e = t.split(":"),
                                a = parseInt(e[0]),
                                s = parseInt(e[1]),
                                i = 0,
                                n = 3600 * a + 60 * s + i;
                            return n
                        } catch (o) { logger.error("Error, no existe array para hacerle split") }
                    },
                    convertToHour: function(t) {
                        try {
                            var e = Math.floor(t / 3600),
                                a = Math.floor(t % 3600 / 60),
                                s = "";
                            return e < 10 && (e = "0" + e), a < 10 && (a = "0" + a), s = e > 12 || 12 == e ? "pm" : "am", e + ":" + a + " " + s
                        } catch (i) { console.log(i) }
                    },
                    buttonActive: function() { return this.selecTimeStart.length <= 0 || this.selecTimeEnd.length <= 0 || this.selecTimeStartNo.length <= 0 || this.selecTimeEndNo.length <= 0 ? (this.myInput = !0, { back: "#D8D8D8 !important", cursor: "not-allowed !important", color: "#8F8F8F !important" }) : (this.myInput = !1, { back: "#008A46 !important", cursor: "pointer !important", color: "#fff !important" }) }
                }
            },
            _t = Ct,
            At = (a("75ac"), Object(m["a"])(_t, wt, xt, !1, null, "0086f424", null)),
            St = At.exports,
            Tt = { name: "settings", components: { OfficeHours: St }, data: function() { return { socket: this.$parent.socket } }, methods: { marginCom: function() { return "50px" == this.$parent.mgleftNav ? "0px" : "150px" } } },
            Dt = Tt,
            It = Object(m["a"])(Dt, bt, vt, !1, null, "5157d536", null),
            jt = It.exports,
            Pt = function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticStyle: { "margin-top": "80px", transition: "all 700ms" }, style: { "margin-left": t.marginCom() } }, [a("CapacityAgent"), t._m(0)], 1)
            },
            Et = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }],
            Nt = function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("div", { staticClass: "main w-75 mx-auto" }, [s("div", { staticClass: "row justify-content-center" }, [s("div", { staticClass: "col", staticStyle: { background: "#FAFAFA", "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)", padding: "34px", margin: "40px", "margin-bottom": "43px" } }, [t._m(0), t._m(1), s("b-alert", { attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert, callback: function(e) { t.showDismissibleAlert = e }, expression: "showDismissibleAlert" } }, [t._v("\n        Ingresa solo números\n        "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), t._m(2), t._l(t.agents, (function(e, a) { return s("div", { key: e.idAgent, staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-around flex-nowrap tableAgents" }, [s("div", { staticClass: "col text-center align-self-center colTable" }, [s("span", [t._v(t._s(e.id))])]), s("div", { staticClass: "col text-center align-self-center colTable" }, [s("span", [t._v(t._s(e.nameagent))])]), s("div", { staticClass: "col text-center align-self-center" }, [s("b-input", { staticClass: "text-center", attrs: { value: e.capacidad }, model: { value: e.capacidad, callback: function(a) { t.$set(e, "capacidad", a) }, expression: "item.capacidad" } })], 1), s("div", { staticClass: "col text-center align-self-center" }, [s("b-button", { on: { click: function(e) { return t.change(a) } } }, [t._v("Modificar")])], 1)]) }))], 2)]), t._m(3)])
            },
            Ot = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "text-center" }, [a("strong", [a("h1", { staticClass: "inicio mb-3" }, [t._v("Capacidad Conversaciones")])])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-center mt-5" }, [a("span", { staticClass: "font-weight-bold", staticStyle: { color: "#002A3A", "font-size": "25px" } }, [t._v("\n          Agentes activos\n        ")])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row w-75 mx-auto rounded-lg justify-content-around mt-3 font-weight-bold flex-nowrap tableAgent" }, [a("div", { staticClass: "col text-center align-self-center colTable" }, [t._v("\n          Identificacion del Agente\n        ")]), a("div", { staticClass: "col text-center align-self-center colTable" }, [t._v("\n          Nombre del Agente\n        ")]), a("div", { staticClass: "col text-center align-self-center" }, [t._v("\n          Capacidad\n        ")]), a("div", { staticClass: "col text-center align-self-center" })])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }];
        a("60ab");
        var Bt = {
                components: {},
                data: function() { return { publicPath: "/bothuman/montechelo/", pathServer: "https://dashbots.outsourcingcos.com/widget/montechelo/tasks", localPathData: "data_agent_bot_montechelo", localPathJToken: "jwt_bothuman_montechelo", localPathState: "state_montechelos", agents: [], showDismissibleAlert: !1 } },
                beforeMount: function() {
                    var t = this,
                        e = localStorage.getItem(this.localPathJToken);
                    this.$parent.socket.emit("Supervisor", e, { idSocketAgent: e, stateAgent: localStorage.getItem(this.localPathState), nameAgent: JSON.parse(localStorage.getItem(this.localPathData)).nameAgent }), c.a.post(this.pathServer + "/callAgents", { tokenUser: localStorage.getItem(this.localPathJToken) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(e) { t.agents = e.data }))
                },
                methods: {
                    change: function(t) {
                        var e = this;
                        !1 === isNaN(this.agents[t].capacidad) ? (this.boxTwo = "", this.$bvModal.msgBoxConfirm("¿Estas seguro de modificar la capacidad del Agente?", { title: "Please Confirm", size: "sm", buttonSize: "sm", okVariant: "secondary", okTitle: "SI", cancelVariant: "info", cancelTitle: "NO", footerBgVariant: "white", footerClass: "p-2", hideHeader: !0, centered: !0 }).then((function(a) {
                            e.boxTwo = a, 1 == a && c.a.post(e.pathServer + "/updateCapAgents", { tokenUser: localStorage.getItem(e.localPathJToken), capacity: e.agents[t].capacidad, idAgent: e.agents[t].id }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(e.localPathData)).jsonWebToken } }).then((function(a) {
                                if (200 == a.status) {
                                    var s = parseInt(e.agents[t].capacidad);
                                    e.$parent.socket.emit("new_capacity", { agent: e.agents[t].token, newCap: s })
                                }
                            }))
                        })).catch((function(t) { console.log(t) }))) : this.showDismissibleAlert = !0
                    }
                }
            },
            zt = Bt,
            $t = (a("d82d"), Object(m["a"])(zt, Nt, Ot, !1, null, "25d624a0", null)),
            Mt = $t.exports,
            Ut = { name: "settingsCap", components: { CapacityAgent: Mt }, data: function() { return { socket: this.$parent.socket } }, methods: { marginCom: function() { return "50px" == this.$parent.mgleftNav ? "0px" : "150px" } } },
            Jt = Ut,
            Ht = Object(m["a"])(Jt, Pt, Et, !1, null, "74259548", null),
            Ft = Ht.exports,
            Wt = function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticStyle: { "margin-top": "80px", transition: "all 700ms" }, style: { "margin-left": t.marginCom() } }, [a("QrFileCom"), t._m(0)], 1)
            },
            Rt = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }],
            qt = function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("div", { staticClass: "main w-75 mx-auto" }, [s("div", { staticClass: "row justify-content-center" }, [s("div", { staticClass: "col", staticStyle: { background: "#FAFAFA", "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)", padding: "34px", margin: "40px", "margin-bottom": "43px" } }, [t._m(0), s("b-alert", { attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert, callback: function(e) { t.showDismissibleAlert = e }, expression: "showDismissibleAlert" } }, [t._v("\n        Tienes que volver a generar el codigo QR\n        "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), s("b-alert", { attrs: { variant: "success" }, model: { value: t.showDismissibleAlert2, callback: function(e) { t.showDismissibleAlert2 = e }, expression: "showDismissibleAlert2" } }, [t._v("\n        Whatsapp esta listo para usarse\n      ")]), s("div", { staticClass: "d-flex flex-column justify-content-center" }, [s("iframe", { staticClass: "align-self-center", staticStyle: { height: "299px", border: "0" }, style: { display: t.algo }, attrs: { id: "qrframe", src: "https://dashbots.outsourcingcos.com/widget/montechelo/qrcode.html" } }), s("b-button", { staticClass: "align-self-center", staticStyle: { "max-width": "250px" }, on: { click: function(e) { return t.generate() } } }, [t._v("Generar QR Whatsapp")])], 1)], 1)]), t._m(1)])
            },
            Vt = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "text-center" }, [a("strong", [a("h1", { staticClass: "inicio mb-3" }, [t._v("Escaneo De Codigo QR Whatsapp")])])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }];
        a("60ab");
        var Lt = {
                components: {},
                data: function() { return { publicPath: "/bothuman/montechelo/", pathServer: "https://dashbots.outsourcingcos.com/widget/montechelo/tasks", localPathData: "data_agent_bot_montechelo", localPathJToken: "jwt_bothuman_montechelo", localPathState: "state_montechelos", showDismissibleAlert: !1, showDismissibleAlert2: !1, algo: "none" } },
                beforeMount: function() {},
                methods: {
                    generate: function() {
                        var t = this;
                        this.showDismissibleAlert = !1, this.showDismissibleAlert2 = !1, this.$bvModal.msgBoxConfirm("¿Estas seguro de Generar otro código?", { title: "Please Confirm", size: "sm", buttonSize: "sm", okVariant: "secondary", okTitle: "SI", cancelVariant: "info", cancelTitle: "NO", footerBgVariant: "white", footerClass: "p-2", hideHeader: !0, centered: !0 }).then((function(e) {
                            if (t.boxTwo = e, 1 == e) {
                                t.$parent.socket.emit("new_qr", 1);
                                var a = t;
                                setTimeout((function() {
                                    var t = document.getElementById("qrframe");
                                    t.src = t.src, a.algo = ""
                                }), 4e3)
                            }
                        })).catch((function(t) { console.log(t) }))
                    }
                },
                mounted: function() {
                    var t = this;
                    this.$parent.socket.on("err_qr", (function(e) { 1 === e ? (t.algo = "none", t.showDismissibleAlert = !0) : 2 === e && (t.algo = "none", t.showDismissibleAlert2 = !0) }))
                }
            },
            Zt = Lt,
            Xt = (a("0ede"), Object(m["a"])(Zt, qt, Vt, !1, null, "01b04ea2", null)),
            Gt = Xt.exports,
            Yt = { name: "qrfile", components: { QrFileCom: Gt }, data: function() { return { socket: this.$parent.socket } }, methods: { marginCom: function() { return "50px" == this.$parent.mgleftNav ? "0px" : "150px" } } },
            Kt = Yt,
            Qt = Object(m["a"])(Kt, Wt, Rt, !1, null, "fe9ee094", null),
            te = Qt.exports,
            ee = function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticStyle: { "margin-top": "80px", transition: "all 700ms" }, style: { "margin-left": t.marginCom() } }, [a("TipCom"), t._m(0)], 1)
            },
            ae = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }],
            se = function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("div", { staticClass: "main w-75 mx-auto" }, [s("div", { staticClass: "row justify-content-center" }, [s("div", { staticClass: "col", staticStyle: { background: "#FAFAFA", "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)", padding: "34px", margin: "40px", "margin-bottom": "43px" } }, [t._m(0), t._m(1), s("b-alert", { staticClass: "text-center", attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert2, callback: function(e) { t.showDismissibleAlert2 = e }, expression: "showDismissibleAlert2" } }, [t._v("\n        Error modificando la tipificación, no puede haber dos códigos iguales\n        "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), s("b-alert", { staticClass: "text-center", attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert5, callback: function(e) { t.showDismissibleAlert5 = e }, expression: "showDismissibleAlert5" } }, [t._v("\n        Error eliminando la tipificación\n        "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), s("b-alert", { staticClass: "text-center", attrs: { variant: "success" }, model: { value: t.showDismissibleAlert3, callback: function(e) { t.showDismissibleAlert3 = e }, expression: "showDismissibleAlert3" } }, [t._v("\n        Se ha actualizado satisfactoriamente la tipificación\n      ")]), s("b-alert", { staticClass: "text-center", attrs: { variant: "success" }, model: { value: t.showDismissibleAlert4, callback: function(e) { t.showDismissibleAlert4 = e }, expression: "showDismissibleAlert4" } }, [t._v("\n        Se ha eliminado satisfactoriamente la tipificación\n      ")]), t._m(2), t._l(t.typify, (function(e, a) { return s("div", { key: e.idAgent, staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-around flex-nowrap tableAgents" }, [s("div", { staticClass: "col-3 text-center align-self-center colTable" }, [s("b-input", { staticClass: "text-center", attrs: { value: e.cod }, on: { click: t.reset, keydown: t.reset }, model: { value: e.cod, callback: function(a) { t.$set(e, "cod", a) }, expression: "item.cod" } })], 1), s("div", { staticClass: "col text-center align-self-center" }, [s("b-textarea", { staticClass: "text-center", staticStyle: { "margin-top": "5px", "margin-bottom": "5px" }, attrs: { value: e.value }, on: { click: t.reset, keydown: t.reset }, model: { value: e.value, callback: function(a) { t.$set(e, "value", a) }, expression: "item.value" } })], 1), s("div", { staticClass: "col-2 text-center align-self-center" }, [s("b-button", { on: { click: function(e) { return t.change(a) } } }, [t._v("Modificar")])], 1), s("div", { staticClass: "col-2 text-center align-self-center" }, [s("b-button", { on: { click: function(e) { return t.deleteTip(a) } } }, [t._v("Eliminar")])], 1)]) })), t._m(3), s("div", { staticClass: "row w-75 mx-auto rounded-lg justify-content-around mt-3 font-weight-bold flex-nowrap tableAgent" }, [s("div", { staticClass: "container mt-2", staticStyle: { "max-width": "450px" } }, [s("div", {}, [s("b-form", [s("b-alert", { staticClass: "text-center", attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert, callback: function(e) { t.showDismissibleAlert = e }, expression: "showDismissibleAlert" } }, [t._v("\n                Error creando la tipificación, no se puede repetir el código\n                "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), s("b-alert", { staticClass: "text-center", attrs: { variant: "success" }, model: { value: t.showDismissibleAlert6, callback: function(e) { t.showDismissibleAlert6 = e }, expression: "showDismissibleAlert6" } }, [t._v("\n                Se ha registrado satisfactoriamente la tipificación\n              ")]), s("b-form-group", { staticClass: "s-label", staticStyle: { "font-weight": "bold" }, attrs: { label: "Código Tipificacion", "label-for": "codTip" } }, [s("b-form-input", { staticClass: "input-style", attrs: { id: "codTip", type: "text", required: "", autofocus: "", placeholder: "Escribe el código..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.codTip, callback: function(e) { t.codTip = e }, expression: "codTip" } })], 1), s("b-form-group", { staticClass: "s-label mt-3 ", staticStyle: { "font-weight": "bold" }, attrs: { label: "Valor Tipificacion", "label-for": "valueTip" } }, [s("b-form-textarea", { staticClass: "input-style", attrs: { id: "valueTip", type: "text", required: "", placeholder: "Escribe el valor..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.valueTip, callback: function(e) { t.valueTip = e }, expression: "valueTip" } })], 1), s("center", [s("br"), s("b-button", { staticClass: "button-login mt-1", style: { background: t.buttonActive().back, "border-color": t.buttonActive().back, cursor: t.buttonActive().cursor, color: t.buttonActive().color }, attrs: { type: "button", disabled: !!t.myInput }, on: { click: t.handleSubmit } }, [t._v("\n                  Ingresar\n                ")])], 1)], 1)], 1)])])], 2)]), t._m(4)])
            },
            ie = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "text-center" }, [a("strong", [a("h1", { staticClass: "inicio mb-3" }, [t._v("Modificaciones de Tipificaciones")])])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-center mt-5" }, [a("span", { staticClass: "font-weight-bold", staticStyle: { color: "#002A3A", "font-size": "25px" } }, [t._v("\n          Tipificaciones actuales\n        ")])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row w-75 mx-auto rounded-lg justify-content-around mt-3 font-weight-bold flex-nowrap tableAgent" }, [a("div", { staticClass: "col-3 text-center align-self-center colTable" }, [t._v("\n          Código\n        ")]), a("div", { staticClass: "col text-center align-self-center colTable" }, [t._v("\n          Valor\n        ")]), a("div", { staticClass: "col-2 text-center align-self-center" }), a("div", { staticClass: "col-2 text-center align-self-center" })])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-center mt-5" }, [a("span", { staticClass: "font-weight-bold", staticStyle: { color: "#002A3A", "font-size": "25px" } }, [t._v("\n          Crear nueva Tipificación\n        ")])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }];
        a("60ab");
        var ne = {
                components: {},
                data: function() { return { publicPath: "/bothuman/montechelo/", pathServer: "https://dashbots.outsourcingcos.com/widget/montechelo/tasks", localPathData: "data_agent_bot_montechelo", localPathJToken: "jwt_bothuman_montechelo", localPathState: "state_montechelos", showDismissibleAlert: !1, showDismissibleAlert2: !1, showDismissibleAlert3: !1, showDismissibleAlert4: !1, showDismissibleAlert5: !1, showDismissibleAlert6: !1, imageInputName: a("7db1"), imageInputPass: "", imageInputNameUser: "", imageInputEmail: "", imageInputPass2: "", typify: [], codTip: "", valueTip: "" } },
                beforeMount: function() {
                    var t = this;
                    c.a.post(this.pathServer + "/callTypify", { tokenUser: localStorage.getItem(this.localPathJToken) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(e) { t.typify = e.data }))
                },
                methods: {
                    buttonActive: function() { return this.codTip.length <= 0 || this.valueTip.length <= 0 ? (this.myInput = !0, { back: "#D8D8D8 !important", cursor: "not-allowed !important", color: "#8F8F8F !important" }) : (this.myInput = !1, { back: "#008A46 !important", cursor: "pointer !important", color: "#fff !important" }) },
                    reset: function() { this.showDismissibleAlert = !1, this.imageInputName = a("7db1"), this.imageInputPass = "", this.imageInputEmail = "", this.imageInputPass2 = "", this.imageInputNameUser = " ", this.showDismissibleAlert3 = !1, this.showDismissibleAlert4 = !1, this.showDismissibleAlert5 = !1, this.showDismissibleAlert6 = !1, this.showDismissibleAlert7 = !1 },
                    handleSubmit: function(t) {
                        var e = this;
                        t.preventDefault(), this.codTip && this.valueTip && c.a.post(this.pathServer + "/saveTypify", { tokenUser: localStorage.getItem(this.localPathJToken), codTip: this.codTip, valueTip: this.valueTip }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(t) { t.data.status && "sucess" === t.data.status ? (e.showDismissibleAlert6 = !0, e.typify.push({ id: t.data.id, cod: e.codTip, value: e.valueTip }), e.codTip = "", e.valueTip = "") : "Error" === t.data && (e.showDismissibleAlert = !0) })).catch((function(t) { console.log(t.response) }))
                    },
                    change: function(t) {
                        var e = this;
                        this.typify[t].cod && this.typify[t].value ? (this.boxTwo = "", this.$bvModal.msgBoxConfirm("¿Estas seguro de modificar la tipificación?", { title: "Please Confirm", size: "sm", buttonSize: "sm", okVariant: "secondary", okTitle: "SI", cancelVariant: "info", cancelTitle: "NO", footerBgVariant: "white", footerClass: "p-2", hideHeader: !0, centered: !0 }).then((function(a) { e.boxTwo = a, 1 == a && c.a.post(e.pathServer + "/updateTypify", { tokenUser: localStorage.getItem(e.localPathJToken), codTip: e.typify[t].cod, valueTip: e.typify[t].value, idTip: e.typify[t].id }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(e.localPathData)).jsonWebToken } }).then((function(t) { 200 == t.status && ("sucess" === t.data ? e.showDismissibleAlert3 = !0 : "Error" === t.data && (e.showDismissibleAlert2 = !0)) })) })).catch((function(t) { console.log(t) }))) : this.showDismissibleAlert = !0
                    },
                    deleteTip: function(t) {
                        var e = this;
                        this.typify[t].cod && this.typify[t].value && (this.boxTwo = "", this.$bvModal.msgBoxConfirm("¿Estas seguro de ELIMINAR la tipificación?", { title: "Please Confirm", size: "sm", buttonSize: "sm", okVariant: "secondary", okTitle: "SI", cancelVariant: "info", cancelTitle: "NO", footerBgVariant: "white", footerClass: "p-2", hideHeader: !0, centered: !0 }).then((function(a) { e.boxTwo = a, 1 == a && c.a.post(e.pathServer + "/eraseTypify", { tokenUser: localStorage.getItem(e.localPathJToken), idTip: e.typify[t].id }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(e.localPathData)).jsonWebToken } }).then((function(a) { 200 == a.status && ("sucess" === a.data ? (e.showDismissibleAlert4 = !0, e.typify.splice(t, 1)) : "Error" === a.data && (e.showDismissibleAlert5 = !0)) })) })).catch((function(t) { console.log(t) })))
                    }
                },
                mounted: function() {}
            },
            oe = ne,
            re = (a("2f6c"), Object(m["a"])(oe, se, ie, !1, null, "52652315", null)),
            le = re.exports,
            ce = { name: "tipfi", components: { TipCom: le }, data: function() { return { socket: this.$parent.socket } }, methods: { marginCom: function() { return "50px" == this.$parent.mgleftNav ? "0px" : "150px" } } },
            de = ce,
            he = Object(m["a"])(de, ee, ae, !1, null, "6bbc3380", null),
            ue = he.exports,
            me = function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticStyle: { "margin-top": "80px", transition: "all 700ms" }, style: { "margin-left": t.marginCom() } }, [a("ShortcutCom"), t._m(0)], 1)
            },
            pe = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }],
            ge = function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("div", { staticClass: "main w-75 mx-auto" }, [s("div", { staticClass: "row justify-content-center" }, [s("div", { staticClass: "col", staticStyle: { background: "#FAFAFA", "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)", padding: "34px", margin: "40px", "margin-bottom": "43px" } }, [t._m(0), t._m(1), s("b-alert", { staticClass: "text-center", attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert2, callback: function(e) { t.showDismissibleAlert2 = e }, expression: "showDismissibleAlert2" } }, [t._v("\n        Error modificando el atajo, no puede haber dos códigos iguales\n        "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), s("b-alert", { staticClass: "text-center", attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert5, callback: function(e) { t.showDismissibleAlert5 = e }, expression: "showDismissibleAlert5" } }, [t._v("\n        Error eliminando el atajo\n        "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), s("b-alert", { staticClass: "text-center", attrs: { variant: "success" }, model: { value: t.showDismissibleAlert3, callback: function(e) { t.showDismissibleAlert3 = e }, expression: "showDismissibleAlert3" } }, [t._v("\n        Se ha actualizado satisfactoriamente el atajo\n      ")]), s("b-alert", { staticClass: "text-center", attrs: { variant: "success" }, model: { value: t.showDismissibleAlert4, callback: function(e) { t.showDismissibleAlert4 = e }, expression: "showDismissibleAlert4" } }, [t._v("\n        Se ha eliminado satisfactoriamente el atajo\n      ")]), t._m(2), t._l(t.typify, (function(e, a) { return s("div", { key: e.idAgent, staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-around flex-nowrap tableAgents" }, [s("div", { staticClass: "col-3 text-center align-self-center colTable" }, [s("b-input", { staticClass: "text-center", attrs: { value: e.cod }, on: { click: t.reset, keydown: t.reset }, model: { value: e.cod, callback: function(a) { t.$set(e, "cod", a) }, expression: "item.cod" } })], 1), s("div", { staticClass: "col text-center align-self-center" }, [s("b-textarea", { staticClass: "text-center", staticStyle: { "margin-top": "5px", "margin-bottom": "5px" }, attrs: { value: e.value }, on: { click: t.reset, keydown: t.reset }, model: { value: e.value, callback: function(a) { t.$set(e, "value", a) }, expression: "item.value" } })], 1), s("div", { staticClass: "col-2 text-center align-self-center" }, [s("b-button", { on: { click: function(e) { return t.change(a) } } }, [t._v("Modificar")])], 1), s("div", { staticClass: "col-2 text-center align-self-center" }, [s("b-button", { on: { click: function(e) { return t.deleteTip(a) } } }, [t._v("Eliminar")])], 1)]) })), t._m(3), s("div", { staticClass: "row w-75 mx-auto rounded-lg justify-content-around mt-3 font-weight-bold flex-nowrap tableAgent" }, [s("div", { staticClass: "container mt-2 mb-3", staticStyle: { "max-width": "450px" } }, [s("div", {}, [s("b-form", [s("b-alert", { staticClass: "text-center", attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert, callback: function(e) { t.showDismissibleAlert = e }, expression: "showDismissibleAlert" } }, [t._v("\n                Error creando el atajo, no se puede repetir el código\n                "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), s("b-alert", { staticClass: "text-center", attrs: { variant: "success" }, model: { value: t.showDismissibleAlert6, callback: function(e) { t.showDismissibleAlert6 = e }, expression: "showDismissibleAlert6" } }, [t._v("\n                Se ha registrado satisfactoriamente el atajo\n              ")]), s("b-form-group", { staticClass: "s-label", staticStyle: { "font-weight": "bold" }, attrs: { label: "Atajo", "label-for": "codTip" } }, [s("b-form-input", { staticClass: "input-style", attrs: { id: "codTip", type: "text", required: "", autofocus: "", placeholder: "Escribe el atajo..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.codTip, callback: function(e) { t.codTip = e }, expression: "codTip" } })], 1), s("b-form-group", { staticClass: "s-label mt-3 ", staticStyle: { "font-weight": "bold" }, attrs: { label: "Texto Atajo", "label-for": "valueTip" } }, [s("b-row", [s("b-col", { staticClass: "col-11" }, [s("b-form-textarea", { staticClass: "input-style", attrs: { id: "valueTip", type: "text", required: "", placeholder: "Escribe el texto..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.valueTip, callback: function(e) { t.valueTip = e }, expression: "valueTip" } })], 1), s("b-col", { staticClass: "p-0 d-flex  border-top border-right justify-content-center", attrs: { id: "divClick" } }, [s("img", { staticClass: "align-self-center", staticStyle: { width: "25px", height: "25px" }, attrs: { src: a("d99d") }, on: { click: t.openEmojiWin } }), s("picker", { style: { display: t.emojiDisplay, position: "absolute", bottom: "40px", right: "27px" }, attrs: { set: "emojione", data: t.emojiIndex, title: "Escoge tu emoji…", emoji: "point_up", i18n: { search: "Buscar", categories: { search: "Resultados", recent: "Recientes", people: "Sonrisas & Gente", nature: "Animales & Naturaleza", foods: "Comida & bebida", activity: "Actividades", places: "Viajes & Lugares", objects: "Objetos", symbols: "Simbolos", flags: "Banderas" } } }, on: { select: t.addEmoji } })], 1)], 1)], 1), s("center", [s("br"), s("b-button", { staticClass: "button-login mt-1", style: { background: t.buttonActive().back, "border-color": t.buttonActive().back, cursor: t.buttonActive().cursor, color: t.buttonActive().color }, attrs: { type: "button", disabled: !!t.myInput }, on: { click: t.handleSubmit } }, [t._v("\n                  Ingresar\n                ")])], 1)], 1)], 1)])])], 2)]), t._m(4)])
            },
            fe = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "text-center" }, [a("strong", [a("h1", { staticClass: "inicio mb-3" }, [t._v("Modificaciones de Atajos")])])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-center mt-5" }, [a("span", { staticClass: "font-weight-bold", staticStyle: { color: "#002A3A", "font-size": "25px" } }, [t._v("\n          Atajos actuales\n        ")])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row w-75 mx-auto rounded-lg justify-content-around mt-3 font-weight-bold flex-nowrap tableAgent" }, [a("div", { staticClass: "col-3 text-center align-self-center colTable" }, [t._v("\n          Atajo\n        ")]), a("div", { staticClass: "col text-center align-self-center colTable" }, [t._v("\n          Texto\n        ")]), a("div", { staticClass: "col-2 text-center align-self-center" }), a("div", { staticClass: "col-2 text-center align-self-center" })])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-center mt-5" }, [a("span", { staticClass: "font-weight-bold", staticStyle: { color: "#002A3A", "font-size": "25px" } }, [t._v("\n          Crear nuevo Atajo\n        ")])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }],
            be = new A["EmojiIndex"](_);
        a("60ab");
        var ve = {
                components: { Picker: A["Picker"] },
                data: function() { return { publicPath: "/bothuman/montechelo/", pathServer: "https://dashbots.outsourcingcos.com/widget/montechelo/tasks", localPathData: "data_agent_bot_montechelo", localPathJToken: "jwt_bothuman_montechelo", localPathState: "state_montechelos", showDismissibleAlert: !1, showDismissibleAlert2: !1, emojiIndex: be, showDismissibleAlert3: !1, showDismissibleAlert4: !1, showDismissibleAlert5: !1, showDismissibleAlert6: !1, imageInputName: a("7db1"), imageInputPass: "", imageInputNameUser: "", imageInputEmail: "", imageInputPass2: "", typify: [], codTip: "", valueTip: "", emojiDisplay: "none" } },
                beforeMount: function() {
                    var t = this;
                    c.a.post(this.pathServer + "/callShortcut", { tokenUser: localStorage.getItem(this.localPathJToken) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(e) { t.typify = e.data }))
                },
                methods: {
                    addEmoji: function(t) {
                        var e = t.native,
                            a = this.$refs.ta;
                        if (document.selection) a.focus(), this.valueTip = e, this.openEmojiWin();
                        else if (a.selectionStart || "0" == a.selectionStart) {
                            a.focus();
                            var s = a.selectionStart,
                                i = a.selectionEnd;
                            this.valueTip = a.value.substring(0, s) + e + a.value.substring(i, a.value.length), a.setSelectionRange(i + e.length, i + e.length), this.openEmojiWin()
                        } else this.valueTip += e, this.openEmojiWin()
                    },
                    openEmojiWin: function() { "none" === this.emojiDisplay ? this.emojiDisplay = "" : this.emojiDisplay = "none" },
                    buttonActive: function() { return this.codTip.length <= 0 || this.valueTip.length <= 0 ? (this.myInput = !0, { back: "#D8D8D8 !important", cursor: "not-allowed !important", color: "#8F8F8F !important" }) : (this.myInput = !1, { back: "#008A46 !important", cursor: "pointer !important", color: "#fff !important" }) },
                    reset: function() { this.showDismissibleAlert = !1, this.imageInputName = a("7db1"), this.imageInputPass = "", this.imageInputEmail = "", this.imageInputPass2 = "", this.imageInputNameUser = " ", this.showDismissibleAlert3 = !1, this.showDismissibleAlert4 = !1, this.showDismissibleAlert5 = !1, this.showDismissibleAlert6 = !1, this.showDismissibleAlert7 = !1 },
                    handleSubmit: function(t) {
                        var e = this;
                        t.preventDefault(), this.codTip && this.valueTip && c.a.post(this.pathServer + "/saveShortcut", { tokenUser: localStorage.getItem(this.localPathJToken), codTip: this.codTip, valueTip: this.valueTip }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(t) { t.data.status && "sucess" === t.data.status ? (e.showDismissibleAlert6 = !0, e.typify.push({ id: t.data.id, cod: e.codTip, value: e.valueTip }), e.codTip = "", e.valueTip = "") : "Error" === t.data && (e.showDismissibleAlert = !0) })).catch((function(t) { console.log(t.response) }))
                    },
                    change: function(t) {
                        var e = this;
                        this.typify[t].cod && this.typify[t].value ? (this.boxTwo = "", this.$bvModal.msgBoxConfirm("¿Estas seguro de modificar el atajo?", { title: "Please Confirm", size: "sm", buttonSize: "sm", okVariant: "secondary", okTitle: "SI", cancelVariant: "info", cancelTitle: "NO", footerBgVariant: "white", footerClass: "p-2", hideHeader: !0, centered: !0 }).then((function(a) { e.boxTwo = a, 1 == a && c.a.post(e.pathServer + "/updateShortcut", { tokenUser: localStorage.getItem(e.localPathJToken), codTip: e.typify[t].cod, valueTip: e.typify[t].value, idTip: e.typify[t].id }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(e.localPathData)).jsonWebToken } }).then((function(t) { 200 == t.status && ("sucess" === t.data ? e.showDismissibleAlert3 = !0 : "Error" === t.data && (e.showDismissibleAlert2 = !0)) })) })).catch((function(t) { console.log(t) }))) : this.showDismissibleAlert = !0
                    },
                    deleteTip: function(t) {
                        var e = this;
                        this.typify[t].cod && this.typify[t].value && (this.boxTwo = "", this.$bvModal.msgBoxConfirm("¿Estas seguro de ELIMINAR el atajo?", { title: "Please Confirm", size: "sm", buttonSize: "sm", okVariant: "secondary", okTitle: "SI", cancelVariant: "info", cancelTitle: "NO", footerBgVariant: "white", footerClass: "p-2", hideHeader: !0, centered: !0 }).then((function(a) { e.boxTwo = a, 1 == a && c.a.post(e.pathServer + "/eraseShortcut", { tokenUser: localStorage.getItem(e.localPathJToken), idTip: e.typify[t].id }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(e.localPathData)).jsonWebToken } }).then((function(a) { 200 == a.status && ("sucess" === a.data ? (e.showDismissibleAlert4 = !0, e.typify.splice(t, 1)) : "Error" === a.data && (e.showDismissibleAlert5 = !0)) })) })).catch((function(t) { console.log(t) })))
                    }
                },
                mounted: function() {}
            },
            we = ve,
            xe = (a("9ae5"), Object(m["a"])(we, ge, fe, !1, null, "0da61e74", null)),
            ke = xe.exports,
            ye = { name: "short", components: { ShortcutCom: ke }, data: function() { return { socket: this.$parent.socket } }, methods: { marginCom: function() { return "50px" == this.$parent.mgleftNav ? "0px" : "150px" } } },
            Ce = ye,
            _e = Object(m["a"])(Ce, me, pe, !1, null, "41cd18e4", null),
            Ae = _e.exports,
            Se = function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticStyle: { "margin-top": "80px", transition: "all 700ms" }, style: { "margin-left": t.marginCom() } }, [a("TmoCom"), t._m(0)], 1)
            },
            Te = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }],
            De = function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("div", { staticClass: "main w-75 mx-auto" }, [s("div", { staticClass: "row justify-content-center" }, [s("div", { staticClass: "col", staticStyle: { background: "#FAFAFA", "box-shadow": "0px -3px 10px rgba(0, 0, 0, 0.09)", padding: "34px", margin: "40px", "margin-bottom": "43px" } }, [t._m(0), t._m(1), s("b-alert", { staticClass: "text-center", attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert2, callback: function(e) { t.showDismissibleAlert2 = e }, expression: "showDismissibleAlert2" } }, [t._v("\n        Error modificando el tmo, no puede haber dos códigos iguales\n        "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), s("b-alert", { staticClass: "text-center", attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert5, callback: function(e) { t.showDismissibleAlert5 = e }, expression: "showDismissibleAlert5" } }, [t._v("\n        Error eliminando el tmo\n        "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), s("b-alert", { staticClass: "text-center", attrs: { variant: "success" }, model: { value: t.showDismissibleAlert3, callback: function(e) { t.showDismissibleAlert3 = e }, expression: "showDismissibleAlert3" } }, [t._v("\n        Se ha actualizado satisfactoriamente el tmo\n      ")]), s("b-alert", { staticClass: "text-center", attrs: { variant: "success" }, model: { value: t.showDismissibleAlert4, callback: function(e) { t.showDismissibleAlert4 = e }, expression: "showDismissibleAlert4" } }, [t._v("\n        Se ha eliminado satisfactoriamente el tmo\n      ")]), t._m(2), t._l(t.tmo, (function(e, a) { return s("div", { key: e.idAgent, staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-around flex-nowrap tableAgents" }, [s("div", { staticClass: "col text-center align-self-center colTable" }, [s("b-input", { staticClass: "text-center", attrs: { value: e.warning }, on: { click: t.reset, keydown: t.reset }, model: { value: e.warning, callback: function(a) { t.$set(e, "warning", a) }, expression: "item.warning" } })], 1), s("div", { staticClass: "col text-center align-self-center" }, [s("b-input", { staticClass: "text-center", attrs: { value: e.ending }, on: { click: t.reset, keydown: t.reset }, model: { value: e.ending, callback: function(a) { t.$set(e, "ending", a) }, expression: "item.ending" } })], 1), s("div", { staticClass: "col text-center align-self-center" }, [s("b-button", { on: { click: function(e) { return t.change(a) } } }, [t._v("Modificar")])], 1)]) }))], 2)]), t._m(3)])
            },
            Ie = [function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "text-center" }, [a("strong", [a("h1", { staticClass: "inicio mb-3" }, [t._v("\n            Modificaciones de Tiempo Medio de Operación\n          ")])])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row w-75 mx-auto rounded-lg text-dark justify-content-center mt-5" }, [a("span", { staticClass: "font-weight-bold", staticStyle: { color: "#002A3A", "font-size": "25px" } }, [t._v("\n          Configuración Actual\n        ")])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row w-75 mx-auto rounded-lg justify-content-around mt-3 font-weight-bold flex-nowrap tableAgent" }, [a("div", { staticClass: "col text-center align-self-center colTable" }, [t._v("\n          Tiempo de Aviso en Minutos\n        ")]), a("div", { staticClass: "col text-center align-self-center colTable" }, [t._v("\n          Tiempo de Finalización en Minutos\n        ")]), a("div", { staticClass: "col text-center align-self-center" })])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n      -\n      "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }];
        a("60ab");
        var je = {
                data: function() { return { publicPath: "/bothuman/montechelo/", pathServer: "https://dashbots.outsourcingcos.com/widget/montechelo/tasks", localPathData: "data_agent_bot_montechelo", localPathJToken: "jwt_bothuman_montechelo", localPathState: "state_montechelos", showDismissibleAlert: !1, showDismissibleAlert2: !1, showDismissibleAlert3: !1, showDismissibleAlert4: !1, showDismissibleAlert5: !1, showDismissibleAlert6: !1, imageInputName: a("7db1"), imageInputPass: "", imageInputNameUser: "", imageInputEmail: "", imageInputPass2: "", tmo: [], codTip: "", valueTip: "", emojiDisplay: "none" } },
                beforeMount: function() {
                    var t = this;
                    c.a.post(this.pathServer + "/tmo", {}, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(e) { t.tmo = e.data }))
                },
                methods: {
                    buttonActive: function() { return this.codTip.length <= 0 || this.valueTip.length <= 0 ? (this.myInput = !0, { back: "#D8D8D8 !important", cursor: "not-allowed !important", color: "#8F8F8F !important" }) : (this.myInput = !1, { back: "#008A46 !important", cursor: "pointer !important", color: "#fff !important" }) },
                    reset: function() { this.showDismissibleAlert = !1, this.imageInputName = a("7db1"), this.imageInputPass = "", this.imageInputEmail = "", this.imageInputPass2 = "", this.imageInputNameUser = " ", this.showDismissibleAlert3 = !1, this.showDismissibleAlert4 = !1, this.showDismissibleAlert5 = !1, this.showDismissibleAlert6 = !1, this.showDismissibleAlert7 = !1 },
                    handleSubmit: function(t) {
                        var e = this;
                        t.preventDefault(), this.codTip && this.valueTip && c.a.post(this.pathServer + "/saveShortcut", { tokenUser: localStorage.getItem(this.localPathJToken), codTip: this.codTip, valueTip: this.valueTip }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(this.localPathData)).jsonWebToken } }).then((function(t) { t.data.status && "sucess" === t.data.status ? (e.showDismissibleAlert6 = !0, e.tmo.push({ id: t.data.id, cod: e.codTip, value: e.valueTip }), e.codTip = "", e.valueTip = "") : "Error" === t.data && (e.showDismissibleAlert = !0) })).catch((function(t) { console.log(t.response) }))
                    },
                    change: function(t) {
                        var e = this;
                        this.tmo[t].warning && this.tmo[t].ending ? (this.boxTwo = "", this.$bvModal.msgBoxConfirm("¿Estas seguro de modificar el tmo?", { title: "Please Confirm", size: "sm", buttonSize: "sm", okVariant: "secondary", okTitle: "SI", cancelVariant: "info", cancelTitle: "NO", footerBgVariant: "white", footerClass: "p-2", hideHeader: !0, centered: !0 }).then((function(a) { e.boxTwo = a, 1 == a && c.a.post(e.pathServer + "/updateTmo", { tokenUser: localStorage.getItem(e.localPathJToken), warning: e.tmo[t].warning, ending: e.tmo[t].ending }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(e.localPathData)).jsonWebToken } }).then((function(a) { 200 == a.status && ("sucess" === a.data ? (e.showDismissibleAlert3 = !0, e.$parent.socket.emit("new_tmo", { warning: e.tmo[t].warning, ending: e.tmo[t].ending })) : "Error" === a.data && (e.showDismissibleAlert2 = !0)) })) })).catch((function(t) { console.log(t) }))) : this.showDismissibleAlert = !0
                    }
                },
                mounted: function() {}
            },
            Pe = je,
            Ee = (a("b348"), Object(m["a"])(Pe, De, Ie, !1, null, "2baf698f", null)),
            Ne = Ee.exports,
            Oe = { name: "tmo", components: { TmoCom: Ne }, data: function() { return { socket: this.$parent.socket } }, methods: { marginCom: function() { return "50px" == this.$parent.mgleftNav ? "0px" : "150px" } } },
            Be = Oe,
            ze = Object(m["a"])(Be, Se, Te, !1, null, "6bdba5fc", null),
            $e = ze.exports,
            Me = function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("div", { staticClass: "main" }, [s("div", { staticClass: "sidenav" }, [s("div", { staticClass: "row", staticStyle: { height: "98vh", background: "#002A3A" } }, [t._m(0), s("div", { staticClass: "col-md-6 col-sm-12", staticStyle: { background: "white" } }, [t._m(1), s("div", { staticClass: "container", staticStyle: { "max-width": "450px" } }, [s("div", { staticClass: "login-form" }, [s("b-form", [s("b-alert", { attrs: { variant: "danger" }, model: { value: t.showDismissibleAlert, callback: function(e) { t.showDismissibleAlert = e }, expression: "showDismissibleAlert" } }, [t._v("\n                Usuario o contraseña incorrecta\n                "), s("b-img", { staticStyle: { float: "right" }, attrs: { src: a("ca21") } })], 1), s("b-form-group", { staticClass: "s-label", staticStyle: { "font-weight": "bold" }, attrs: { label: "Nombre de Usuario", "label-for": "user" } }, [s("b-alert", { attrs: { variant: "danger", dismissible: "" }, model: { value: t.showDismissibleAlert2, callback: function(e) { t.showDismissibleAlert2 = e }, expression: "showDismissibleAlert2" } }, [t._v("\n                  Escribe un Nombre de Usuario Correcto\n                ")]), s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputName + ")" }, attrs: { id: "user", type: "text", required: "", autofocus: "", placeholder: "Escribe tu usuario..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.user, callback: function(e) { t.user = e }, expression: "user" } })], 1), s("b-form-group", { staticClass: "s-label mt-3 ", staticStyle: { "font-weight": "bold" }, attrs: { label: "Contraseña", "label-for": "password" } }, [s("b-alert", { attrs: { variant: "danger", dismissible: "" }, model: { value: t.showDismissibleAlert3, callback: function(e) { t.showDismissibleAlert3 = e }, expression: "showDismissibleAlert3" } }, [t._v("\n                  Escribe una Contraseña Correcto\n                ")]), s("b-form-input", { staticClass: "input-style", style: { "background-image": "url(" + t.imageInputPass + ")" }, attrs: { id: "password", type: "password", required: "", placeholder: "Escribe tu contraseña..." }, on: { click: t.reset, keydown: t.reset }, model: { value: t.password, callback: function(e) { t.password = e }, expression: "password" } })], 1), s("center", [s("br"), s("b-button", { staticClass: "button-login mt-3", style: { background: t.buttonActive().back, "border-color": t.buttonActive().back, cursor: t.buttonActive().cursor, color: t.buttonActive().color }, attrs: { type: "submit", disabled: !!t.myInput }, on: { click: t.handleSubmit } }, [t._v("\n                  Ingresar\n                ")])], 1)], 1)], 1)])])]), t._m(2)])])
            },
            Ue = [function() {
                var t = this,
                    e = t.$createElement,
                    s = t._self._c || e;
                return s("div", { staticClass: "col-md-6 col-sm-12 d-flex justify-content-center" }, [s("img", { staticClass: "align-self-center", staticStyle: { width: "550px" }, attrs: { src: a("39df"), alt: "" } })])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "text-center" }, [a("strong", [a("h1", { staticClass: "inicio m-login" }, [t._v("¡Bienvenido!")]), a("span", [t._v("Ten un grandioso día")])])])
            }, function() {
                var t = this,
                    e = t.$createElement,
                    a = t._self._c || e;
                return a("div", { staticClass: "row justify-content-center", staticStyle: { position: "fixed", bottom: "0px", width: "100%", "margin-left": "0%", background: "#002A3A", "font-size": "12px" } }, [a("span", { staticStyle: { color: "white" } }, [t._v("Powered By ")]), a("span", [t._v("\n        -\n        "), a("a", { attrs: { href: "https://www.montechelo.com.co/", target: "_blank" } }, [t._v("Monte"), a("span", { staticStyle: { color: "white" } }, [t._v("chelo")])])])])
            }];
        a("0a31");
        var Je = {
                data: function() { return { publicPath: "/bothuman/montechelo/", pathServer: "https://dashbots.outsourcingcos.com/widget/montechelo/tasks", localPathData: "data_agent_bot_montechelo", localPathJToken: "jwt_bothuman_montechelo", localPathState: "state_montechelos", user: "", password: "", bot: "", showDismissibleAlert: !1, showDismissibleAlert2: !1, showDismissibleAlert3: !1, imageInputName: a("7db1"), imageInputPass: "", myInput: !0 } },
                beforeCreate: function() { localStorage.getItem(this.localPathData) && this.$router.push({ name: "home" }) },
                methods: {
                    handleSubmit: function(t) {
                        var e = this;
                        t.preventDefault(), this.password.length > 0 && this.user.length > 0 ? c.a.post(this.pathServer + "/login", { username: this.user, password: this.password }).then((function(t) {
                            var a = t.data.login;
                            if (a) switch (localStorage.setItem(e.localPathJToken, t.data.token), localStorage.setItem(e.localPathData, JSON.stringify({ idAgent: e.user, nameAgent: t.data.name, jsonWebToken: t.data.jwtoken, capacity: t.data.capacity })), e.$parent.isActive = !1, e.$parent.navDis = "", e.$parent.nameAgent = JSON.parse(localStorage.getItem(e.localPathData)).nameAgent, e.$parent.idAgent = JSON.parse(localStorage.getItem(e.localPathData)).idAgent, window.idAg = JSON.parse(localStorage.getItem(e.localPathData)).idAgent, t.data.rol) {
                                case "Agentes":
                                    e.$parent.menu[1].hidden = !1, e.$parent.menu[2].hidden = !0, e.$parent.menu[3].hidden = !0, e.$parent.menu[4].hidden = !0, e.$router.push({ name: "home" });
                                    break;
                                case "Supervisor":
                                    e.$parent.menu[1].hidden = !0, e.$parent.menu[2].hidden = !1, e.$parent.menu[3].hidden = !1, e.$parent.menu[4].hidden = !1, e.$router.push({ name: "control" });
                                    break
                            } else e.showDismissibleAlert = !0
                        })).catch((function(t) { console.log(t.response) })) : this.user.length <= 0 ? (this.imageInputName = a("ca21"), this.showDismissibleAlert2 = !0) : this.password.length <= 0 && (this.showDismissibleAlert3 = !0, this.imageInputPass = a("ca21"))
                    },
                    reset: function() { this.showDismissibleAlert = !1, this.showDismissibleAlert2 = !1, this.imageInputName = a("7db1"), this.imageInputPass = "", this.showDismissibleAlert3 = !1 },
                    buttonActive: function() { return this.password.length <= 0 || this.user.length <= 0 ? (this.myInput = !0, { back: "#D8D8D8 !important", cursor: "not-allowed !important", color: "#8F8F8F !important" }) : (this.myInput = !1, { back: "#008A46 !important", cursor: "pointer !important", color: "#fff !important" }) }
                }
            },
            He = Je,
            Fe = (a("566c"), Object(m["a"])(He, Me, Ue, !1, null, "1c6bf1e4", null)),
            We = Fe.exports,
            Re = "https://dashbots.outsourcingcos.com/widget/montechelo/tasks",
            qe = "data_agent_bot_montechelo",
            Ve = "jwt_bothuman_montechelo";
        s["default"].use(f["a"]);
        var Le = new f["a"]({ base: "/bothuman/montechelo/", routes: [{ path: "/", component: O, meta: { requiresAuth: !0 }, name: "home" }, { path: "/LoginCom", component: We, meta: { Logi: !0 }, name: "login" }, { path: "/control", component: V, meta: { requiresAuthSup: !0 }, name: "control" }, { path: "/user", component: it, meta: { requiresAuthSup: !0 }, name: "user" }, { path: "/user/update", component: ft, meta: { requiresAuthSup: !0 }, name: "userUpdate" }, { path: "/settings", component: jt, meta: { requiresAuthSup: !0 }, name: "settings" }, { path: "/settings/capacity", component: Ft, meta: { requiresAuthSup: !0 }, name: "settingsCap" }, { path: "/settings/qr", component: te, meta: { requiresAuthSup: !0 }, name: "qrfile" }, { path: "/settings/tmo", component: $e, meta: { requiresAuthSup: !0 }, name: "tmo" }, { path: "/settings/tipificaciones", component: ue, meta: { requiresAuthSup: !0 }, name: "tipfi" }, { path: "/settings/atajos", component: Ae, meta: { requiresAuthSup: !0 }, name: "short" }] });
        Le.beforeEach((function(t, e, a) {
            var s = localStorage.getItem(Ve),
                i = JSON.parse(localStorage.getItem(qe));
            t.meta.requiresAuth ? s && i ? c.a.post(Re + "/checkAgent", { idAgent: window.idAg, tokenUser: localStorage.getItem(Ve) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(qe)).jsonWebToken } }).then((function(t) { "sucess" === t.data ? a() : a({ name: "control" }) })) : a({ name: "login" }) : t.meta.Logi ? i ? i && a({ name: "home" }) : a() : t.meta.requiresAuthSup && (s ? c.a.post(Re + "/checkSup", { idAgent: window.idAg, tokenUser: localStorage.getItem(Ve) }, { headers: { "Content-Type": "application/json", authorization: "Bearer " + JSON.parse(localStorage.getItem(qe)).jsonWebToken } }).then((function(t) { "sucess" === t.data ? a() : a({ name: "home" }) })) : a({ name: "login" }))
        }));
        var Ze = Le,
            Xe = (a("f9e3"), a("2dd8"), a("5f5b"));
        s["default"].config.productionTip = !1, s["default"].use(Xe["a"]), new s["default"]({ router: Ze, render: function(t) { return t(g) } }).$mount("#app")
    },
    "5d38": function(t, e) { t.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADEAAAA1CAIAAABnb5RdAAAAA3NCSVQICAjb4U/gAAAAEHRFWHRTb2Z0d2FyZQBTaHV0dGVyY4LQCQAAAihJREFUWMNj/PXrFwPp4NW371vvPhXj4vBWlmGgNmAhVcPf///3PHi26c7j3//++SrLMtAAkOam3//+zbhw8/Lr9wy0BCwkOWja+RtX33xgoDFgIl7poit36eAgEtx07Omrk89fM9AFEOWmb7//rLv1kIFegCg37Xn4/NOv33RzE740/vbXn8WP3nz+9efg/eeffv7DEn7PPl74xcjAzMTAzEShOyxZf7nIy0HYjLjKzPVP34Ufvs7w6w8dAkbx2/PVv87q5XXji7v1T9+FH7hKHweJ/3jbf2Ym59/f+NLT5hcfwg9cZfj7jz4Omnx2usivr/jS+KkPX4P3XaGPg/h+fZx8drrU94/48t2tLz88d19i+POXHrnr748J52ZiOgjFTbe+/HDddfHz91/0cNC/350X56p9eU2gLFi5YtXzuYtIMrk2Oa42JY4MJ304s+fWvgdUqO8GVzk+6qZRN426adRNo24addMQdBMLr9DgchO7sLiUf9IgchMrn6BG+RR2UZnB4iZmdnb1ol52CfnBkp6YWFlV87u5lHQGSxpnYmVVze/k07EgrR9MELz//Pne02dkaPz5/qNKTiu/ng3JfXOCYMqq9VNWrSdDo5elyYa+jsFVPjEysYyW46NuGnXTqJtG3TTqpgFxk4igAN1sFRXkJ8pNEa6OcuJi9Gh9s7Lmhgfjq6GRx+yfv3k7acXaF2/f0c5BPFxcWSF+mooKxLppNI3jBAAX69BjuFmkiQAAAABJRU5ErkJggg==" },
    "60ab": function(t, e, a) {},
    6310: function(t, e, a) { t.exports = a.p + "img/chat_msg.cb170a68.svg" },
    6364: function(t, e, a) { t.exports = a.p + "img/bot_chat.e1351f3c.svg" },
    "639f": function(t, e, a) {},
    "66fc": function(t, e, a) { t.exports = a.p + "img/checkChats.4a42842b.svg" },
    6935: function(t, e, a) { t.exports = a.p + "img/iconAgent.4724b177.svg" },
    "6f6b": function(t, e, a) { t.exports = a.p + "img/search.84ce85c3.svg" },
    "75ac": function(t, e, a) {
        "use strict";
        var s = a("b824"),
            i = a.n(s);
        i.a
    },
    "7db1": function(t, e, a) { t.exports = a.p + "img/person_outline.26b5e00e.svg" },
    "811d": function(t, e, a) { t.exports = a.p + "img/user_chat.797f585f.svg" },
    "87d5": function(t, e, a) {},
    "896a": function(t, e, a) { t.exports = a.p + "img/chat_fin.f111d148.svg" },
    "8ed7": function(t, e, a) { t.exports = a.p + "img/iconmontechelo.c02bc70b.png" },
    "8fbe": function(t, e, a) {
        "use strict";
        var s = a("066c"),
            i = a.n(s);
        i.a
    },
    "963a": function(t, e, a) {
        "use strict";
        var s = a("06fc"),
            i = a.n(s);
        i.a
    },
    9734: function(t, e) { t.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADEAAAA1CAIAAABnb5RdAAAAA3NCSVQICAjb4U/gAAAAEHRFWHRTb2Z0d2FyZQBTaHV0dGVyY4LQCQAAAihJREFUWMNj/PXrFwPp4NW371vvPhXj4vBWlmGgNmAhVcPf///3PHi26c7j3//++SrLMtAAkOam3//+zbhw8/Lr9wy0BCwkOWja+RtX33xgoDFgIl7poit36eAgEtx07Omrk89fM9AFEOWmb7//rLv1kIFegCg37Xn4/NOv33RzE740/vbXn8WP3nz+9efg/eeffv7DEn7PPl74xcjAzMTAzEShOyxZf7nIy0HYjLjKzPVP34Ufvs7w6w8dAkbx2/PVv87q5XXji7v1T9+FH7hKHweJ/3jbf2Ym59/f+NLT5hcfwg9cZfj7jz4Omnx2usivr/jS+KkPX4P3XaGPg/h+fZx8drrU94/48t2tLz88d19i+POXHrnr748J52ZiOgjFTbe+/HDddfHz91/0cNC/350X56p9eU2gLFi5YtXzuYtIMrk2Oa42JY4MJ304s+fWvgdUqO8GVzk+6qZRN426adRNo24addMQdBMLr9DgchO7sLiUf9IgchMrn6BG+RR2UZnB4iZmdnb1ol52CfnBkp6YWFlV87u5lHQGSxpnYmVVze/k07EgrR9MELz//Pne02dkaPz5/qNKTiu/ng3JfXOCYMqq9VNWrSdDo5elyYa+jsFVPjEysYyW46NuGnXTqJtG3TTqpgFxk4igAN1sFRXkJ8pNEa6OcuJi9Gh9s7Lmhgfjq6GRx+yfv3k7acXaF2/f0c5BPFxcWSF+mooKxLppNI3jBAAX69BjuFmkiQAAAABJRU5ErkJggg==" },
    "9ae5": function(t, e, a) {
        "use strict";
        var s = a("f9cb"),
            i = a.n(s);
        i.a
    },
    "9bd9": function(t, e, a) { t.exports = a.p + "img/file-chooser.b3853c2e.svg" },
    "9d89": function(t, e, a) { t.exports = a.p + "img/download.cb50e8c3.png" },
    "9e5a": function(t, e, a) { t.exports = a.p + "img/chat.0a22ffd6.svg" },
    a9dc: function(t, e, a) { t.exports = a.p + "img/elipseOcupado.415031e9.svg" },
    b348: function(t, e, a) {
        "use strict";
        var s = a("096c"),
            i = a.n(s);
        i.a
    },
    b824: function(t, e, a) {},
    b882: function(t, e, a) {},
    bb66: function(t, e, a) { t.exports = a.p + "img/iconAgent.d14e5245.png" },
    bb6e: function(t, e, a) {
        var s = { "./Break.svg": "cd47", "./Ocupado.svg": "f09a", "./Offbutton.svg": "f8be", "./Online.svg": "245b", "./base/download.png": "9d89", "./bot_chat.svg": "6364", "./chat.svg": "9e5a", "./chat_fin.svg": "896a", "./chat_module.svg": "56a9", "./chat_msg.svg": "6310", "./checkChats.svg": "66fc", "./download.png": "c9b9", "./elipseBreak.svg": "f3a5", "./elipseOcupado.svg": "a9dc", "./elipseOffline.svg": "1143", "./elipseOnline.svg": "c58e", "./emoji.svg": "d99d", "./exit_to_app.svg": "d365", "./file-chooser.svg": "9bd9", "./iconAgent.png": "bb66", "./iconAgent.svg": "6935", "./iconmontechelo.png": "8ed7", "./chat1.svg": "39df", "./logom.png": "9734", "./montechelo-logo.svg": "175c", "./montechelo.png": "5d38", "./navbarmontechelo.svg": "13fd", "./person_outline.svg": "7db1", "./person_outline_err.svg": "ca21", "./question.svg": "5009", "./search.svg": "6f6b", "./send.svg": "c23e", "./tip.svg": "d5e9", "./user_chat.svg": "811d", "./whatsapp.svg": "3776" };

        function i(t) { var e = n(t); return a(e) }

        function n(t) { if (!a.o(s, t)) { var e = new Error("Cannot find module '" + t + "'"); throw e.code = "MODULE_NOT_FOUND", e } return s[t] }
        i.keys = function() { return Object.keys(s) }, i.resolve = n, t.exports = i, i.id = "bb6e"
    },
    bc54: function(t, e, a) {},
    c23e: function(t, e, a) { t.exports = a.p + "img/send.ff8027db.svg" },
    c327: function(t, e, a) {
        "use strict";
        var s = a("1188"),
            i = a.n(s);
        i.a
    },
    c58e: function(t, e, a) { t.exports = a.p + "img/elipseOnline.f0d26ce9.svg" },
    c9b9: function(t, e, a) { t.exports = a.p + "img/download.cb50e8c3.png" },
    ca21: function(t, e, a) { t.exports = a.p + "img/person_outline_err.aa934315.svg" },
    cb5f: function(t, e, a) {},
    cd47: function(t, e, a) { t.exports = a.p + "img/Break.266d3419.svg" },
    d000: function(t, e, a) {
        "use strict";
        var s = a("4db4"),
            i = a.n(s);
        i.a
    },
    d365: function(t, e, a) { t.exports = a.p + "img/exit_to_app.1c35bc29.svg" },
    d417: function(t, e, a) {
        "use strict";
        var s = a("4d58"),
            i = a.n(s);
        i.a
    },
    d5e9: function(t, e, a) { t.exports = a.p + "img/tip.15a775b3.svg" },
    d675: function(t, e, a) {
        "use strict";
        var s = a("f8c1"),
            i = a.n(s);
        i.a
    },
    d82d: function(t, e, a) {
        "use strict";
        var s = a("ee6a"),
            i = a.n(s);
        i.a
    },
    d99d: function(t, e, a) { t.exports = a.p + "img/emoji.98cf5c3e.svg" },
    ee6a: function(t, e, a) {},
    f09a: function(t, e, a) { t.exports = a.p + "img/Ocupado.43215494.svg" },
    f3a5: function(t, e, a) { t.exports = a.p + "img/elipseBreak.7271a0ea.svg" },
    f8be: function(t, e, a) { t.exports = a.p + "img/Offbutton.ebd2bd51.svg" },
    f8c1: function(t, e, a) {},
    f9cb: function(t, e, a) {}
});
//# sourceMappingURL=app.b073452f.js.map