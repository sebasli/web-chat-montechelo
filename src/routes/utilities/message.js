const axios = require("axios");

const bot = require("../../bot");

//Funcion que procesa mensajes de DialogFlow
async function processMessage(from, message) {
    try {
        let sessionId = `5714864355-${from}`;
        let receivedMessage = message;
        let intent = await bot(sessionId, receivedMessage);
        let responseOfDialogflow = intent[0].queryResult.fulfillmentMessages;
        var actionQuery = intent[0].queryResult.action;
        let messagesToSend = [];

        responseOfDialogflow.forEach(element => {
            if (element.platform === "PLATFORM_UNSPECIFIED") {
                messagesToSend.push(element.text.text[0])
            }
        });


        console.log(responseOfDialogflow);
        return messagesToSend;
    } catch (error) {
        console.error(error);
        return [];
    }
}

//Funcion que envia mensajes a WhatsApp Oficial mediante Infobip
function sendMessage(to, message) {
    axios
        .post(
            "https://ejj6rq.api.infobip.com/omni/1/advanced",
            JSON.stringify({
                scenarioKey: "C162C938664AA201EC22BCAE82266F8A",
                destinations: [{
                    to: {
                        phoneNumber: to
                    }
                }],
                whatsApp: {
                    text: message
                }
            }), {
                headers: {
                    "Authorization": "Basic TW9udGVjaGVsby50ZXN0OipNMG50M2NoM2wwMjAyMCo=",
                    "Content-Type": "application/json"
                }
            }
        )
        .then(data => {
            return true;
        })
        .catch(err => {
            return false;
        });
}


module.exports = { processMessage, sendMessage };